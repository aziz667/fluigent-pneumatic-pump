#!/bin/sh
if [ "$(whoami)" != "root" ]; then
	su -c "$0" root
	exit
fi
echo "Extracting FRP library..."
tar -xvzf ./Shared/Raspberry/pi_libfrp_lib.so.tar.gz -C /usr/lib
ldconfig -l /usr/lib/libfrp_lib.so.2.0.0
echo "Copy of 99-hid.rules..."
cp ./Shared/Raspberry/99-hid.rules /etc/udev/rules.d/
echo "Compilation of example application..."
g++ -I. -o Cpp_FRP_Series_Application ./Cpp_Linux/Cpp_FRP_Series.cpp -lfrp_lib

