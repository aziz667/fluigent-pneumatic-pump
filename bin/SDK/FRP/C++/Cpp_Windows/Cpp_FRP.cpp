/*============================================================================*/
/*                    Fluigent / Cpp FRP library Example                      */
/*----------------------------------------------------------------------------*/
/*         Copyright (c) Fluigent 2018.  All Rights Reserved.                 */
/*----------------------------------------------------------------------------*/
/*                                                                            */
/* Title:   Cpp_FRP.cpp                                                       */
/* Purpose: Interact with Fluigent Flow-Rate Platform			              */
/* Version: 1.4.0                                                             */
/* Hardware setup:One flow-rate sensor connected to channel 1 of the flowboard*/
/*                                                                            */
/*============================================================================*/

#include <cstdlib>
#include <iostream>
#include <windows.h>
//#include "frp_c.h"

/* Define functions prototype */
typedef unsigned long long(__stdcall *init)(unsigned short serial);
typedef unsigned char(__stdcall *serial)(unsigned long long handle, unsigned short *serial, unsigned short *version);
typedef unsigned char(__stdcall *close)(unsigned long long handle);
typedef unsigned char(__stdcall *read)(unsigned long long handle, unsigned char index,
	unsigned char * timeStamp, float * flow);
typedef unsigned char(__stdcall *data_FU)(unsigned long long handle,
	unsigned char index,
	unsigned char * cal,
	unsigned char * res,
	unsigned char * artcod,
	unsigned short * sf,
	unsigned char * unit,
	unsigned char * tb);

//===============================================================
//		Main function
//===============================================================
int main(int argc, char *argv[])
{
	/* Load DLL into memory */
	HINSTANCE ProcDLL_FRP;

#ifdef _WIN64
	ProcDLL_FRP = LoadLibrary(TEXT("frp_c_64.dll"));
#else
	ProcDLL_FRP = LoadLibrary(TEXT("frp_c_32.dll"));
#endif

	/* Declare pointers on dll functions */
	init dll_init;
	serial dll_serial;
	close dll_close;
	read dll_read;
	data_FU dll_data_FU;

	/* Link dll pointers with functions prototype */
	dll_init = (init)GetProcAddress(ProcDLL_FRP, "frp_initialization");
	dll_serial = (serial)GetProcAddress(ProcDLL_FRP, "frp_get_serial");
	dll_close = (close)GetProcAddress(ProcDLL_FRP, "frp_close");
	dll_read = (read)GetProcAddress(ProcDLL_FRP, "frp_read_flow");
	dll_data_FU = (data_FU)GetProcAddress(ProcDLL_FRP, "frp_data_FU");

	/* Flow board variable declaration */
	unsigned long long frpHandle = 0;
	unsigned short Serial = 0;
	unsigned short Version = 0;

	/* Flow-rate acquisition variables */
	unsigned char sensor_index = 0;			// sensor index coresponds to flow-unit port on the flowboard from 0 to 7
	unsigned char TimeCheck = 0;
	float flow_rate = 0;
	unsigned int loop = 0;

	if (ProcDLL_FRP != NULL) {				// If dll loaded

		/* Initialize device */
		if (dll_init != NULL) {					// Check if function was properly linked to the dll file
			/* Initialize the first FLOWBOARD in Windows enumeration list */
			frpHandle = dll_init(0);
			std::cout << "FRP session initialized" << std::endl;
		}

		/* Read device serial number */
		if (dll_serial != NULL) {
			/*Get the serial number of the MFCS*/
			dll_serial(frpHandle, &Serial, &Version);
			std::cout << "FLOWBOARD SN: " << std::hex << Serial << std::endl;
		}

		/* If Flowboard is detected returned serial number is different from 0 */
		if (Serial != 0)
		{
			/* Read flow rate value over 2 seconds*/
			for (loop = 0; loop < 20; loop++){
				if (dll_read != NULL) {
					dll_read(frpHandle, sensor_index, &TimeCheck, &flow_rate);
					std::cout << "Flow-rate: " << std::fixed << flow_rate << " ul/min" << std::endl;
				}
				Sleep(100);
			}
		}

		/* Close FRP session */
		if (dll_close != NULL) {
			dll_close(frpHandle);
			std::cout << "FRP session closed" << std::endl;
		}
	}

	/* Release the DLL */
	FreeLibrary(ProcDLL_FRP);
	std::cout << "frp dll unloaded" << std::endl;

	/* Exit application */
	system("PAUSE");

	return EXIT_SUCCESS;
}


