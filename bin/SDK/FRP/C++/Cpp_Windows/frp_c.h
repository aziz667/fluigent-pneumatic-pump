/*============================================================================*/
/*                         Fluigent / FRP API                                 */
/*----------------------------------------------------------------------------*/
/*         Copyright (c) Fluigent 2018.  All Rights Reserved.                 */
/*----------------------------------------------------------------------------*/
/*                                                                            */
/* Title:   frp_c.h                                                           */
/* Purpose: Include file to interact with the FRP API                         */
/* Version: 1.4.0.0                                                           */
/*                                                                            */
/*============================================================================*/

#ifndef frp_cH
#define frp_cH

/* Name: 		frp_initialization
 *
 * Description:	initializes USB connection and launches a continuous check for a FLOWBOARD with the specified serial number
 * Arguments:	serial is the serial number of the FLOWBOARD for which  you want to open the session.  
 * Returns:		handle is the identifier of the opened session
 */
unsigned long long __stdcall frp_initialization(unsigned short serial);

/* Name: 		frp_close
 *
 * Description:	terminates threads and deallocates memory used by the FRP session
 * Arguments:	handle is the identifier of the FRP session 
 * Returns:		C error is the error code
 */
unsigned char __stdcall frp_close(unsigned long long handle);

/* Name: 		frp_read_flow
 *
 * Description:	returns the last flow-rate acquisition with its measurement parameters for the specified sensor position
 * Arguments:	handle is the identifier of the FRP session 
 *              index is the port number
 * Returns:		C error is the error code
 *              timeStamp is the data timestamp
 *              flow is sensor read flow-rate in µL/min  
 */
unsigned char __stdcall  frp_read_flow(unsigned long long handle,
									unsigned char index,
									unsigned char * timeStamp,
									float * flow);
									   
/* Name: 		frp_data_FU
 *
 * Description:	reads the flow unit parameters for the specified position
 * Arguments:	handle is the identifier of the FRP session 
 *              index is the port number
 * Returns:		C error is the error code
 *              cal is the active calibration table of the sensor 0:H2O, 1:IPA
 *              res is sensor resolution
 *              artcod is sensor article code, an array of 32 char maximum
 *              sf is the scale factor needs to convert raw data to the measured flow-rate for the active calibration table
 *              unit is the flow unit used in the flow-rate unit
 *              tb is the timebase used in the flow-rate unit     
 */									   
unsigned char __stdcall frp_data_FU(unsigned long long handle,
    								unsigned char index,
                                    unsigned char * cal,
                                    unsigned char * res,
                                    unsigned char * artcod,
                                    unsigned short * sf,
                                    unsigned char * unit,
                                    unsigned char * tb);
										
/* Name: 		frp_get_serial
 *
 * Description:	gets serial number
 * Arguments:	handle is the identifier of the FRP session
 * Returns:		C error is the error code
 *              serial is the FLOWBOARD serial number associated to the FRP session specified by the connection handle
 *				version is the FLOWBOARD firmware version
 */
unsigned char __stdcall frp_get_serial(unsigned long long handle,
									   unsigned short * serial, 
									   unsigned short * version);
										
/* Name: 		frp_set_calibration
 *
 * Description:	Sets the calibration table for the specified position
 * Arguments:	handle is the identifier of the FRP session 
 *              index is the port number
 *              cal is the calibration table of the sensor 0:H2O, 1:IPA
 * Returns:		C error is the error code
 */												
unsigned char __stdcall  frp_set_calibration(unsigned long long handle,
											 unsigned char index,
											 unsigned char cal);
										
/* Name: 		frp_detect
 *
 * Description:	Detect the connected FLOWBOARD and return their serial numbers in the table
 * Arguments:	 
 * Returns:		table is the array of all connected FLOWBOARD
 */
unsigned char __stdcall frp_detect(unsigned short table[256]);

// return values

#define FRP_OK		    		0
#define FRP_ERR_USB_CLOSED	   	1
#define FRP_WRONG_CHANEL    	2

#endif






