//---------------------------------------------------------------------------
// frp_c.h
//---------------------------------------------------------------------------
#ifndef frp_cH
#define frp_cH

#ifdef __cplusplus
extern "C"
{
#endif

/* Name: 		frp_initialization
 *
 * Description:	initializes USB connection and launches a continuous check for a FLOWBOARD with the specified serial number
 * Arguments:	serial is the serial number of the FLOWBOARD for which  you want to open the session.  
 * Returns:		handle is the identifier of the opened session
 */
unsigned long long frp_initialization(unsigned short serial);

/* Name: 		frp_close
 *
 * Description:	terminates threads and deallocates memory used by the FRP session
 * Arguments:	handle is the identifier of the FRP session 
 * Returns:		C error is the error code
 */
unsigned char frp_close(unsigned long long handle);

/* Name: 		frp_detect
 *
 * Description:	detects the connected FLOWBOARD devices and return their serial numbers in the table
 * Arguments:	
 * Returns:		C error is the error code
 *              table is the array of all connected FLOWBOARDs
 */	
unsigned char frp_detect(unsigned short table[256]);

/* Name: 		frp_read_flow
 *
 * Description:	returns the last flow-rate acquisition of the flow unit at the specified index
 * Arguments:	handle is the identifier of the FRP session 
 *              index is the port number (0 to 7)
 * Returns:		C error is the error code
 *              ts is the data timestamp
 *              flow is the flow rate in ul/min  
 */
unsigned char frp_read_flow(unsigned long long handle,
	unsigned char index, unsigned char * ts, float * flow);
	
/* Name: 		frp_data_FU
 *
 * Description:	reads the flow unit parameters for the specified position
 * Arguments:	handle is the identifier of the FRP session 
 *              pos is the port number
 * Returns:		C error is the error code
 *              cal is the active calibration table on the sensor
 *              res is sensor resolution
 *              artcod is sensor article code
 *              sf is the scale factor needs to convert raw data to the measured flow-rate for the active calibration table
 *              unit is the flow unit used in the flow-rate unit
 *              tb is the timebase used in the flow-rate unit     
 * Note: the "res", "unit" and "tb "return values are deprecated. The resolution is fixed at 13 bits and 
 *       the flow rate is always given in ul/min
 */
unsigned char frp_data_FU(unsigned long long handle,
	unsigned char index, unsigned char * cal, unsigned char * res,
	unsigned char * artcod, unsigned short * sf, unsigned char * unit, unsigned char * tb);
	
/* Name: 		frp_get_serial
 *
 * Description:	gets serial number
 * Arguments:	handle is the identifier of the FRP session
 * Returns:		C error is the error code
 *              serial is the FLOWBOARD serial number associated to the FRP session specified by the connection handle
 */
unsigned char frp_get_serial(unsigned long long handle, unsigned short * serial, unsigned short * version);

/* Name: 		frp_set_calibration
 *
 * Description:	Sets the calibration table for the specified position
 * Arguments:	handle is the identifier of the FRP session 
 *              index is the port number
 *              cal is the calibration table (0 for water, 1 for isopropanol)
 * Returns:		C error is the error code
 */	
unsigned char frp_set_calibration(unsigned long long handle, unsigned char index, unsigned char cal);

#ifdef __cplusplus
}
#endif

#endif






