﻿===========================================================================
    CONSOLE APPLICATION : Cpp_FRP_Series_Application
===========================================================================

This file contains a summary of the FRP example application.

-------------------------- HARDWARE ---------------------------------------
A flow-rate device connected to a flowboard on channel #1.

-------------------------- SOFTWARE ---------------------------------------
The software was implemented and tested on different Linux distributions including Debian, OpenSuse, Ubuntu and Scientific Linux

------------------------ INSTALLATION -------------------------------------
Super user rights are required in order to grant full access to “/usr/lib” before installation.

The application is based on a dynamically linked shared object libraries contained in .tar archieve. 
In order to use the library functions, all files have to be copied in the default Linux library directory: “/usr/lib”.

USB device has to be registred (in order to grant full access to Fluigent devices).
Copy or merge "99-hid.rules" file to /etc/udev/rules.d/ folder.

The "frp_c.h" file is the header for the library. FRP functions can be directly called using this file.
However "libfrp_lib.so" library has to be included in project makefile (“LIBS=-lfrp_lib”) while building application.


-------------------- APPLICATION BEHAVIOUR -------------------------------
The executable application performs the following steps:
1. Open a FRP session for the first found device
2. Display device serial number and version
3. Display flow-rate value and unit for the first flow-rate on the platform
4. Close communication with the device

