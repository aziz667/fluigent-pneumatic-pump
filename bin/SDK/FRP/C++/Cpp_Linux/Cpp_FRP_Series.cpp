/*============================================================================*/
/*                    Fluigent / Cpp FRP library Example                      */
/*----------------------------------------------------------------------------*/
/*         Copyright (c) Fluigent 2018.  All Rights Reserved.                 */
/*----------------------------------------------------------------------------*/
/*                                                                            */
/* Title:   Cpp_FRP_Series.cpp                                                */
/* Purpose: Interact with Fluigent Flow-Rate Platform			      */
/* Version: 1.2                                                               */
/* Hardware setup:One flow-rate sensor connected to channel 1 of the flowboard*/
/*                                                                            */
/*============================================================================*/

#include "frp_c.h"
#include <iostream>
#include <iomanip>
#include <string>
#include <cstdlib>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>             // Use of sleep() function
#include <signal.h>             // Used to capture keyboard interrupts

#define True 1
#define False 0

#define SUCCESS 1
#define FAIL 0

typedef long unsigned int UL;
typedef unsigned short US;
typedef char S[];
typedef unsigned char C;
typedef float F;
typedef char B;

/*============================================================================*/
/* Function name:   exit_application()                                        */
/* Purpose:         releases resources (FRP session) and exits application   */
/* Arguments:       BOOLEAN SUCCESS/FAIL: end state of the application        */
/* Returns:         N/A                                                       */
/*============================================================================*/
int exit_application(bool state, UL Handle)
{
    B B_OK;
    std::cout << "Releasing USB resource." << std::endl;
    B_OK=frp_close(Handle);	    //Close FRP session
    if (B_OK==SUCCESS){
	printf("\nPress 'ENTER' to continue.");      
        getchar();   	   	    // Wait for user to press Enter
        return EXIT_SUCCESS;        //Returns global state of the application as success
    }                               //Note: if used by other application(s), global result status can also be used for control
    else{
        return EXIT_FAILURE;        //Returns global state of the application as failed
    }
}


/*============================================================================*/
/* 				Main function 				      */
/*============================================================================*/
int main(int argc, char *argv[])
{
    /* System settings variable definition */
    C sensor_position = 0;		// 0 to 7 (Flowboard port number -1)

    /* Script general variables definition */
    UL frpHandle;
    US sysSerial;
    US sysVersion;
    C C_error;
    B B_OK;

    /* Flow-rate acquisition variables */
    C TimeCheck = 0;
    float flow_rate = 0;
    US ScaleFactor = 0;

    /* Optional: Set displayed precision of float numbers */
    std::cout.precision(2);

    std::cout << "FLUIGENT: C++ FRP Series example." << std::endl;		// Display on Terminal

    /* Open FRP session for the first enumerated FRP device*/
    frpHandle=frp_initialization(0);
    std::cout << "Frp session handle: " << frpHandle << std::endl;		// Display session handle

    /* Get serial number of the FRP associated to the FRP session*/
    C_error=frp_get_serial(frpHandle, &sysSerial, &sysVersion);
    if (C_error==0){								// Manage returned status of called library function
      std::cout << "Device serial number: " << std::hex << sysSerial << std::endl;
      std::cout << "Device version: " << sysVersion << std::endl;
    }else{
      std::cout << "Error getting device serial number (1: USB closed; 2: Sensor not available; 3: Calibration error): " << int(C_error) << std::endl;
    }

    /* Read flow rate value */
    std::cout << "Reading flow-rate value on channel " << int(sensor_position) << std::endl;
    C_error=frp_read_flow(frpHandle, sensor_position, &TimeCheck, &flow_rate);
    if (C_error==0){								// Manage returned status of called library function
      std::cout << "Time stamp: " << std::dec << int(TimeCheck) << std::endl;
      std::cout << "Flow-rate: " << std::fixed  << flow_rate << "ul/min" << std::endl;
    }else{
      std::cout << "Error reading flow-rate value (1: USB closed; 2: Sensor not available; 3: Calibration error): " << int(C_error) << std::endl;
    }

    /* Exit application */
    return exit_application(SUCCESS,frpHandle);
}
