#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This example shows how to interact with a Flowboard device using the Flowboard
class.
The Flowboard class acts as a virtual representation of the instrument. It also 
manages the lifetime of the connection with the instrument, ensuring it gets
closed when the object is destroyed.
"""

# Print function for Python 2 compatibility
from __future__ import print_function 
# Flowboard class
from Fluigent.FRP import Flowboard

# Get the serial numbers of the available instruments
instrument_serial_numbers = Flowboard.detect()

if not instrument_serial_numbers:
    raise Exception("No Flowboard device detected")
    
print("Available Flowboards: {}".format(instrument_serial_numbers))

# Initialize the first instrument in the list
flowboard = Flowboard(instrument_serial_numbers[0])

# "print" the initialized device to view its basic information
print(flowboard)

# Get the ports that have Flow Units connected
available_ports = flowboard.get_available_ports()

if not available_ports:
    raise Exception("No Flow Unit connected to this Flowboard")

# Display the flow rate on the first connected Flow Unit in ul/min
port_number = available_ports[0]
flowrate = flowboard.get_flowrate(available_ports[0])
print("port {} flow rate: {} ul/min".format(port_number, flowrate))

# Display data on each Flow Unit connected to the Flowboard
for port in available_ports:
    data = flowboard.get_flowunit_data(port)
    print("Flow Unit on port {}: {}".format(port, data))
    
# Set the calibration table to water on all the Flow Units
# If the Flow Unit has dual calibration, you can also set to "IPA" (Isopropanol)
for port in available_ports:
    flowboard.set_calibration(port, "Water")
