#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This example shows how to interact with individual Flow Unit devices using
the properties of the Flow Unit class.
The properties allow you to read and modify data on the physical instrument
by reading and setting attributes on the object, thus abstracting away the
function calls. They are added for convenience but they are not necessary.
To interact with the instruments using regular function calls, see the 
previous examples.
"""

# Print function for Python 2 compatibility
from __future__ import print_function 
# Flowboard class
from Fluigent.FRP import Flowboard

# Get the serial numbers of the available instruments
instrument_serial_numbers = Flowboard.detect()

if not instrument_serial_numbers:
    raise Exception("No Flowboard device detected")
    
print("Available Flowboards: {}".format(instrument_serial_numbers))

# Initialize the first instrument in the list
flowboard = Flowboard(instrument_serial_numbers[0])

# "print" the initialized device to view its basic information
print(flowboard)

# Get a reference to the first flow unit, regardless of which port 
# it is connected to
for flow_unit in flowboard:
    break

# Display the flow rate on the first connected Flow Unit in ul/min
print("port {} flow rate: {} ul/min".format(flow_unit.port, flow_unit.flowrate))

# Display data on each Flow Unit connected to the Flowboard
for flow_unit in flowboard:
    print("Flow Unit on port {}: {}".format(flow_unit.port, flow_unit.data))
    
# Set the calibration to water on all the Flow Units
# If the Flow Unit has dual calibration, you can also set to "IPA" (Isopropanol)
for flow_unit in flowboard:
    flow_unit.calibration = "Water"
