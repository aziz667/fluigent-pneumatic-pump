# -*- coding: utf-8 -*-
"""
This example shows how to interact with an individual Switc connected to a Switchboard
via the Switch class.
The Switch properties allow you to abstract away method calls and communicate
with the instrument by setting and reading attributes, thus making the code cleaner
"""

# Print function for Python 2 compatibility
from __future__ import print_function 
# MFCS class
from Fluigent.ESS import Switchboard

# Get the serial numbers of the available instruments
instrument_serial_numbers = Switchboard.detect()

if not instrument_serial_numbers:
    raise Exception("No Switchboard device detected")
    
print("Available Switchboards: {}". format(instrument_serial_numbers))

# Initialize the first instrument in the list
switchboard = Switchboard(instrument_serial_numbers[0])

# "print" the initialized device to view its basic information
print(switchboard)

# Get the ports that have switches connected
ports = switchboard.get_available_ports()
if not ports:
    raise Exception("No Switch connected to this Switchboard")

print("Switches at ports {}".format(ports))

# Get the position of the Switches
for switch in switchboard:
    print("Switch at port {} is at position {}".format(switch.port, switch.position))

# Get a reference to the Switch at port A
try:
    switch = switchboard["A"]
# If there is no Switch at port A, get the first Switch available
except:
    switch = switchboard[ports[0]]
    
# Get data on the first available Switch
print("Switch at port {} data: {}".format(switch.port, switch.data))

# Set the Switch to all of its available positions
for position in switch.positions:
    switch.position = position
    
# Then back to position 1, which is the default for all Switches
switch.position = 1
