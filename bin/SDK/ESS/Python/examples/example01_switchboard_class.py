# -*- coding: utf-8 -*-
"""
This example shows how to interact with a Switchboard and its connected Switches
via the Switchboard class
The Switchboard class acts as a virtual representation of the instrument. It also 
manages the lifetime of the connection with the instrument, ensuring it gets
closed when the object is destroyed.
"""

# Print function for Python 2 compatibility
from __future__ import print_function 
# MFCS class
from Fluigent.ESS import Switchboard

# Get the serial numbers of the available instruments
instrument_serial_numbers = Switchboard.detect()

if not instrument_serial_numbers:
    raise Exception("No Switchboard device detected")
    
print("Available Switchboards: {}". format(instrument_serial_numbers))

# Initialize the first instrument in the list
switchboard = Switchboard(instrument_serial_numbers[0])

# "print" the initialized device to view its basic information
print(switchboard)

# Get the ports that have switches connected
ports = switchboard.get_available_ports()
if not ports:
    raise Exception("No Switch connected to this Switchboard")

print("Switches on ports {}".format(ports))

# Get the position of the Switches
for port in ports:
    print("Switch on port {} is at position {}".format(port, switchboard.get_position(port)))
    
# Set the position of the Switch on port "A" to 1, if the Switch is connected
try:
    switchboard.set_position("A", 1)
except:
    print("No Switch at port A")
    
# Set the first Switch available to all of its positions, one by one
for position in switchboard.get_positions(ports[0]):
    switchboard.set_position(ports[0], position)

# Get data on the first available Switch
switchboard.get_switch_data(ports[0])
