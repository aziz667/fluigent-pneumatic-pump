/*============================================================================*/
/*                         Fluigent / ESS API                                 */
/*----------------------------------------------------------------------------*/
/*         Copyright (c) Fluigent 2018.  All Rights Reserved.                 */
/*----------------------------------------------------------------------------*/
/*                                                                            */
/* Title:   ess_c.h                                                           */
/* Purpose: Include file to interact with the ESS API                         */
/* Version: 1.4.0	                                                      	  */
/*                                                                            */
/*============================================================================*/

#ifndef ess_cH
#define ess_cH

/* Name: 		ess_initialization
 *
 * Description:	initialize USB connection and launches a continuous check for a SWITCHBOARD with the specified serial number
 * Arguments:	serial is the serial number of the SWITCHBOARD for which  you want to open the session.  
 * Returns:		handle is the identifier of the opened session
 */
unsigned long long __stdcall ess_initialization(unsigned short serial);

/* Name: 		ess_close
 *
 * Description:	terminates threads and deallocates memory used by the ESS session
 * Arguments:	handle is the identifier of the ESS session 
 * Returns:		C error is the error code
 */
unsigned char __stdcall ess_close(unsigned long long handle);

/* Name: 		ess_get_serial
 *
 * Description:	get serial number and firmware version number
 * Arguments:	handle is the identifier of the ESS session
 * Returns:		C error is the error code
 *              serial is the SWITCHBOARD serial number associated to the ESS session specified by the connection handle
 *              version is the SWITCHBOARD firmware version number
 */
unsigned char __stdcall ess_get_serial(unsigned long long handle,
    								   unsigned short * serial,
                                       unsigned short * version);
									   
/* Name: 		ess_set_rot_switch
 *
 * Description:	rotate a specific M-SWITCH (0 to 3) to the desired position (0 to 9), with the specified direction OR
 *				rotate a specific L-SWITCH (0 to 3) to the desired position (0 to 1). ROT-SWITCH ports support both
 *				L and M switch models. 
 * Arguments:	handle is the identifier of the ESS session
 *              rot_switch is the number associated to the port letter: 0 for A, 1 for B, 2 for C and 3 for D
 *              position is the desired position
 *              direction is the rotation direction: 0 for shorter sense, 1 for anticlockwise and 2 for clockwise;
 *					(note: as L-SWITCH has only 2 positions, set 0 for direction)
 * Returns:		C error is the error code
 */
unsigned char __stdcall ess_set_rot_switch(unsigned long long handle,
    								   unsigned char rot_switch,
                                       unsigned char position,
                                       unsigned char direction);
									   
/* Name: 		ess_set_all_rot_switch
 *
 * Description:	rotate the all ROT-SWITCH to the desired position. ROT-SWITCH ports support both
 *				L and M switch models even if number of ports is different!
 * Arguments:	handle is the identifier of the ESS session
 *              position is the desired position: M-SWITCH range is from 0 to 9; L-SWITCH range is from 0 to 1
 *              direction is the rotation direction: 0 for shorter sense, 1 for anticlockwise and 2 for clockwise
 * Returns:		C error is the error code
 * Comment: 	If both L-SWITCH and M-SWITCH are used, use only 0 or 1 as position argument.
 */
unsigned char __stdcall ess_set_all_rot_switch(unsigned long long handle,
                                       unsigned char position,
                                       unsigned char direction);

/* Name: 		ess_set_two_switch
 *
 * Description:	set the specific 2-switch to the desired position (0 or 1)
 * Arguments:	handle is the identifier of the ESS session
 *              2_SWICTH is the port number
 *              position is the desired position
 * Returns:		C error is the error code
 */
unsigned char __stdcall ess_set_two_switch(unsigned long long handle,
    								   unsigned char two_switch,
                                       unsigned char position);

/* Name: 		ess_set_all_two_switch
 *
 * Description:	set all 2-SWITCH to the desired position (0 or 1)
 * Arguments:	handle is the identifier of the ESS session
 *              position is the desired position
 * Returns:		C error is the error code
 */
unsigned char __stdcall ess_set_all_two_switch(unsigned long long handle,
                                       unsigned char position);
									   
/* Name: 		ess_get_data_rot_switch
 *
 * Description:	get ROT-SWITCH data
 * Arguments:	handle is the identifier of the ESS session 
 *              rot_switch is the number associated to the port letter: 0 for A, 1 for B, 2 for C and 3 for D
 * Returns:		C error is the error code
 *              presence 0 <=> SWITCH not connected - presence 1 <=> ROT-SWITCH connected
 *              type 0 <=> ROT-SWITCH connected - type 1 <=> 2-SWITCH connected
 *              model EV model: 1 for M-SWITCH; 3 for L-SWITCH
 *              soft_vers ROT-SWITCH firmware version number
 *              err_code specific error code for ROT-SWITCH (cf. Documentation).
 *              processing 1 if rotating, 0 otherwise
 *              position ROT-SWITCH  position (last if processing)
 *              
 */
unsigned char __stdcall ess_get_data_rot_switch(unsigned long long handle,
										unsigned char rot_switch,
    								    unsigned char * presence,
                                        unsigned char * type,
                                        unsigned char * model,
                                        unsigned char * soft_vers,
                                        unsigned char * err_code,
                                        unsigned char * processing,
                                        unsigned char * position);
										
/* Name: 		ess_get_data_two_switch
 *
 * Description:	get 2-SWITCH data
 * Arguments:	handle is the identifier of the ESS session 
 *              2_SWICTH is the port number
 * Returns:		C error is the error code
 *              presence 0 <=> no 2-SWITCH connected - presence 1 <=> 2-SWITCH connected
 *              type 0 <=> ROT-SWITCH connected - 1 <=> 2-SWITCH connected
 *              model EV model: 0 for 2-SWITCH
 *              soft_vers 2-SWITCH firmware version number
 *              status 1 if the switch is ON, 0 otherwise
 *              origin : 0 if last moved by the software, 1 if last moved manually
 *              
 */
unsigned char __stdcall ess_get_data_two_switch(unsigned long long handle,
										unsigned char two_switch,
    								    unsigned char * presence,
                                        unsigned char * type,
                                        unsigned char * model,
                                        unsigned char * soft_vers,
                                        unsigned char * status,
                                        unsigned char * origin);
										
/* Name: 		ess_detect
 *
 * Description:	Detect the connected SWITCHBOAR and return their serial numbers in the table
 * Arguments:	 
 * Returns:		C error is the error code
 *              table is the array of all connected SWITCHBOARD
 */
unsigned char __stdcall ess_detect(unsigned short table[256]);

// return values

#define ESS_OK		    		0
#define ESS_ERR_USB_CLOSED	   	1
#define ESS_ERR_COMMAND	    	2
#define ESS_ERR_NO_SWITCH	    3
#define ESS_ERR_WRONG_SWITCH	4


#endif






