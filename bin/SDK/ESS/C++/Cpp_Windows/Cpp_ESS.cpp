/*============================================================================*/
/*                    Fluigent / Cpp ESS library Example                      */
/*----------------------------------------------------------------------------*/
/*         Copyright (c) Fluigent 2018.  All Rights Reserved.                 */
/*----------------------------------------------------------------------------*/
/*                                                                            */
/* Title:   Cpp_ESS.cpp										                  */
/* Purpose: Interact with Fluigent Easy Switch Solutions		      	      */
/* Version: 1.4.0                                                             */
/* Hardware set-up: one 2-SWITCH electro valve connected to a Switch Board on */
/*                    1st position and/or one ROT-SWITCH connected to position A*/
/*============================================================================*/

#include <cstdlib>
#include <iostream>
#include <windows.h>
//#include "ess_c.h"

/* Define functions prototype */
typedef unsigned long long(__stdcall *init)(int);
typedef int(__stdcall *serial)(unsigned long long, unsigned short *, unsigned short *);
typedef int(__stdcall *close)(unsigned long long);
typedef int(__stdcall *read2SWITCH)(unsigned long long handle, unsigned char two_switch, unsigned char * presence,
	unsigned char * type, unsigned char * model, unsigned char * soft_vers, unsigned char * status, unsigned char * origin);
typedef int(__stdcall *readROTSWITCH)(unsigned long long handle, unsigned char rot_switch, unsigned char * presence,
	unsigned char * type, unsigned char * model, unsigned char * soft_vers, unsigned char * err_code,
	unsigned char * processing, unsigned char * position);
typedef int(__stdcall *setROTSWITCH)(unsigned long long handle, unsigned char rot_switch, unsigned char position, unsigned char direction);

//===============================================================
//		Main function
//===============================================================
int main(int argc, char *argv[])
{
	/* Load DLL into memory */
	HINSTANCE ProcDLL_ESS;

#ifdef _WIN64
	ProcDLL_ESS = LoadLibrary(TEXT("ess_c_64.dll"));
#else
	ProcDLL_ESS = LoadLibrary(TEXT("ess_c_32.dll"));
#endif

	/* Declare pointers on dll functions */
	init dll_init;
	serial dll_serial;
	close dll_close;
	read2SWITCH dll_read2SWITCH;
	readROTSWITCH dll_readROTSWITCH;
	setROTSWITCH dll_setROTSWITCH;

	/* Link dll pointers with functions prototype */
	dll_init = (init)GetProcAddress(ProcDLL_ESS, "ess_initialization");
	dll_serial = (serial)GetProcAddress(ProcDLL_ESS, "ess_get_serial");
	dll_close = (close)GetProcAddress(ProcDLL_ESS, "ess_close");
	dll_read2SWITCH = (read2SWITCH)GetProcAddress(ProcDLL_ESS, "ess_get_data_two_switch");
	dll_readROTSWITCH = (readROTSWITCH)GetProcAddress(ProcDLL_ESS, "ess_get_data_rot_switch");
	dll_setROTSWITCH = (setROTSWITCH)GetProcAddress(ProcDLL_ESS, "ess_set_rot_switch");

	/* Define variables used for ESS */
	unsigned long long essHandle = 0;
	unsigned short Serial = 0;
	unsigned short Version = 0;
	unsigned char position = 0;
	unsigned char two_switch = 0;		// 2-SWITCH position (0 is the 1st on the SWITCHBOARD)
	unsigned char rot_switch = 0;		// ROT-SWITCH position (0 is the 1st, position 'A')

	/* SWITCH variables definition used by the shared library */
	unsigned char Presence;
	unsigned char Type;
	unsigned char Model;
	unsigned char Software_Version;
	unsigned char Status;
	unsigned char Origin;
	unsigned char Err_code;
	unsigned char Processing;
	unsigned char Position;

	unsigned int localCount = 0;

	if (ProcDLL_ESS != NULL) {							// If dll loaded
		std::cout << "ESS dll loaded" << std::endl;

		/* Initialize device */
		if (dll_init != NULL) {								// Check if function was properly linked to the dll file
			/* Initialize the first SWITCHBOARD in Windows enumeration list */
			essHandle = dll_init(0);
			std::cout << "ESS session initialized" << std::endl;
		}

		/* Read device serial number */
		if (dll_serial != NULL) {
			/*Get the serial number of the SWITCHBOARD*/
			dll_serial(essHandle, &Serial, &Version);
			std::cout << "SWITCH BOARD SN: " << Serial << std::endl;
		}

		/* Get status of the 2-SWITCH (N�1 on the board) */
		if (dll_read2SWITCH != NULL){
			dll_read2SWITCH(essHandle, two_switch, &Presence, &Type, &Model, &Software_Version, &Status, &Origin);
			if ((Presence == 1) && (Type == 1)){												// If 2-SWITCH detected
				std::cout << std::endl << "2-SWITCH detected." << std::endl;
				std::cout << "Model (1 if 2-SWITCH): " << int(Model) << std::endl;				// Display model
				std::cout << "Software version: " << int(Software_Version) << std::endl;		// Display software version
				if (Status == 1){																// Display 2-SWITCH status 
					std::cout << "Switch is ON." << std::endl;
				}
				else{ std::cout << "Switch is OFF." << std::endl; }
			}
			else{ std::cout << "No 2-SWITCH found on 1st position." << std::endl; }
		}

		/* Get status of the ROT-SWITCH (N�A on the board) */
		if (dll_readROTSWITCH != NULL){
			dll_readROTSWITCH(essHandle, rot_switch, &Presence, &Type, &Model, &Software_Version, &Err_code, &Processing, &Position);
			if ((Presence == 1) && (Type == 0)){												// If SWITCH detected
				std::cout << std::endl << "ROT-SWITCH detected." << std::endl;
				std::cout << "Model (1: M-SWITCH; 3: L-SWITCH): " << int(Model) << std::endl;	// Display model
				std::cout << "Software version: " << int(Software_Version) << std::endl;		// Display software version
				std::cout << "Switch position: " << int(Position) << std::endl;					// Display ROT-SWITCH position

				/* Change ROT-SWITCH position */
				if (Position == 0){																// SWITCH changes from position 1 to 0 or from 0 to 1
					dll_setROTSWITCH(essHandle, rot_switch, 1, 0);
				}
				else{
					dll_setROTSWITCH(essHandle, rot_switch, 0, 0);
				}
				std::cout << "ROT-SWITCH position changed!" << std::endl;

				/* Wait for processing flag to be active */
				do {
					dll_readROTSWITCH(essHandle, rot_switch, &Presence, &Type, &Model, &Software_Version, &Err_code, &Processing, &Position);
					Sleep(100);
				} while (Processing != 1);
				Sleep(100);

				/* Wait for processing flag to be down */
				do{
					dll_readROTSWITCH(essHandle, rot_switch, &Presence, &Type, &Model, &Software_Version, &Err_code, &Processing, &Position);
					Sleep(100);
				} while (Processing == 1);
				Sleep(100);

				// Read new position after change
				dll_readROTSWITCH(essHandle, rot_switch, &Presence, &Type, &Model, &Software_Version, &Err_code, &Processing, &Position);
				std::cout << "New Switch position: " << int(Position) << std::endl;					// Display ROT-SWITCH position
			}
			else{ std::cout << "No ROT-SWITCH found on position 'A'." << std::endl; }
		}

		/* Close ESS session */
		if (dll_close != NULL) {
			dll_close(essHandle);
			std::cout << std::endl << "ESS session closed" << std::endl;
		}
	}

	/* Release the DLL */
	FreeLibrary(ProcDLL_ESS);
	std::cout << "ESS dll unloaded" << std::endl;

	/* Exit application */
	system("PAUSE");

	return EXIT_SUCCESS;
}


