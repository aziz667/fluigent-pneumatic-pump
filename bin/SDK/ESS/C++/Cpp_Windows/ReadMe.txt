﻿===========================================================================
    CONSOLE APPLICATION : Cpp_ESS
===========================================================================
This file contains a summary of what you will find in each of the files that make up Cpp_ESS application.

Cpp_ESS.vcxproj
    This is the main project file for VC++ projects. It contains information about the version of Visual C++ used to generate the file and information about the platforms, configurations, and project features.

Cpp_ESS.vcxproj.filters
    This is the filter file for VC++ projects. It contains information about the association between files in your project and filters. This association is used in the IDE to display the grouping of files with similar extensions on a specific node (eg, files. "Cpp" are associated with the filter "Source Files").

Cpp_ESS.cpp
   This is the main source file for the application.

ess_c_32.dll
   This is the 32bit shared library to interact with the Fluigent ESS device	

ess_c_64.dll
   This is the 64bit shared library to interact with the Fluigent ESS device	

ess_c_32.h
   This is the header file which defines the functions used within the DLL file