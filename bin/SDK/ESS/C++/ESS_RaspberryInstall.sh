#!/bin/sh
if [ "$(whoami)" != "root" ]; then
	su -c "$0" root
	exit
fi
echo "Extracting ESS library..."
tar -xvzf ./Shared/Raspberry/pi_libess_lib.so.tar.gz -C /usr/lib
ldconfig -l /usr/lib/libess_lib.so.2.0.0
echo "Copy of 99-hid.rules..."
cp ./Shared/Raspberry/99-hid.rules /etc/udev/rules.d/
echo "Compilation of example applications..."
g++ -I. -o Cpp_ESS_2-SWITCH_Series_Application ./Cpp_Linux/Cpp_ESS_2-SWITCH_Series.cpp -less_lib
g++ -I. -o Cpp_ESS_ROT-SWITCH_Series_Application ./Cpp_Linux/Cpp_ESS_ROT-SWITCH_Series.cpp -less_lib
