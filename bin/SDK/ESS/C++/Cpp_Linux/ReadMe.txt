﻿===========================================================================
    CONSOLE APPLICATIONS : Cpp_ESS_2-SWITCH_Series_Application,
			   Cpp_ESS_ROT-SWITCH_Series_Application
===========================================================================

This file contains a summary of the switchboard C++ series example application.

-------------------------- HARDWARE ---------------------------------------
An ESS Switchboard is required in order to run the example.
By default, the 2-SWITCH example looks for a 2-SWITCH on port 1 of the Switchboard (this can be changed in the code). The software reads then changes switch position.
The ROT-SWITCH example looks for a ROT-SWITCH (L or M model) on port A. The software reads then changes switch position.

-------------------------- SOFTWARE ---------------------------------------
The software was implemented and tested on Ubuntu 18.04 64-bit version.

--------------------------- FILES ----------------------------------------
List of project files:

Cpp_ESS_2-SWITCH_Series.cpp
    This is the main source file for the 2-SWITCH use case application.
	
Cpp_ESS_2-SWITCH_Series_Application
	This is the compiled 64-bit application. It can be directly executed in a 64-Bit Linux system.

Cpp_ESS_ROT-SWITCH_Series.cpp
    This is the main source file for the ROT-SWITCH (L or M model) use case application.
	
Cpp_ESS_ROT-SWITCH_Series_Application
	This is the compiled 64-bit application. It can be directly executed in a 64-Bit Linux system.

ess_c.h
	This file is the header of shared object library. Use functions prototype to interact with the ESS.
