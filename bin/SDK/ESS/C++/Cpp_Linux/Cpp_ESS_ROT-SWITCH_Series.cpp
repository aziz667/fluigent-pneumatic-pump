/*============================================================================*/
/*                    Fluigent / Cpp ESS library Example                      */
/*----------------------------------------------------------------------------*/
/*         Copyright (c) Fluigent 2018.  All Rights Reserved.                 */
/*----------------------------------------------------------------------------*/
/*                                                                            */
/* Title:   Cpp_ESS_ROT-SWITCH_Series.cpp                                     */
/* Purpose: Interact with Fluigent Easy Switch Solutions		      	      */
/* Version: 1.3                                                               */
/* Hardware set-up: one ROT-SWITCH electro valve connected to a Switch Board  */
/*                   on 1st position                                          */
/*============================================================================*/

#include "ess_c.h"
#include <iostream>
#include <iomanip>
#include <string>
#include <cstdlib>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>             // Use of sleep() function

#define True 1
#define False 0

#define SUCCESS 1
#define FAIL 0

typedef long unsigned int UL;
typedef unsigned short US;
typedef char S[];
typedef unsigned char C;
typedef float F;
typedef char B;

/*============================================================================*/
/* Function name:   exit_application()                                        */
/* Purpose:         releases resources (ESS session) and exits application   */
/* Arguments:       BOOLEAN SUCCESS/FAIL: end state of the application        */
/* Returns:         N/A                                                       */
/*============================================================================*/
int exit_application(bool state, UL Handle)
{
    B B_OK;
    std::cout << "Releasing USB resource." << std::endl;
    B_OK=ess_close(Handle);	    //Close ESS session
    if (B_OK==SUCCESS){
	printf("\nPress 'ENTER' to continue.");      
        getchar();   	   	    //Wait for user to press Enter
        return EXIT_SUCCESS;        //Returns global state of the application as success
    }                               //Note: if used by other application(s), global result status can also be used for control
    else{
        return EXIT_FAILURE;        //Returns global state of the application as failed
    }
}


/*============================================================================*/
/* 				Main function	     	     	     	     	     	      */
/*============================================================================*/
int main(int argc, char *argv[])
{
    /* System settings variable definition */
    C position = 0;		// Connected ROT-SWITCH on 1st position of the Switch Board
						// "A" position on the board equals to "0" (first index value)
    /* Script general variables definition */
    UL essHandle=0;
    US sysSerial=0;
    US sysVersion=0;
    C C_error;
    B B_OK;

    /* ROT-SWITCH variables definition used by the shared library */
    C myPresence;
    C myType;
    C myModel;
    C mySoftware_Version;
	C myErr_code;
	C myProcessing;
	C myPosition;
	C myNewPosition;

    std::cout << "FLUIGENT: C++ ESS ROT-SWITCH Series example." << std::endl;		// Display on Terminal

    /* Open ESS session for the first enumerated Switch Board device */
    essHandle=ess_initialization(0);
    std::cout << "ESS session handle: " << essHandle << std::endl;		// Display session handle

    /* Get serial number of the ESS associated to the opened session */
    C_error = ess_get_serial(essHandle, &sysSerial, &sysVersion);
    if (C_error == 0){								// Manage returned status of called library function
      std::cout << "Device serial number: " << sysSerial << std::endl;
      std::cout << "Device version: " << sysVersion << std::endl;
    }else{
      std::cout << "Error getting device serial number (1: USB closed; 2: Wrong command; 3: No switch; 4: Wrong switch): " << int(C_error) << std::endl;
    }

	/* Get status on the 1st position switch */
	std::cout << "Reading ROT-SWITCH data on 1st position (of switch board)." << std::endl;
	C_error = ess_get_data_rot_switch(essHandle, position, &myPresence, &myType, &myModel, &mySoftware_Version, &myErr_code, &myProcessing, &myPosition);
	if ((C_error == 0) && (myPresence == 1) && (myType == 0)){								// If ROT-SWITCH detected and no error occurred 
		std::cout << "ROT-SWITCH connected" << std::endl;
		std::cout << "Model (1 if M-SWITCH; 3 if L-SWITCH): " << int(myModel) << std::endl;	// Display model
		std::cout << "Software version: " << int(mySoftware_Version) << std::endl;			// Display software version
		std::cout << "Switch position: " << int(myPosition) << std::endl;					// Display ROT-SWITCH position
	} else {																				// If no sensor detected, or incorrect type detected
		std::cout << "No ROT-SWITCH found at this position." << std::endl;
	}
	
	/* Change 1st switch status (rotate for 1 step) */
	if ((myPresence == 1) && (myType == 0)){										// If a ROT-SWITCH is plugged on 1st board position
		std::cout << "Press ENTER to change switch position..." << std::endl;
		getchar();																	// Wait for the user to press "Enter"
		myNewPosition = myPosition+1;												// Increment position using previous value (from ess_get_data_rot_switch function)
		if (myNewPosition>1){ myNewPosition = 0; }									// Check if position value is 0 or 1 
		C_error = ess_set_rot_switch (essHandle, position, myNewPosition, 0);			// Set new ROT-SWITCH position
		sleep(1);																	// Wait some time for the mechanical delay of the switch
		C_error = ess_get_data_rot_switch(essHandle, position, &myPresence, &myType, &myModel, &mySoftware_Version, &myErr_code, &myProcessing, &myPosition);
		std::cout << "Switch new position: " << int(myPosition) << std::endl;			// Display ROT-SWITCH position
	}
	
    /* Exit application */
    return exit_application(SUCCESS,essHandle);
}
