/*============================================================================*/
/*           Fluigent / LineUP Object interface to Middleware                        */
/*----------------------------------------------------------------------------*/
/*         Copyright (c) Fluigent 2018.  All Rights Reserved.                 */
/*----------------------------------------------------------------------------*/
/*                                                                            */
/* Title:   LineUP low level.cpp											  */
/* Purpose: Interact with Fluigent LineUP interface using Windows Middleware.	      */
/*			Hardware componenets are modelised by objects.  				  */
/*			Please read documentation for architecture details.				  */
/* Version: 18.0.0                                                            */
/* Library version: 1.0.2.2													  */
/*                                                                            */
/*============================================================================*/

#include "LineUP low level.h"


////////////////////////////////////////////////////////////////////////////////
/// ... High level abstract instruments ...
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// ... iInstrument : simple generic instrument high level class ...
////////////////////////////////////////////////////////////////////////////////
iInstrument::iInstrument(){
	SN = 0;
	Version = 0;
	Handle = 0;
}

iInstrument::~iInstrument()
{
}

////////////////////////////////////////////////////////////////////////////////
/// ... iLink : simple generic Link instrument high level class ...
////////////////////////////////////////////////////////////////////////////////
iLink::iLink(){
}

iLink::~iLink()
{
}

////////////////////////////////////////////////////////////////////////////////
/// ... iModule : simple generic module instrument high level class ...
////////////////////////////////////////////////////////////////////////////////
iModule::iModule(){
	this->Module = 0;
};

iModule::~iModule(){
}

////////////////////////////////////////////////////////////////////////////////
/// ... --------------------- Low level instruments ------------------------ ...
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// ... Controller: Link Controller class ...
////////////////////////////////////////////////////////////////////////////////

/**
* \brief Default empty constructor of Link controller
*/
Controller::Controller()
{
	TTL_MODE_1 = TTL_MODE_2 = TTL_MODE::DETECT_RISING_EDGE;
}

/**
 * \brief Constructor of Link controller
 * Load dll functions and initialize communication with the device.
 * \param serial 
 */
Controller::Controller(unsigned short serial){
	// Variables definition
	unsigned short localSerial = 0;
	unsigned short localVersion = 0;
	TTL_MODE_1 = TTL_MODE_2 = TTL_MODE::DETECT_RISING_EDGE;

	// Load dll file according to application bitness
#ifdef WIN64
	this->ProcDLL = LoadLibrary(TEXT("LineUP_c_64.dll"));
#else
	this->ProcDLL = LoadLibrary(TEXT("LineUP_c_32.dll"));
#endif	// WIN32
	
	//==============================================================================
	// Get a pointer to each function and cast function pointers to the types defined
	//==============================================================================
	this->plineup_detect = (lineup_detect)GetProcAddress(this->ProcDLL, "lineup_detect");
	this->plineup_initialization = (lineup_initialization)GetProcAddress(this->ProcDLL, "lineup_initialization");
	this->plineup_get_serial = (lineup_get_serial)GetProcAddress(this->ProcDLL, "lineup_get_serial");
	this->plineup_get_module_number = (lineup_get_module_number)GetProcAddress(this->ProcDLL, "lineup_get_module_number");
	this->plineup_get_module_info = (lineup_get_module_info)GetProcAddress(this->ProcDLL, "lineup_get_module_info");
	this->plineup_power = (lineup_power)GetProcAddress(this->ProcDLL, "lineup_power");
	this->plineup_set_ttl_mode = (lineup_set_ttl_mode)GetProcAddress(this->ProcDLL, "lineup_set_ttl_mode");
	this->plineup_get_ttl_state = (lineup_get_ttl_state)GetProcAddress(this->ProcDLL, "lineup_get_ttl_state");
	this->plineup_trig_ttl = (lineup_trig_ttl)GetProcAddress(this->ProcDLL, "lineup_trig_ttl");
	this->plineup_close = (lineup_close)GetProcAddress(this->ProcDLL, "lineup_close");

	if (this->ProcDLL != NULL){
		// Opens LineUP session with selected serial number USB device. If serial equals to 0, first found device will be used. 
		this->Handle = this->plineup_initialization(serial);

		// If no serial number is recovered, instrument is not detected
		this->plineup_get_serial(this->Handle, &localVersion, &localSerial);
		if (localSerial == 0)
			this->manageReturnCode(1, "Link");
		else
		{
			this->Version = localVersion;
			this->SN = localSerial;
		}
	}
	else{
		this->manageReturnCode(1, "DLL file not found");
	}
}

/**
 * \brief Class destructor
 * Free ressources
 */
Controller::~Controller(){
		if (this->Handle != 0)
			plineup_close(this->Handle);			// Close USB session
		if (this->ProcDLL != NULL)					// Free dynamic library ressource
			FreeLibrary(this->ProcDLL);
}

/**
 * \brief Link Serial Number
 * Overloaded function for dynamic update
 * \return serial number
 */
unsigned short Controller::Serial(void)
{
	// Variables definition
	unsigned short localSerial = 0;
	unsigned short localVersion = 0;
	unsigned char localError = 0;
	localError = this->plineup_get_serial(this->Handle, &localVersion, &localSerial);
	this->manageReturnCode(localError, "Serial");
	this->SN = localSerial;
	return localSerial;
}

/**
* \brief Link Version Number
* Overloaded function for dynamic update
* \return version number
*/
unsigned short Controller::VersionNumber(void)
{
	// Variables definition
	unsigned short localSerial = 0;
	unsigned short localVersion = 0;
	unsigned char localError = 0;
	localError = this->plineup_get_serial(this->Handle, &localVersion, &localSerial);
	this->manageReturnCode(localError, "VersionNumber");
	this->Version = localVersion;
	return localVersion;
}

/**
 * \brief Set operating mode of TTL connectors
 * \param port 
 * \param mode 
 */
void Controller::SetTTLMode(TTL_PORT port, TTL_MODE mode){
	unsigned char localError = 0;
	if (port == TTL_PORT::TTL1)
		this->TTL_MODE_1 = mode;
	if (port == TTL_PORT::TTL2)
		this->TTL_MODE_2 = mode;
	localError = this->plineup_set_ttl_mode(this->Handle, unsigned char(this->TTL_MODE_1), unsigned char(this->TTL_MODE_2));
	this->manageReturnCode(localError, "SetTTLMode");
}

/**
 * \brief Get TTL state
 * \param TTL1 
 * \param TTL2 
 */
void Controller::ReadTTL(bool *TTL1, bool *TTL2){
	unsigned char localTTL1;
	unsigned char localTTL2;
	unsigned char localError = 0;
	localError = this->plineup_get_ttl_state(this->Handle, 1, &localTTL1, &localTTL2);
	*TTL1 = localTTL1 & 0x01;
	*TTL2 = localTTL2 & 0x01;
	this->manageReturnCode(localError, "ReadTTL");
}

/**
 * \brief Trigger TTL1 or TTL2 if set to output
 * \param port 
 */
void Controller::TriggerTTL(TTL_PORT port){
	unsigned char localError = 0;
	if (port == TTL_PORT::TTL1)
		localError = this->plineup_trig_ttl(this->Handle, 0);
	if (port == TTL_PORT::TTL2)
		localError = this->plineup_trig_ttl(this->Handle, 1);
	this->manageReturnCode(localError, "TriggerTTL");
}

/**
 * \brief Get total number of detected channels connected to the Link
 * \return modules number
 */
unsigned char Controller::ModulesNumber(void){
	unsigned char localStatus;
	unsigned char localModuleNumber;
	unsigned char localError = 0;
	localError = this->plineup_get_module_number(this->Handle, &localStatus, &localModuleNumber);
	this->manageReturnCode(localError, "ModulesNumber");
	return localModuleNumber;
}

/**
 * \brief Get Link device power state
 * \return power state
 */
POWER_STATE Controller::Power(void){
	unsigned char localStatus;
	unsigned char localModuleNumber;
	unsigned char localError = 0;
	localError = this->plineup_get_module_number(this->Handle, &localStatus, &localModuleNumber);
	this->manageReturnCode(localError, "Power get");
	return POWER_STATE(localStatus);
}

/**
 * \brief Set Link device power on or off
 * \param power 
 */
void Controller::Power(POWER_STATE power){
	unsigned char localError = 0;
	localError = this->plineup_power(this->Handle, unsigned char(power));
	this->manageReturnCode(localError, "Power set");
}


////////////////////////////////////////////////////////////////////////////////
/// ... This function displays an error message in case of LineUP returned status 
/// ... is not Ok. This is mandatory
/// ... Arguments:	functionName is the string name of called function
/// ...			errorCode is the returned code of called function 
/// ... Returns:	void
////////////////////////////////////////////////////////////////////////////////
void Controller::manageReturnCode(unsigned char code, char *functionName){
	switch (code){
	case 0:		// No error
		break;
	case 1:		// Error USB closed
		std::cout << functionName << "\tError Link USB closed" << std::endl;
		break;
	case 2:		// Error command
		std::cout << functionName << "\tError Link wrong command" << std::endl;
		break;
	case 3:		// Error no module
		std::cout << functionName << "\tError Link no module at this index" << std::endl;
		break;
	case 4:		// Error wrong module
		std::cout << functionName << "\tError Link wrong module" << std::endl;
		break;
	case 5:		// Error module sleep
		std::cout << functionName << "\tError Module is in sleep mode" << std::endl;
		break;
	default:
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////
/// ... Channel: Flow EZ ...
////////////////////////////////////////////////////////////////////////////////

/**
* \brief Default empty constructor of Flow EZ channel
*/
Channel::Channel()
{
	this->FlowUnit = uLperMin;
	this->PressureUnit = mbar;
}

/**
 * \brief Flow EZ channel Constructor
 * \param Handle 
 * \param module 
 */
Channel::Channel(unsigned long long Handle, unsigned char module, HINSTANCE ProcDLL){
	unsigned short localSerial = 0;
	unsigned short localVersion = 0;
	unsigned char localType = 0;

	this->ProcDLL = ProcDLL;

	//==============================================================================
	// Get a pointer to each function and cast function pointers to the types defined
	//==============================================================================
	this->plineup_get_module_info = (lineup_get_module_info)GetProcAddress(this->ProcDLL, "lineup_get_module_info");
	this->pflowez_read_module = (flowez_read_module)GetProcAddress(this->ProcDLL, "flowez_read_module");
	this->pflowez_module_config = (flowez_module_config)GetProcAddress(this->ProcDLL, "flowez_module_config");
	this->pflowez_set_pressure = (flowez_set_pressure)GetProcAddress(this->ProcDLL, "flowez_set_pressure");
	this->pflowez_set_flowrate = (flowez_set_flowrate)GetProcAddress(this->ProcDLL, "flowez_set_flowrate");
	this->pflowez_get_flowcontrol_status = (flowez_get_flowcontrol_status)GetProcAddress(this->ProcDLL, "flowez_get_flowcontrol_status");
	this->pflowez_set_mode = (flowez_set_mode)GetProcAddress(this->ProcDLL, "flowez_set_mode");
	this->pflowez_pressure_reset = (flowez_pressure_reset)GetProcAddress(this->ProcDLL, "flowez_pressure_reset");
	this->pflowez_set_q_calibtable = (flowez_set_q_calibtable)GetProcAddress(this->ProcDLL, "flowez_set_q_calibtable");

	// Internal variables initialization
	this->Handle = Handle;
	this->Module = module;
	this->FlowUnit = uLperMin;
	this->PressureUnit = mbar;

	// Get device information
	this->plineup_get_module_info(Handle, module, &localType, &localVersion, &localSerial);

	this->SN = localSerial;
	this->Version = localVersion;
}

/**
 * \brief Flow EZ channel destructor
 */
Channel::~Channel(){
}

/**
 * \brief Instrument Serial Number
 * \return serial number
 */
unsigned short Channel::Serial(void){
	unsigned short localSerial = 0;
	unsigned short localVersion = 0;
	unsigned char localType = 0;
	unsigned char localError = 0;
	localError = this->plineup_get_module_info(this->Handle, this->Module, &localType, &localVersion, &localSerial);
	this->manageReturnCode(localError, "Serial get");
	return localSerial;
}

/**
 * \brief Instrument firmware version
 * \return version number
 */
unsigned short Channel::VersionNumber(void){
	unsigned short localSerial = 0;
	unsigned short localVersion = 0;
	unsigned char localType = 0;
	unsigned char localError = 0;
	localError = this->plineup_get_module_info(this->Handle, this->Module, &localType, &localVersion, &localSerial);
	this->manageReturnCode(localError, "VersionNumber get");
	return localVersion;
}

/**
 * \brief Instrument Status
 * \return Flow EZ status mask:
 * 0x01 - MSK_RUN � Power state of the module
 * 0x02 - MSK_LOCAL � Module control local or software
 * 0x04 - MSK_REGUL  - If pressure control regulating
 * 0x08 - MSK_STABLE � Pressure mode fast or smooth
 * 0x10 - MSK_UNDERP � Input pressure is too low
 * 0x20 - MSK_OVERP � Input pressure is too high
 * 0x80 - MSK_QDETECT � If flow-unit connected
 */
unsigned char Channel::Status()
{
	float localPressure;
	float localFlowRate;
	unsigned char localTimeStamp;
	unsigned char localStatus = 0;
	unsigned char localError = 0;
	localError = this->pflowez_read_module(this->Handle, this->Module, &localStatus, &localPressure, &localFlowRate, &localTimeStamp);
	this->manageReturnCode(localError, "Status get");
	return localStatus;
}

/**
 * \brief Get pressure maximal range in set PRESSURE_UNIT
 * \return pressure range max
 */
float Channel::PressureRangeMax(void){
	float localPressureMax;
	float localFlowRateMax;
	unsigned char localQtable;
	unsigned char localError = 0;
	unsigned char localQArtCode[32] = { 0 };

	localError = this->pflowez_module_config(this->Handle, this->Module, &localPressureMax, &localFlowRateMax, &localQtable, localQArtCode);
	this->manageReturnCode(localError, "PressureRangeMax");
	return abs(ConvertPressureFromMbar(this->PressureUnit, localPressureMax));
}

/**
 * \brief Get pressure minimal range in set PRESSURE_UNIT
 * \return pressure range min
 */
float Channel::PressureRangeMin(void){
	return 0;	// not implemented for Flow EZ
}

/**
 * \brief Get flow-rate maximal range in set FLOW_UNIT
 * \return flow-rate max range
 */
float Channel::FlowrateRangeMax(void){
	float localPressureMax;
	float localFlowRateMax;
	unsigned char localQtable;
	unsigned char localError = 0;
	unsigned char localQArtCode[32] = { 0 };

	localError = this->pflowez_module_config(this->Handle, this->Module, &localPressureMax, &localFlowRateMax, &localQtable, localQArtCode);
	this->manageReturnCode(localError, "FlowrateRangeMax");
	return ConvertFlowFromUlperMin(this->FlowUnit, localFlowRateMax);
}

/**
 * \brief Get flow-rate minimal range in set FLOW_UNIT
 * \return flow-rate min range
 */
float Channel::FlowrateRangeMin(void){
	float localPressureMax;
	float localFlowRateMax;
	unsigned char localQtable;
	unsigned char localError = 0;
	unsigned char localQArtCode[32] = { 0 };

	localError = this->pflowez_module_config(this->Handle, this->Module, &localPressureMax, &localFlowRateMax, &localQtable, localQArtCode);
	this->manageReturnCode(localError, "FlowrateRangeMin");
	return - ConvertFlowFromUlperMin(this->FlowUnit, localFlowRateMax);
}


/// Functions - read/write control
/**
* \brief Read pressure value in set PRESSURE_UNIT
* \return pressure
*/
float Channel::Pressure(void){
	float localPressure;
	float localFlowRate;
	unsigned char localTimeStamp;
	unsigned char localStatus = 0;
	unsigned char localError = 0;

	localError = this->pflowez_read_module(this->Handle, this->Module, &localStatus, &localPressure, &localFlowRate, &localTimeStamp);
	this->manageReturnCode(localError, "Pressure get");
	return ConvertPressureFromMbar(this->PressureUnit, localPressure);
}

/**
 * \brief Set pressure value in set PRESSURE_UNIT.	
 * Regulate the pressure of the Flow EZ Module and enter remote control
 * \param command 
 */
void Channel::Pressure(float command){
	unsigned char localError = 0;
	localError = this->pflowez_set_pressure(this->Handle, this->Module, ConvertPressureToMbar(this->PressureUnit, command));
	this->manageReturnCode(localError, "Pressure set");
}

/**
 * \brief Read flow-rate value in set FLOW_UNIT
 * \return flow-rate
 */
float Channel::FlowRate(void){	
	float localPressure = 0;
	float localFlowRate = 0;
	unsigned char localTimeStamp;
	unsigned char localStatus = 0;
	unsigned char localError = 0;
	localError = this->pflowez_read_module(this->Handle, this->Module, &localStatus, &localPressure, &localFlowRate, &localTimeStamp);
	this->manageReturnCode(localError, "FlowRate get");
	return ConvertFlowFromUlperMin(this->FlowUnit, localFlowRate);
}

/**
 * \brief Set flow-rate value in set FLOW_UNIT.
 * Regulate the flow-rate of the Flow EZ Module and enter remote control
 * \param command 
 */
void Channel::FlowRate(float command){
	unsigned char localError = 0;
	localError = this->pflowez_set_flowrate(this->Handle, this->Module, ConvertFlowToUlperMin(this->FlowUnit, command));
	this->manageReturnCode(localError, "FlowRate set");
}

/**
 * \brief Read status of flow-rate regulation state
 * \return status of flow-rate control; 0 = OK, 1 = error
 */
unsigned char Channel::FlowrateControlStatus(void){
	unsigned char localError = 0;
	unsigned char status = 0;
	localError = this->pflowez_get_flowcontrol_status(this->Handle, this->Module, &status);
	this->manageReturnCode(localError, "FlowrateControlStatus");
	return status;
}

/**
 * \brief Read flow unit sensor calibration table:
 * (0:H20; 1:IPA; 2:HFE; 3:FC40; 4:OIL) for M dual calibration type; (0:H20; 1:IPA) for XS, S, L and XL flow - units
 * \return flow unit sensor calibration table 
 */
FLOW_UNIT_CALIBRATION_TABLE Channel::FlowCalibrationTable(void){
	float localPressureMax;
	float localFlowRateMax;
	unsigned char localQtable;
	unsigned char localQArtCode[32] = { 0 };
	unsigned char localError = 0;
	localError = this->pflowez_module_config(this->Handle, this->Module, &localPressureMax, &localFlowRateMax, &localQtable, localQArtCode);
	this->manageReturnCode(localError, "FlowCalibrationTable get");
	return FLOW_UNIT_CALIBRATION_TABLE(localQtable);
}

/**
 * \brief Set flow unit sensor calibration table
 * (0:H20; 1:IPA; 2:HFE; 3:FC40; 4:OIL) for M dual calibration type; (0:H20; 1:IPA) for XS, S, L and XL flow - units
 * \param unit 
 */
void Channel::FlowCalibrationTable(FLOW_UNIT_CALIBRATION_TABLE unit){
	unsigned char localError = 0;
	localError = this->pflowez_set_q_calibtable(this->Handle, this->Module, unsigned char(unit));
	this->manageReturnCode(localError, "FlowCalibrationTable set");
}

/**
 * \brief Get pressure controller mode
 * FAST or SMOOTH
 * \return pressure mode; 0 = fast = use of the TOR, 1 = smooth = no use of TOR
 */
PRESSURE_MODE Channel::PressureMode(void){
	float localPressure = 0;
	float localFlowRate = 0;
	unsigned char localTimeStamp;
	unsigned char localStatus = 0;
	unsigned char localError = 0;
	localError = this->pflowez_read_module(this->Handle, this->Module, &localStatus, &localPressure, &localFlowRate, &localTimeStamp);
	this->manageReturnCode(localError, "PressureMode get");
	return PRESSURE_MODE((localStatus & 0x08) == 0x08);
}

/**
 * \brief Set actual pressure controller mode
 * FAST or SMOOTH
 * \param pressure mode; 0 = fast = use of the TOR, 1 = smooth = no use of TOR
 */
void Channel::PressureMode(PRESSURE_MODE mode){
	unsigned char localError = 0;
	localError = this->pflowez_set_mode(this->Handle, this->Module, unsigned char(mode));
	this->manageReturnCode(localError, "PressureMode set");
}

/// Functions - Specific
/**
 * \brief Send a calibration signal
 * Save current pressure as zero sensor value
 * Module is not operational for next 5 seconds
 */
void Channel::CalibratePressureSensor(void){
	this->manageReturnCode(this->pflowez_pressure_reset(this->Handle, this->Module), "CalibratePressureSensor");
}

/**
* \brief Get type of the flow unit and available calibration table(s)
* \return FLOW_UNIT_TYPE
*/
FLOW_UNIT_TYPE Channel::FlowUnitType(void){
	float localPressureMax;
	float localFlowRateMax;
	unsigned char localQtable;
	unsigned char localQArtCode[32] = { 0 };
	unsigned char localError = 0;
	FLOW_UNIT_TYPE localType = FLOW_UNIT_TYPE::None;
	localError = this->pflowez_module_config(this->Handle, this->Module, &localPressureMax, &localFlowRateMax, &localQtable, localQArtCode);
	this->manageReturnCode(localError, "FlowUnitArticleCode get");

	// Get sensor type from sensor article code
	if ((std::find(localQArtCode, localQArtCode + 17, 'S')) != (localQArtCode + 17))		
	{
		if ((std::find(localQArtCode, localQArtCode + 17, 'X')) != (localQArtCode + 17))		// XS
			localType = FLOW_UNIT_TYPE::XS_single;
		else
		{
			if ((std::find(localQArtCode, localQArtCode + 17, 'I')) != (localQArtCode + 17))	// S IPA
				localType = FLOW_UNIT_TYPE::S_dual;
			else 																				// S simple
				localType = FLOW_UNIT_TYPE::S_single;
		}
	}
	if ((std::find(localQArtCode, localQArtCode + 17, 'M')) != (localQArtCode + 17))
	{
		if ((std::find(localQArtCode, localQArtCode + 17, 'I')) != (localQArtCode + 17))		// M IPA
			localType = FLOW_UNIT_TYPE::M_dual;
		else 																					// M simple
			localType = FLOW_UNIT_TYPE::M_single;
	}
	if ((std::find(localQArtCode, localQArtCode + 17, 'L')) != (localQArtCode + 17))
	{
		if ((std::find(localQArtCode, localQArtCode + 17, 'X')) != (localQArtCode + 17))		// XL
			localType = FLOW_UNIT_TYPE::XL_single;
		else
		{
			if ((std::find(localQArtCode, localQArtCode + 17, 'I')) != (localQArtCode + 17))	// L IPA
			localType = FLOW_UNIT_TYPE::L_dual;
			else 																				// L simple
			localType = FLOW_UNIT_TYPE::L_single;
		}
	}
	return localType;
}

////////////////////////////////////////////////////////////////////////////////
/// ... This function displays an error message in case of Flow EZ returned status  
/// ... is not Ok. This is mandatory
/// ... Arguments:	functionName is the string name of called function
/// ... 		errorCode is the returned code of called function 
/// ... Returns:	void ...
////////////////////////////////////////////////////////////////////////////////
void Channel::manageReturnCode(unsigned char code, char *functionName){
	switch (code){
	case 0:		// No error
		break;
	case 1:		// Error USB closed
		std::cout << functionName << " at index " << int(this->Module) << "\tError Link USB closed" << std::endl;
		break;
	case 2:		// Error command
		std::cout << functionName << " at index " << int(this->Module) << "\tError Link wrong command" << std::endl;
		break;
	case 3:		// Error no module
		std::cout << functionName << " at index " << int(this->Module) << "\tError Link no module at this index" << std::endl;
		break;
	case 4:		// Error wrong module
		std::cout << functionName << " at index " << int(this->Module) << "\tError Link wrong module" << std::endl;
		break;
	case 5:		// Error module sleep
		std::cout << functionName << " at index " << int(this->Module) << "\tError Module is in sleep mode" << std::endl;
		break;
	default:
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////
/// ... Instrument: LineUP Detect 
/// Detect all connected LineUP Link module(s) and return a table of serial numbers. 
/// If one Link is connected, first value of the table is the board serial number; 
/// last values of the table are set to 0. If two or more Link are connected, 
/// first values of the returned table are the serial numbers of the devices 
/// (depends on Windows detection order) ...
////////////////////////////////////////////////////////////////////////////////
void LineUPDetect(unsigned short serialTable[256]){
	HINSTANCE localProcDLL = NULL;								// Create an instance which will handle the dynamic library
	int i, j;
	unsigned short temp;

	lineup_detect plineup_detect;

	// Load dll file according to application bitness
#ifdef WIN64	// WIN64
	localProcDLL = LoadLibrary(TEXT("LineUP_c_64.dll"));
#else			// WIN32
	localProcDLL = LoadLibrary(TEXT("LineUP_c_32.dll"));
#endif	

	plineup_detect = (lineup_detect)GetProcAddress(localProcDLL, "lineup_detect");
	plineup_detect(serialTable);							// Get serial number array of connected Link devices 

	// Sort table
	for (i = 0; i < 255; i++)
	{
		for (j = 0; j < 255 - i; j++)
		{
			if ((serialTable[j]<serialTable[j + 1]) && (serialTable[j]!=0))
			{
				temp = serialTable[j];
				serialTable[j] = serialTable[j + 1];
				serialTable[j + 1] = temp;
			}
		}
	}

	if (localProcDLL != NULL)								// Free ressources (dll)
		FreeLibrary(localProcDLL);
}


////////////////////////////////////////////////////////////////////////////////
/// ... Unit Converter Tool: convert pressure or flow-rate values between different units ...
////////////////////////////////////////////////////////////////////////////////

/**
* \brief Convert pressure value from mbar to a selected PRESSURE_UNIT
* Default read unit is mbar.
* \param out PRESSURE_UNIT
* \param value
* \return converted value
*/
float ConvertPressureFromMbar(PRESSURE_UNIT out, float value){
	switch (out){
	case mbar:
		return value;
		break;
	case kPa:
		return value / 10;
		break;
	case PSI:
		return value / float(68.9476);
		break;
	default:
		return value;
		break;
	};
}

/**
* \brief Convert pressure value to mbar from a selected PRESSURE_UNIT
* Default read unit is mbar.
* \param in PRESSURE_UNIT
* \param value
* \return converted value
*/
float ConvertPressureToMbar(PRESSURE_UNIT in, float value){
	switch (in){
	case mbar:
		return value;
		break;
	case kPa:
		return value * 10;
		break;
	case PSI:
		return value * float(68.9476);
		break;
	default:
		return value;
		break;
	};
}

/**
* \brief Convert flow-rate value from ul/min to a selected FLOW_UNIT
* Default read unit is ul/min.
* \param out FLOW_UNIT
* \param value
* \return converted value
*/
float ConvertFlowFromUlperMin(FLOW_UNIT out, float value){
	switch (out){
	case nLperMin:
		return value * 1000;
		break;
	case uLperSec:
		return value / 60;
		break;
	case uLperMin:
		return value;
		break;
	case uLperHour:
		return value * 60;
		break;
	case mLperMin:
		return value / 1000;
		break;
	case mLperHour:
		return value / 1000 * 60;
		break;
	default:
		return value;
		break;
	};
}

/**
* \brief Convert flow-rate value to ul/min from a selected FLOW_UNIT
* Default read unit is ul/min.
* \param in FLOW_UNIT
* \param value
* \return converted value
*/
float ConvertFlowToUlperMin(FLOW_UNIT in, float value){
	switch (in){
	case nLperMin:
		return value / 1000;
		break;
	case uLperSec:
		return value * 60;
		break;
	case uLperMin:
		return value;
		break;
	case uLperHour:
		return value / 60;
		break;
	case mLperMin:
		return value * 1000;
		break;
	case mLperHour:
		return value * 1000 / 60;
		break;
	default:
		return value;
		break;
	};
}
