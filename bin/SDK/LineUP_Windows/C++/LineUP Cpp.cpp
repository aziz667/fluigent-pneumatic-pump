/*============================================================================
*           Fluigent / LineUP library Example template file                   
*----------------------------------------------------------------------------
*         Copyright (c) Fluigent 2018.  All Rights Reserved.                 
*----------------------------------------------------------------------------
*                                                                            
* Title:   LineUP Cpp.cpp                                           
* Purpose: Interact with Fluigent LineUP devices using object orientated     
*				programming and Windows Middleware. This is a blank file which can
*				be used as a template.									  	
*																			  
* Version: 18.0.0                                                            
* Software: "LineUP Middleware.h" is the Fluigent instrument interface
* Hardware setup: One Link connected to one Flow EZ       
* Library version: 1.0.2.2													  
*                                                                            
*============================================================================*/

#pragma once

#include <stdio.h>
#include <tchar.h>

#include "LineUP Middleware.h"


int _tmain(int argc, _TCHAR* argv[])
{
	// Variable declaration

	try
	{
		// Instantiate Line UP Session Factory
		LineUPClassicalSessionFactory LineUPFactory;
		// Initialize session, create LineUP object
		ClassicalLineUPSession LineUP = LineUPFactory.Create(0);
		// ClassicalLineUPSession *LineUPArray = LineUPFactory.CreateAll();


		std::cout << "Link serial number: " << LineUP.LinkSerial() << std::endl;
		std::cout << "Flow EZ #1 serial number: " << LineUP.FlowEZSerial(0) << std::endl;

		system("PAUSE");
	}
	catch (std::exception const& e)
	{
		std::cerr << "LineUP error : " << e.what() << std::endl;
	}
	return 0;
}

