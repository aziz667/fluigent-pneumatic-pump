var class_controller =
[
    [ "Controller", "class_controller.html#a95c56822d667e94b031451729ce069a9", null ],
    [ "Controller", "class_controller.html#ad7640f77ea3d691fc1be32045aad0035", null ],
    [ "~Controller", "class_controller.html#a0ab87934c4f7a266cfdb86e0f36bc1b5", null ],
    [ "ModulesNumber", "class_controller.html#abf272956234862f242c68ea1940b4a51", null ],
    [ "Power", "class_controller.html#a4bb841856f14d611b913481482571829", null ],
    [ "Power", "class_controller.html#a142c8f6e54446d796f91f43b337b23c6", null ],
    [ "ReadTTL", "class_controller.html#a3c4b20443f4a117bd2da3c0969aff9c3", null ],
    [ "Serial", "class_controller.html#a209ee46739f34fc3a031c4e7cabb0fdc", null ],
    [ "SetTTLMode", "class_controller.html#acd918da2110b49c03c58f3a365ae3a95", null ],
    [ "TriggerTTL", "class_controller.html#a421b84456a074113d67e5fab6fac7f78", null ],
    [ "VersionNumber", "class_controller.html#aa615c9b619d262fa1fe57af47719b45f", null ]
];