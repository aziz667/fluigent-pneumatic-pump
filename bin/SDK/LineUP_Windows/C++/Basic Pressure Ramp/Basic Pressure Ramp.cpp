/*============================================================================
*					 Fluigent LineUP C++ SDK
*----------------------------------------------------------------------------
*         Copyright (c) Fluigent 2018.  All Rights Reserved.
*----------------------------------------------------------------------------
*
* Title:   Basic Pressure Ramp.cpp
* Purpose: This example shows how to generate a pressure ramp
*			(over the whole pressure range) on the first Flow EZ module 
*			of your LineUP chain
*
* Version: 18.0.0
* Software: "LineUP Middleware.h" is the Fluigent instrument interface
* Hardware setup: One Link connected to one Flow EZ
* Library version: 1.0.2.2
*
*============================================================================*/

#pragma once

#include <stdio.h>
#include <tchar.h>

#include "../LineUP Middleware.h"					// Include Fluigent LineUP Middleware file


int _tmain(int argc, _TCHAR* argv[])
{
	// Variables declaration
	int localLoop = 0;
	try{
		// Instantiate Line UP Session Factory
		LineUPClassicalSessionFactory LineUPFactory;
		// Initialize session, create LineUP object
		ClassicalLineUPSession LineUP = LineUPFactory.Create(0);

		// Retrieve Flow EZ's maximum pressure and loop 10 times over the range
		for (localLoop = 0; localLoop < LineUP.PressureRangeMax(0); localLoop += LineUP.PressureRangeMax(0) / 10)
		{
			// Set pressure on Flow EZ #1
			std::cout << "Set pressure on Flow EZ #1: " << localLoop;
			LineUP.Pressure(0, localLoop);

			// Wait 1 second
			Sleep(1000);

			// Read pressure
			std::cout << "\tRead pressure : " << LineUP.Pressure(0) << std::endl;
		}

		system("PAUSE");
	}
	catch (std::exception const& e)		// Catch exception if occured
	{
		std::cerr << "LineUP error : " << e.what() << std::endl;
	}

	return 0;
}

