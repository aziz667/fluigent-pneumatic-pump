
#include "LineUP low level.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/// ... Classical LineUP Session class : high level class containing a Link and channels 
/// This class' methods support all available actions on Link and Flow EZ LineUP hardwares ...
/////////////////////////////////////////////////////////////////////////////////////////////////////////
class ClassicalLineUPSession{
private:
	Controller Link;												// Link controller: private handler of Link module
	Channel FlowEZ[8];												// Flow EZ channel: private handler of Flow EZ modules
public:
	ClassicalLineUPSession();				// Class constructor
	ClassicalLineUPSession(unsigned short LinkSerial);				// Class constructor
	~ClassicalLineUPSession();										// Class destructor

	/// General purpose functions
	unsigned short LinkSerial(void);								// Read Link serial number.
	unsigned short LinkVersion(void);								// Read Link firmware version number.
	unsigned short FlowEZSerial(unsigned char module);				// Read specific module serial number.
	unsigned short FlowEZVersion(unsigned char module);				// Read specific module firmware version number.

	void SetSessionPressureUnit(PRESSURE_UNIT unit);				// Set pressure unit used for all modules connected to this Link session. See PRESSURE_UNIT enum for details. Default value is mbar.

	/// Link related functions
	void Power(POWER_STATE state);									// Power On or Off Link module.
	POWER_STATE Power(void);										// Read power state (On/Off) of Link module.
	unsigned char ModulesNumber(void);								// Returns total number of connected modules on Link.

	void SetTTLMode(TTL_PORT port, TTL_MODE mode);					// Configure a specific TTL port (BNC port on Link) as input, output, rising or falling edge.
	void ReadTTL(bool *port1, bool *port2);							// Read TTL ports (BNC port on Link) if set as input.
	void TriggerTTL(TTL_PORT port);									// Triggers a specific TTL port (BNC port on Link) if set as output.

	/// FlowEZ related functions - read-only
	unsigned char ModuleStatus(unsigned char module);				// Returns status of the module, refer to documentation for bit assignment.

	float PressureRangeMin(unsigned char module);					// Returns device minimal pressure value in selected PRESSURE_UNIT. Default value is mbar.
	float PressureRangeMax(unsigned char module);					// Returns device maximal pressure value in selected PRESSURE_UNIT. Default value is mbar.

	float FlowrateRangeMin(unsigned char module);					// Returns device minimal sensor flowrate value in selected FLOW_UNIT. Default value is ul/min.
	float FlowrateRangeMax(unsigned char module);					// Returns device maximal sensor flowrate value in selected FLOW_UNIT. Default value is ul/min.

	void CalibratePressureSensor(unsigned char module);				// Send a calibration signal; module is not operational for next 5 seconds
	FLOW_UNIT_TYPE FlowUnitType(unsigned char module);				// Get type of the flow unit and available calibration table(s)

	unsigned char FlowrateControlStatus(unsigned char module);		// Read flowrate regulation status when module is in flowrate control.

	/// FlowEZ related functions - Set-get
	void PressureMode(unsigned char module, PRESSURE_MODE mode);	// Set pressure controller mode from FAST or SMOOTH.
	PRESSURE_MODE PressureMode(unsigned char module);				// Read actual pressure controller mode from FAST or SMOOTH

	void PressureUnit(unsigned char module, PRESSURE_UNIT unit);	// Change module pressure unit from mbar, kPa, or PSI. All orders and read values are then in selected unit. Default value is mbar.
	PRESSURE_UNIT PressureUnit(unsigned char module);				// Read module pressure unit

	void FlowUnit(unsigned char module, FLOW_UNIT unit);			// Change module flow sensor unit. Default value is ul/min, uLperMin in enumeration list.
	FLOW_UNIT FlowUnit(unsigned char module);						// Read flow sensor unit.

	void CalibrationTable(unsigned char module, FLOW_UNIT_CALIBRATION_TABLE table);		// Change flow sensor calibration table. Default value is H2O (water). See enumeration list for further details.
	FLOW_UNIT_CALIBRATION_TABLE CalibrationTable(unsigned char module);					// Read flow sensor calibration table.

	void Pressure(unsigned char module, float pressure);			// Send pressure order in selected PRESSURE_UNIT. Default unit is mbar.
	float Pressure(unsigned char module);							// Read module pressure in selected PRESSURE_UNIT.

	void Flowrate(unsigned char module, float flowrate);			// Send pressure order in selected FLOW_UNIT. Default unit is ul/min.
	float Flowrate(unsigned char module);							// Read module flow sensor value in selected FLOW_UNIT.

	
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/// ... LineUP Classical Session Factory class : high level factory class of ClassicalLineUPSession ...
/////////////////////////////////////////////////////////////////////////////////////////////////////////
class LineUPClassicalSessionFactory{
	// Internal storage of LineUP sessions
	ClassicalLineUPSession LineUPSessions[256];
public:
	LineUPClassicalSessionFactory();
	~LineUPClassicalSessionFactory();
	
	ClassicalLineUPSession Create(unsigned short LinkSerial);		// Create single instance of LineUP Session. Use this method if only one Link is used. If no serial is set (=0), first found device will be initialized
	ClassicalLineUPSession *CreateMultiple(unsigned short LinkSerial[256]);	// Create multiple instances of LineUP Session. Use this method if multiple Link are connected and only specific modules are used. LinkSerial is an array of Link Serial numbers to initialize.
	ClassicalLineUPSession *CreateAll(void);						// Create all LineUP Session from all found Link module(s).
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/// ... Unit Converter Tool: convert pressure or flow-rate values between different units ...
/////////////////////////////////////////////////////////////////////////////////////////////////////////
float ConvertPressure(PRESSURE_UNIT in, PRESSURE_UNIT out, float value); // Convert pressure value from a selected PRESSURE_UNIT to another
float ConvertFlow(FLOW_UNIT in, FLOW_UNIT out, float value);		// Convert flow-rate value from a selected FLOW_UNIT to another
