/*============================================================================
*					 Fluigent LineUP C++ SDK 
*----------------------------------------------------------------------------
*         Copyright (c) Fluigent 2018.  All Rights Reserved.
*----------------------------------------------------------------------------
*
* Title:   Basic Pressure Ramp.cpp
* Purpose: This example shows how to set concurrent pressure or flowrate
*			orders on the two Flow EZ modules on two different LineUP chains
*
* Version: 18.0.0
* Software: "LineUP Middleware.h" is the Fluigent instrument interface
* Hardware setup: Two Link each connected to a Flow EZ with a flow unit
* Library version: 1.0.2.2
*
*============================================================================*/

#pragma once

#include <stdio.h>
#include <tchar.h>
#include <thread>							// Include threading library
#include <time.h>							// Include time library for random number generation

#include "../LineUP Middleware.h"			// Include Fluigent LineUP Middleware file

using namespace std;

bool CancellationToken = false;

// Link #1 Flow EZ #1 is driven in pressure, every 2 seconds randomly
void task1(ClassicalLineUPSession *LineUP)
{
	float PressureOrder;
	while (!CancellationToken)
	{
		srand(time(NULL));				// Random initialization 
		PressureOrder = ((rand() % 100) * LineUP->PressureRangeMax(0)) / 100;
		LineUP->Pressure(0, PressureOrder);
		std::cout << "task1: New pressure order: " << PressureOrder << " mBar " << std::endl;
		this_thread::sleep_for(std::chrono::milliseconds(2000));
	}
}

// Link #2 Flow EZ #1 is driven in flow, every 5 seconds, randomly
void task2(ClassicalLineUPSession *LineUP)
{
	float FlowrateOrder;
	while (!CancellationToken)
	{
		srand(time(NULL));				// Random initialization 
		FlowrateOrder = ((rand() % 100) * LineUP->FlowrateRangeMax(0)) / 100;
		LineUP->Flowrate(0, FlowrateOrder);
		std::cout << "task2: New flowrate order: " << FlowrateOrder << " ul/min " << std::endl;
		this_thread::sleep_for(std::chrono::milliseconds(5000));
	}
}


int _tmain(int argc, _TCHAR* argv[])
{
	try{
		// Instantiate Line UP Session Factory
		LineUPClassicalSessionFactory LineUPFactory;
		// Initialize session on all connected Links, create LineUP array object
		ClassicalLineUPSession *LineUP = LineUPFactory.CreateAll();

		// Construct two threads and run. Does not block execution.
		thread thread1(task1, &LineUP[0]);
		thread thread2(task2, &LineUP[1]);

		// Wait 10 seconds
		Sleep(10000);

		// Stop threads
		CancellationToken = true;
		thread1.join();
		thread2.join();
		std::cout << "Application is closing..." << std::endl;

		system("PAUSE");
	}
	catch (std::exception const& e)		// Catch exception if occured
	{
		std::cerr << "LineUP error : " << e.what() << std::endl;
	}

	return 0;
}

