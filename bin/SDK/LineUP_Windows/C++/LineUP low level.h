
#include <cstdlib>
#include <iostream>
#include <windows.h>
#include <stdint.h>
#include <stdio.h>


////////////////////////////////////////////////////////////////////////////////
/// ...----------------- Enumeration of global types ----------------------- ...
////////////////////////////////////////////////////////////////////////////////

/**
 * \brief POWER_STATE
 * POWER_STATE is working status of the Link: OFF, ON
 */
enum POWER_STATE { OFF, ON };

/**
 * \brief TTL_MODE
 * TTL mode in/out and falling/rising: DETECT_RISING_EDGE, DETECT_FALLING_EDGE, OUTPUT_PULSE_LOW, OUTPUT_PULSE_HIGH
 */
enum TTL_MODE { DETECT_RISING_EDGE, DETECT_FALLING_EDGE, OUTPUT_PULSE_LOW, OUTPUT_PULSE_HIGH }; 

/**
 * \brief TTL_PORT
 * BNC ports on Link: TTL1, TTL2
 */
enum TTL_PORT { TTL1, TTL2 };		

/**
* \brief FLOW_UNIT_TYPE
* Type of the flow unit and available calibration table(s)
*/
enum FLOW_UNIT_TYPE { None, XS_single, S_single, S_dual, M_single, M_dual,L_single, L_dual, XL_single };

/**
 * \brief FLOW_UNIT_CALIBRATION_TABLE
 * Calibration table of flow units: H2O, IPA, HFE, FC40, OIL
 */
enum FLOW_UNIT_CALIBRATION_TABLE { H2O, IPA, HFE, FC40, OIL };		

/**
 * \brief PRESSURE_MODE
 * Pressure regulation mode: FAST, SMOOTH
 */
enum PRESSURE_MODE { FAST, SMOOTH };												

/**
 * \brief PRESSURE_UNIT
 * Pressure unit: mbar, kPa, PSI 
 */
enum PRESSURE_UNIT { mbar, kPa, PSI };		


/**
 * \brief FLOW_UNIT
 * Flow-rate unit: nLperMin, uLperSec, uLperMin, uLperHour, mLperMin, mLperHour
 */
enum FLOW_UNIT { nLperMin, uLperSec, uLperMin, uLperHour, mLperMin, mLperHour };

//==============================================================================
// Define functions type prototype and pointers to dll functions
//==============================================================================
typedef unsigned char(__stdcall *lineup_detect)(unsigned short serialTable[256]);
typedef unsigned long long(__stdcall *lineup_initialization)(unsigned short serial);
typedef unsigned char(__stdcall *lineup_get_serial)(unsigned long long handle, unsigned short * version, unsigned short * serial);
typedef unsigned char(__stdcall *lineup_get_module_number)(unsigned long long handle, unsigned char * status, unsigned char * moduleNumber);
typedef unsigned char(__stdcall *lineup_power)(unsigned long long handle, unsigned char mode);
typedef unsigned char(__stdcall *lineup_get_module_info)(unsigned long long handle, unsigned char module,
	unsigned char * type,
	unsigned short * version,
	unsigned short * serial);
typedef unsigned char(__stdcall *lineup_set_ttl_mode)(unsigned long long handle,
	unsigned char TTL_mode1,
	unsigned char TTL_mode2);
typedef unsigned char(__stdcall *lineup_get_ttl_state)(
	unsigned long long handle,
	unsigned char TTL_ack,
	unsigned char * TTL_state1,
	unsigned char * TTL_state2);
typedef unsigned char(__stdcall *lineup_trig_ttl)(unsigned long long handle, unsigned char TTL_index);
typedef unsigned char(__stdcall *lineup_close)(unsigned long long handle);

typedef unsigned char(__stdcall *flowez_read_module)(unsigned long long handle, unsigned char module, unsigned char *status,
	float * pressure,
	float * flowrate,
	unsigned char * timeStamp);
typedef unsigned char(__stdcall *flowez_module_config)(unsigned long long handle, unsigned char module,
	float * P_max,
	float * Q_max,
	unsigned char * Q_calibTable,
	unsigned char * Q_artCode);
typedef unsigned char(__stdcall *flowez_set_pressure)(unsigned long long handle, unsigned char module, float P_cmd);
typedef unsigned char(__stdcall *flowez_set_flowrate)(unsigned long long handle, unsigned char module, float Q_cmd);
typedef unsigned char(__stdcall *flowez_get_flowcontrol_status)(unsigned long long handle, unsigned char module, unsigned char * status);
typedef unsigned char(__stdcall *flowez_set_mode)(unsigned long long handle, unsigned char module, unsigned char mode);
typedef unsigned char(__stdcall *flowez_pressure_reset)(unsigned long long handle, unsigned char module);
typedef unsigned char(__stdcall *flowez_set_q_calibtable)(unsigned long long handle, unsigned char module, unsigned char Q_calibTable);

////////////////////////////////////////////////////////////////////////////////
/// ... High level abstract instruments ...
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// ... iInstrument : simple generic instrument high level class ...
////////////////////////////////////////////////////////////////////////////////
class iInstrument{
protected:
public:
	// Variables
	unsigned long long Handle;				// Local instrument handle; do not change this value
	unsigned short SN;						// Instrument Serial Number
	unsigned short Version;					// Instrument Version Number

	// Functions
	iInstrument();
	~iInstrument();
};

////////////////////////////////////////////////////////////////////////////////
/// ... iLink : simple generic Link instrument high level class ...
////////////////////////////////////////////////////////////////////////////////
class iLink : public iInstrument{
public:
	// Functions
	iLink();
	~iLink();
	POWER_STATE Power(void);				// Get Link device power state
	void Power(POWER_STATE power);			// Set Link device power on or off
	unsigned char ModulesNumber(void);		// Total number of detected devices connected to the Link
};

////////////////////////////////////////////////////////////////////////////////
/// ... iModule : simple generic module instrument high level class ...
////////////////////////////////////////////////////////////////////////////////
class iModule: public iInstrument {
protected:
	// Variables
	unsigned char Module;					// Internal index of the module
public:
	// Functions
	iModule();
	~iModule();
	unsigned char Status(void);				// Internal status of the module
};


////////////////////////////////////////////////////////////////////////////////
/// ... --------------------- Low level instruments ------------------------ ...
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// ... Controller: Link Controller class ...
////////////////////////////////////////////////////////////////////////////////
class Controller : public iLink{
private:
	// Internal variables
	TTL_MODE TTL_MODE_1;
	TTL_MODE TTL_MODE_2;

	// Pointer to each dll function
	lineup_detect plineup_detect;
	lineup_initialization plineup_initialization;
	lineup_get_serial plineup_get_serial;
	lineup_get_module_number plineup_get_module_number;
	lineup_get_module_info plineup_get_module_info;
	lineup_set_ttl_mode plineup_set_ttl_mode;
	lineup_get_ttl_state plineup_get_ttl_state;
	lineup_trig_ttl plineup_trig_ttl;
	lineup_power plineup_power;
	lineup_close plineup_close;
	
	// Functions
	void manageReturnCode(unsigned char code, char *functionName);	// This function manages error codes, it can be written again in order to meet your needs

public:
	HINSTANCE ProcDLL = NULL;						// Instance which will handle the dynamic library

	// Constructor/destructor
	Controller();
	Controller(unsigned short serial);
	~Controller();

	// Functions
	unsigned short Serial(void);					// Link Serial Number. Overloaded function for dynamic update
	unsigned short VersionNumber(void);				// Link Version Number. Overloaded function for dynamic update

	POWER_STATE Power(void);						// Get Link device power state
	void Power(POWER_STATE power);					// Set Link device power on or off
	unsigned char ModulesNumber(void);				// Total number of detected devices connected to the Link

	void SetTTLMode(TTL_PORT port, TTL_MODE mode);	// Set operating mode of TTL connectors
	void ReadTTL(bool *TTL1, bool *TTL2);			// Get TTL state
	void TriggerTTL(TTL_PORT port);					// Trigger TTL1 or TTL2 if set to output
};


////////////////////////////////////////////////////////////////////////////////
/// ... Single LineUP channel, Flow EZ in this case ...
////////////////////////////////////////////////////////////////////////////////
class Channel : public iModule{
private:
	HINSTANCE ProcDLL;								// Instance which handles the dynamic library
	// Pointer to each dll function
	lineup_get_module_info plineup_get_module_info;
	flowez_read_module pflowez_read_module;
	flowez_module_config pflowez_module_config;
	flowez_set_pressure pflowez_set_pressure;
	flowez_set_flowrate pflowez_set_flowrate;
	flowez_get_flowcontrol_status pflowez_get_flowcontrol_status;
	flowez_set_mode pflowez_set_mode;
	flowez_pressure_reset pflowez_pressure_reset;
	flowez_set_q_calibtable pflowez_set_q_calibtable;

	// Functions
	void manageReturnCode(unsigned char code, char *functionName); // This function manages error codes, it can be written again in order to meet your needs

public:

	// Varibles	
	FLOW_UNIT FlowUnit;										// Flow-rate unit: nLperMin, uLperSec, uLperMin, uLperHour, mLperMin, mLperHour
	PRESSURE_UNIT PressureUnit;								// Pressure unit: mbar, kPa, PSI 

	// Constructor/destructor
	Channel();
	Channel(unsigned long long Handle, unsigned char module, HINSTANCE ProcDLL); // Constructor of a channel, input: Link session handler, index of the module
	~Channel();

	// Functions - Device settings
	unsigned short Serial(void);							// Instrument Serial Number. Overloaded function for dynamic update
	unsigned short VersionNumber(void);						// Instrument Version Number. Overloaded function for dynamic update
	unsigned char Status(void);								// Instrument Status

	float PressureRangeMax(void);							// Get pressure maximal range in set PRESSURE_UNIT
	float PressureRangeMin(void);							// Get pressure minimal range in set PRESSURE_UNIT

	float FlowrateRangeMax(void);							// Get flow-rate maximal range in set FLOW_UNIT
	float FlowrateRangeMin(void);							// Get flow-rate minimal range in set FLOW_UNIT

	// Functions - read/write control
	float Pressure(void);									// Read pressure value in set PRESSURE_UNIT
	void Pressure(float command);							// Set pressure value in set PRESSURE_UNIT	

	float FlowRate(void);									// Read flow-rate value in set FLOW_UNIT
	void FlowRate(float command);							// Set flow-rate value in set FLOW_UNIT

	unsigned char FlowrateControlStatus(void);				// Read status of flow-rate regulation state

	// Functions - Custom
	FLOW_UNIT_CALIBRATION_TABLE FlowCalibrationTable(void);	// Read flow unit sensor calibration table (0:H20; 1:IPA; 2:HFE; 3:FC40; 4:OIL) for M dual calibration type; (0:H20; 1:IPA) for XS, S, L and XL flow - units
	void FlowCalibrationTable(FLOW_UNIT_CALIBRATION_TABLE unit); // Set flow unit sensor calibration table (0:H20; 1:IPA; 2:HFE; 3:FC40; 4:OIL) for M dual calibration type; (0:H20; 1:IPA) for XS, S, L and XL flow - units

	PRESSURE_MODE PressureMode(void);						// Read actual pressure controller mode from FAST or SMOOTH
	void PressureMode(PRESSURE_MODE mode);					// Set pressure controller mode from FAST or SMOOTH

	// Functions - Specific
	void CalibratePressureSensor(void);						// Send a calibration signal; module is not operational for next 5 seconds
	FLOW_UNIT_TYPE FlowUnitType(void);				// Get type of the flow unit and available calibration table(s)
};

////////////////////////////////////////////////////////////////////////////////
/// ... Instrument: LineUP Detect ...
////////////////////////////////////////////////////////////////////////////////
void LineUPDetect(unsigned short serialTable[256]);

////////////////////////////////////////////////////////////////////////////////
/// ... Unit Converter Tool: convert pressure or flow-rate values between different units ...
////////////////////////////////////////////////////////////////////////////////

float ConvertPressureFromMbar(PRESSURE_UNIT out, float value); // Convert pressure value from a selected PRESSURE_UNIT to another
float ConvertPressureToMbar(PRESSURE_UNIT in, float value);	// Convert pressure value to mbar from a selected PRESSURE_UNIT

float ConvertFlowFromUlperMin(FLOW_UNIT out, float value);	// Convert flow-rate value from a selected FLOW_UNIT to another
float ConvertFlowToUlperMin(FLOW_UNIT in, float value);		// Convert flow-rate value to ul/min from a selected FLOW_UNIT
