/*============================================================================
*					 Fluigent LineUP C++ SDK
*----------------------------------------------------------------------------
*         Copyright (c) Fluigent 2018.  All Rights Reserved.
*----------------------------------------------------------------------------
*
* Title:   Basic Pressure Ramp.cpp
* Purpose: This example shows how to set concurrent pressure or flowrate 
*			orders on the two Flow EZ modules on two different LineUP chains
*
* Version: 18.0.0
* Software: "LineUP Middleware.h" is the Fluigent instrument interface
* Hardware setup: Two Link each connected to a Flow EZ
* Library version: 1.0.2.2
*
*============================================================================*/

#pragma once

#include <stdio.h>
#include <tchar.h>

#include "../LineUP Middleware.h"					// Include Fluigent LineUP Middleware file


int _tmain(int argc, _TCHAR* argv[])
{
	try{
		// Instantiate Line UP Session Factory
		LineUPClassicalSessionFactory LineUPFactory;
		// Initialize session on all connected Links, create LineUP array object
		ClassicalLineUPSession *LineUP = LineUPFactory.CreateAll();

		std::cout << "Link #1 Flow EZ #1, serial number: " << LineUP[0].FlowEZSerial(0) << std::endl;
		std::cout << "Link #2 Flow EZ #1, serial number: " << LineUP[1].FlowEZSerial(0) << std::endl;

		// Set pressure to 100 (mBar is the default unit) on Link #1 Flow EZ #1
		std::cout << "Link #1 Flow EZ #1: Set pressure to 100 mBar " << std::endl;
		LineUP[0].Pressure(0, 100);

		// Set flowrate to 1 (ul/min is the default unit) on Link #2 Flow EZ #1 
		std::cout << "Link #2 Flow EZ #1: Set flowrate to 1 ul/min " << std::endl;
		LineUP[1].Flowrate(0, 1);

		// Wait 10 seconds
		std::cout << "Waiting 10 seconds..." << std::endl;
		Sleep(10000);

		// Set pressure to 50 (mBar is the default unit) on Link #2 Flow EZ #1
		// Note that flow regulation stops when a pressure order is sent
		std::cout << "Link #2 Flow EZ #1: Set pressure to 50 mBar " << std::endl;
		LineUP[1].Pressure(0, 50);

		// Wait 1 second
		Sleep(1000);

		system("PAUSE");
	}
	catch (std::exception const& e)		// Catch exception if occured
	{
		std::cerr << "LineUP error : " << e.what() << std::endl;
	}

	return 0;
}

