/*============================================================================*/
/*                         Fluigent / LineUP API                              */
/*----------------------------------------------------------------------------*/
/*         Copyright (c) Fluigent 2018.  All Rights Reserved.                 */
/*----------------------------------------------------------------------------*/
/*                                                                            */
/* Title:   LineUP_c.h                                                        */
/* Purpose: Include file to interact with the LineUP API                      */
/* Version: 1.0.2.2                                                           */
/*                                                                            */
/*============================================================================*/

/* Returned function values (C error)		
#define OK		    				0
#define LINEUP_ERR_USB_CLOSED	    1
#define LINEUP_ERR_CMD		   		2
#define LINEUP_ERR_NO_MODULE	    3
#define LINEUP_ERR_WRONG_MODULE		4
#define LINEUP_ERR_MODULE_SLEEP		5
*/ 

#ifndef lineup_cH
#define lineup_cH

#ifdef __cplusplus
extern "C"
{
#endif

/*============================================================================*/
/*--------------------  LineUP Link Module Section  --------------------------*/
/*============================================================================*/

/* Name: 		lineup_detect
 *
 * Description:	detects connected LineUP Link Module(s) and return their serial numbers in the table
 * Arguments:	
 * Returns:		C error is the error code
 *              serialTable is the array of all connected LineUP Link Module
 */	
unsigned char __stdcall lineup_detect(
		unsigned short serialTable[256]);

/* Name: 		lineup_initialization
 *
 * Description:	initialize USB connection and launches a continuous check for a LineUP with the specified serial number
 * Arguments:	serial is the serial number of the LineUP Link Module for which  you want to open the session.  
 * Returns:		handle is the identifier of the opened session
 */
unsigned long long __stdcall lineup_initialization(
		unsigned short serial);

/* Name: 		lineup_get_serial
 *
 * Description:	gets serial number of the LineUP Link Module
 * Arguments:	handle is the identifier of the LineUP session
 * Returns:		C error is the error code
 *              serial is the LineUP Link Module serial number associated to the LineUP session specified by the connection
 *              version is the LineUP Link Module serial number associated to the LineUP session specified by the connection
 */	
unsigned char __stdcall lineup_get_serial(
		unsigned long long handle,
		unsigned short * version,
		unsigned short * serial);
		
/* Name: 		lineup_get_module_number
 *
 * Description:	gets total number of modules connected to the LineUP Link Module
 * Arguments:	handle is the identifier of the LineUP session 
 * Returns:		C error is the error code
 *      		status is the LineUP Link Module status
 *					#define MSK_RUN      0x01
 *      		moduleNumber is the total number of connected modules
 */		
unsigned char __stdcall lineup_get_module_number(
		unsigned long long handle,
		unsigned char * status,
		unsigned char * moduleNumber);
		
/* Name: 		lineup_power
 *
 * Description:	sets power state for the LineUP modules
 * Arguments:	handle is the identifier of the LineUP session
 *				mode is the power state: 1 power on, 0 power off
 * Returns:		C error is the error code
 */	
unsigned char __stdcall lineup_power(
		unsigned long long handle,
		unsigned char mode);

/* Name: 		lineup_set_ttl_mode
 *
 * Description:	sets the TTL signals mode
 * Arguments:	handle is the identifier of the LineUP session 
 *              TTL_mode = 0 (TTL_IN_DETECT_RISING_EDGE), 1 (TTL_IN_DETECT_FALLING_EDGE),
 *                     2 (TTL_OUTPUT_PULSE_LOW), 3 (TTL_OUTPUT_PULSE_HIGH)
 * Returns:		C error is the error code
 */
unsigned char __stdcall lineup_set_ttl_mode(
		unsigned long long handle,
		unsigned char TTL_mode1,
		unsigned char TTL_mode2);
		
/* Name: 		lineup_get_ttl_state
 *
 * Description:	gets the TTL signal state
 * Arguments:	handle is the identifier of the LineUP session 
 *              TTL_ack is the acknowledge of read state. If set to 1 TTL_states will be reset.
 * Returns:		C error is the error code
 *      		TTL_state1 is 1 if an edge was detected
 *      		TTL_state2 is 1 if an edge was detected
 */
unsigned char __stdcall lineup_get_ttl_state(
		unsigned long long handle,
		unsigned char TTL_ack,
		unsigned char * TTL_state1,
		unsigned char * TTL_state2);	
		
/* Name: 		lineup_trig_ttl
 *
 * Description:	sends a trigger on the specified TTL output
 * Arguments:	handle is the identifier of the LineUP session 
 *              TTL_index is the TTL number
 * Returns:		C error is the error code
 */		
unsigned char __stdcall lineup_trig_ttl(
		unsigned long long handle,
		unsigned char TTL_index);	
		
/* Name: 		lineup_close
 *
 * Description:	terminates threads and deallocates memory used by the LineUP session
 * Arguments:	handle is the identifier of the LineUP session 
 * Returns:		C error is the error code
 */	
unsigned char __stdcall lineup_close(
		unsigned long long handle);


/*============================================================================*/
/*-------------------  LineUP Flow EZ Module Section  ------------------------*/
/*============================================================================*/		

/* Name: 		lineup_get_module_info
 *
 * Description:	gets information for the specified LineUP Module
 * Arguments:	handle is the identifier of the LineUP session
 *              module is the module index 
 * Returns:		C error is the error code
 *      		type is the module type (Link, Flow EZ, Adapt, Invert, ...)
 *      		version is the module firmware version
 *      		serial is the module serial number
 */		
unsigned char __stdcall lineup_get_module_info(
		unsigned long long handle,
		unsigned char module,
		unsigned char * type,
		unsigned short * version,
		unsigned short * serial);
	
/* Name: 		flowez_read_module
 *
 * Description:	reads pressure and flow-rate value(s) of specified LineUP Flow EZ Module
 * Arguments:	handle is the identifier of the LineUP session 
 *              module is the module index
 * Returns:		C error is the error code
 *      		status is the Flow EZ status
 *					#define MSK_RUN      0x01
 *					#define MSK_LOCAL    0x02
 *					#define MSK_REGUL    0x04
 *					#define MSK_STABLE   0x08
 *					#define MSK_UNDERP   0x10
*					#define MSK_OVERP    0x20
 *					#define MSK_QDETECT  0x80
 *      		pressure is the measured pressure in mbar
 *				flowrate is the measured flow-rate in ul/min
 *      		timeStamp is the device data timestamp (incremented each 20ms)
 */
unsigned char __stdcall flowez_read_module(
		unsigned long long handle,
		unsigned char module,
		unsigned char * status,
		float * pressure,
		float * flowrate,
		unsigned short * timeStamp);

/* Name: 		flowez_set_pressure
 *
 * Description:	regulates the pressure of the specified LineUP Flow EZ Module and enters remote control
 * Arguments:	handle is the identifier of the LineUP session
 *              module is the module index
 *              P_cmd is the desired pressure value in mbar
 * Returns:		C error is the error code
 */		
unsigned char __stdcall flowez_set_pressure(
		unsigned long long handle,
		unsigned char module,
		float P_cmd);
		
/* Name: 		flowez_set_flowrate
 *
 * Description:	regulates the flow-rate of the specified LineUP Flow EZ Module and enters remote control
 * Arguments:	handle is the identifier of the LineUP session
 *              module is the module index
 *              Q_cmd is the desired flow-rate value in ul/min
 * Returns:		C error is the error code
 */		
unsigned char __stdcall flowez_set_flowrate(
		unsigned long long handle,
		unsigned char module,
		float Q_cmd);
		
/* Name: 		flowez_get_flowcontrol_status
 *
 * Description:	gets the flow-rate control status of the specified LineUP FlowEZ Module
 * Arguments:	handle is the identifier of the LineUP session
 *              module is the module index
 *              status is the flow control status : 0=OK, 1=error
 * Returns:		C error is the error code
 */		
unsigned char __stdcall flowez_get_flowcontrol_status(
		unsigned long long handle,
		unsigned char module,
		unsigned char * status);
		
/* Name: 		flowez_module_config
 *
 * Description:	reads pressure and flowrate sensors specification
 * Arguments:	handle is the identifier of the LineUP session 
 *              module is the module index
 * Returns:		C error is the error code
 *      		P_range is the pressure range in mbar
 *      		Q_range is the flow-unit range in �l/min
 *      		Q_calibTable is the active calibration table on the flow-unit (0:H20; 1:IPA; 2:HFE; 3:FC40; 4:OIL) for M dual calibration type
 *					(0:H20; 1:IPA) for XS, S, L and XL flow-units
 *      		Q_artCode is the flow-unit article code (17 characters)
*/		
unsigned char __stdcall flowez_module_config(
		unsigned long long handle,
		unsigned char module,
		float * P_range,
		float * Q_range,
		unsigned char * Q_calibTable,
		char * Q_artCode);
		
/* Name: 		flowez_set_mode
 *
 * Description:	sets the mode for the specified LineUP Flow EZ Module
 * Arguments:	handle is the identifier of the LineUP session
 *              module is the module index
 *              mode is the mode value (0 = fast = use of the TOR; 1 = smooth = no use of TOR)
 * Returns:		C error is the error code
 */		
unsigned char __stdcall flowez_set_mode(
		unsigned long long handle,
		unsigned char module,
		unsigned char mode);
		
/* Name: 		flowez_pressure_reset
 *
 * Description:	saves current pressure as zero sensor value on the LineUP Flow EZ Module
 * Arguments:	handle is the identifier of the LineUP session
 *              module is the module index
 * Returns:		C error is the error code
 */		
unsigned char __stdcall flowez_pressure_reset(
		unsigned long long handle,
		unsigned char module);
		
/* Name: 		flowez_set_q_calibtable
 *
 * Description:	sets the calibration table value for the flow-unit connected on the LineUP Flow EZ Module
 * Arguments:	handle is the identifier of the LineUP session
 *              module is the module index
 *              Q_calibTable is the active calibration table on the flow-unit (0:H20; 1:IPA; 2:HFE; 3:FC40; 4:OIL) for M dual calibration type
 *					(0:H20; 1:IPA) for XS, S, L and XL flow-units
 * Returns:		C error is the error code
 */		
unsigned char __stdcall flowez_set_q_calibtable(
		unsigned long long handle,
		unsigned char module,
		unsigned char Q_calibTable);


#ifdef __cplusplus
}
#endif
	
#endif



