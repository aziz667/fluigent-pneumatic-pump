/*============================================================================
*					 Fluigent LineUP C++ SDK
*----------------------------------------------------------------------------
*         Copyright (c) Fluigent 2018.  All Rights Reserved.                 
*----------------------------------------------------------------------------
*                                                                            
* Title:   Basic Set Pressure.cpp                                           
* Purpose: This example shows how to set a pressure order on the first 
*				Flow EZ module of your LineUP chain
*																			  
* Version: 18.0.0                                                            
* Software: "LineUP Middleware.h" is the Fluigent instrument interface  
* Hardware setup: One Link connected to one Flow EZ         
* Library version: 1.0.2.2													  
*                                                                            
*============================================================================*/

#pragma once

#include <stdio.h>
#include <tchar.h>

#include "../LineUP Middleware.h"					// Include Fluigent LineUP Middleware file


int _tmain(int argc, _TCHAR* argv[])
{
	try{
		// Instantiate Line UP Session Factory
		LineUPClassicalSessionFactory LineUPFactory;
		// Initialize session, create LineUP object
		ClassicalLineUPSession LineUP = LineUPFactory.Create(0);	

		// Set pressure to 100 (mBar in default unit: mbar) on Flow EZ #1
		std::cout << "Set pressure to 100 (mBar is the default unit) on Flow EZ #1" << std::endl;
		LineUP.Pressure(0, 100);

		// Wait 10 seconds
		std::cout << "Waiting 10 seconds..." << std::endl;
		Sleep(10000);

		// Read pressure
		std::cout << "Read pressure on Flow EZ #1: " << LineUP.Pressure(0) << std::endl;

		system("PAUSE");
	}
	catch (std::exception const& e)		// Catch exception if occured
	{
		std::cerr << "LineUP error : " << e.what() << std::endl;
	}

	return 0;
}

