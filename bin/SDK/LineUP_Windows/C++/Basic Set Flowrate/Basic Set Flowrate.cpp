/*============================================================================
*					 Fluigent LineUP C++ SDK
*----------------------------------------------------------------------------
*         Copyright (c) Fluigent 2018.  All Rights Reserved.
*----------------------------------------------------------------------------
*
* Title:   Basic Set Flowrate.cpp
* Purpose: This example shows how to set a flowrate order on the first
*				Flow EZ module of your LineUP chain
*
* Version: 18.0.0
* Software: "LineUP Middleware.h" is the Fluigent instrument interface
* Hardware setup: One Link connected to one Flow EZ with a flow unit
* Library version: 1.0.2.2
*
*============================================================================*/

#pragma once

#include <stdio.h>
#include <tchar.h>

#include "../LineUP Middleware.h"				// Include Fluigent LineUP Middleware file


int _tmain(int argc, _TCHAR* argv[])
{
	try{
		// Instantiate Line UP Session Factory
		LineUPClassicalSessionFactory LineUPFactory;
		// Initialize session, create LineUP object
		ClassicalLineUPSession LineUP = LineUPFactory.Create(0);

		// Set flowrate to 1 (ul/min is the default unit) on Flow EZ #1
		std::cout << "Set flowrate to 1 (ul/min is the default unit) on Flow EZ #1" << std::endl;
		LineUP.Flowrate(0, 1);

		// Wait 10 seconds
		std::cout << "Waiting 10 seconds..." << std::endl;
		Sleep(10000);

		// Read pressure
		std::cout << "Read flowrate on Flow EZ #1: " << LineUP.Flowrate(0) << std::endl;

		system("PAUSE");
	}
	catch (std::exception const& e)		// Catch exception if occured
	{
		std::cerr << "LineUP error : " << e.what() << std::endl;
	}

	return 0;
}

