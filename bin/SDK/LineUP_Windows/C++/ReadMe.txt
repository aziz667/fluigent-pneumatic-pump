﻿===========================================================================
							C++ LineUP SDK
===========================================================================

This file contains a summary of LineUP SDK C++ middleware and basic examples.

-------------------------- SOFTWARE ---------------------------------------
The software was implemented and tested on Windows x64 platform using Microsoft Visual Studio 2013 version.

------------------------ INSTALLATION -------------------------------------
The application is based on a dynamically linked library "LineUP_c_32.dll" and "LineUP_c_64.dll".
LineUP low level.cpp file contains dll functions call.
LineUP Middleware.cpp file is the entry point to the LineUP interface. Create a session using LineUPClassicalSessionFactory object then all functions are available through the interface. 
Five examples are provided.
When opening the project with a newer version of the compiler, you can migrate it.
If using Visual Studio environment, LineUP Cpp.sln file can be directly opened.

--------------------------- FILES ----------------------------------------
List of project files:

LineUP low level.cpp
    Low level interface to LineUP dll

LineUP Middleware.cpp
    User interface used to access LineUP functionalities
	
Basic Set Pressure folder
	Simple example to set a pressure
	
Basic Set Flowrate folder
	Simple example to set a flowrate
	
Basic Pressure Ramp folder
	Simple example to set a pressure ramp over time
	
Basic Multiple Flow EZ Modules Control folder
	Simple example to set pressures on different multiple Link modules

Advanced Parallel Multiple Flow EZ Modules Control folder
	Advanced example using threads in order to apply pressure asynchronously

LineUP_c_64.dll
    This is the dynamic linked library used by 64bit systems in order to control the device

LineUP_c_32.dll
   This is the dynamic linked library used by 32bit systems in order to control the device
   
LineUP_c.h
	File header of dll function prototype
