/*============================================================================
*           Fluigent / LineUP Object interface to Middleware                        
*----------------------------------------------------------------------------
*         Copyright (c) Fluigent 2018.  All Rights Reserved.                 
*----------------------------------------------------------------------------
*                                                                            
* Title:   LineUP Middleware.cpp													  
* Purpose: Interact with Fluigent LineUP interface using high level object.  
*			Please read documentation for architecture details.				  
* Version: 18.0.0                                                            
* Library version: 1.0.2.2													  
*                                                                            
*============================================================================*/

#include "LineUP Middleware.h"


/////////////////////////////////////////////////////////////////////////////////////////////////////////
/// ... Classical LineUP Session class : high level class containing a Link and channels
///		This class' methods support all available actions on Link and Flow EZ LineUP hardwares ...
/////////////////////////////////////////////////////////////////////////////////////////////////////////

LineUPClassicalSessionFactory::LineUPClassicalSessionFactory(){
	

}
LineUPClassicalSessionFactory::~LineUPClassicalSessionFactory(){
}

/**
* \brief Create single instance of LineUP Session
* Use this method if only one Link is used. If no serial is set (=0), first found device will be initialized
* \return ClassicalLineUPSession
*/
ClassicalLineUPSession LineUPClassicalSessionFactory::Create(unsigned short LinkSerial){
	return ClassicalLineUPSession(LinkSerial);
}

/**
* \brief Create multiple instances of LineUP Session
* Use this method if multiple Link are connected and only specific modules are used. LinkSerial is an array of Link Serial numbers to initialize.
* \param: LinkSerial array table
* \return ClassicalLineUPSession array
*/
// ClassicalLineUPSession *LineUPClassicalSessionFactory::CreateMultiple(unsigned short LinkSerial[256]){
ClassicalLineUPSession *LineUPClassicalSessionFactory::CreateMultiple(unsigned short LinkSerial[256]){
	unsigned char localLoop = 0;

	while ((LinkSerial[localLoop] != 0) && (localLoop < 255)){
		ClassicalLineUPSession *localNewClassicalLineUPSession = new ClassicalLineUPSession(LinkSerial[localLoop]);
		this->LineUPSessions[localLoop] = *localNewClassicalLineUPSession;
		localLoop++;
	}
	return this->LineUPSessions;
}





/**
* \brief Create all LineUP Session from all found Link module(s)
* \return ClassicalLineUPSession array
*/
// std::vector<ClassicalLineUPSession> LineUPClassicalSessionFactory::CreateAll(void){
ClassicalLineUPSession *LineUPClassicalSessionFactory::CreateAll(void){
	unsigned char localLoop = 0;
	unsigned short localSN[256] = { 0 };
	LineUPDetect(localSN);

	while ((localSN[localLoop] != 0) && (localLoop < 255)){

		ClassicalLineUPSession *localNewClassicalLineUPSession = new ClassicalLineUPSession(localSN[localLoop]);
		this->LineUPSessions[localLoop] = *localNewClassicalLineUPSession;
		localLoop++;
	}


	return this->LineUPSessions;
	/*
	while ((localSN[localLoop] != 0) && (localLoop < 255)){
		ClassicalLineUPSession *localNewClassicalLineUPSession = new ClassicalLineUPSession(localSN[localLoop]);

		Arr[localLoop] = ClassicalLineUPSession(localSN[localLoop]);

		//localClassicalLineUPSession[localLoop] = localNewClassicalLineUPSession;
		localLoop++;
	}
	*/

	/*
	ClassicalLineUPSession *localClassicalLineUPSessionArr[256];

	while ((localSN[localLoop] != 0) && (localLoop < 255)){
		ClassicalLineUPSession *localNewClassicalLineUPSession = new ClassicalLineUPSession(localSN[localLoop]);

		localClassicalLineUPSessionArr[localLoop] = new ClassicalLineUPSession(localSN[localLoop]);

		//localClassicalLineUPSession[localLoop] = localNewClassicalLineUPSession;
		localLoop++;
	}

	localLoop = 0;
	*/
	/*
	ClassicalLineUPSession *localNewClassicalLineUPSession = new ClassicalLineUPSession(localSN[localLoop]);

	std::vector<ClassicalLineUPSession> localClassicalLineUPSession;

	while ((localSN[localLoop] != 0) && (localLoop < 255)){


		localNewClassicalLineUPSession = new ClassicalLineUPSession(localSN[localLoop]);
		

		//localClassicalLineUPSession[localLoop] = new ClassicalLineUPSession(localSN[localLoop]);

		localClassicalLineUPSession.emplace_back(*localNewClassicalLineUPSession);
		//localClassicalLineUPSession.emplace_back(*localNewClassicalLineUPSession);
		//localClassicalLineUPSession[localLoop] = localNewClassicalLineUPSession;
		LineUPSessions[localLoop] = *localNewClassicalLineUPSession;

		localLoop++;
	}
	*/
	/*
	localLoop = 0;
	while ((localSN[localLoop] != 0) && (localLoop < 255)){
		localClassicalLineUPSession[localLoop].
	}
	*/



	//return localClassicalLineUPSession;
	//delete localNewClassicalLineUPSession;

	

		/*

	ClassicalLineUPSession *localClassicalLineUPSession[256];

	while ((localSN[localLoop] != 0) && (localLoop < 255)){
		ClassicalLineUPSession *localNewClassicalLineUPSession = new ClassicalLineUPSession(localSN[localLoop]);

		localClassicalLineUPSession[localLoop] = new ClassicalLineUPSession(localSN[localLoop]);

		//localClassicalLineUPSession[localLoop] = localNewClassicalLineUPSession;
		localLoop++;
	}
	return localClassicalLineUPSession;

	*/


}


/////////////////////////////////////////////////////////////////////////////////////////////////////////
/// ... LineUP Classical Session Factory class : high level factory class of ClassicalLineUPSession ...
/////////////////////////////////////////////////////////////////////////////////////////////////////////

ClassicalLineUPSession::ClassicalLineUPSession()
{
	
}

/**
* \brief Class constructor
* \param Link Serial
*/
ClassicalLineUPSession::ClassicalLineUPSession(unsigned short LinkSerial){
	unsigned char localLoop = 0;

	Controller *Link = new Controller(LinkSerial);						// Create new Link Controller Object. This object will handle all connected modules
	this->Link = *Link;
	for (localLoop = 0; localLoop < 8; localLoop++){	
		Channel *localChannel = new Channel(this->Link.Handle, localLoop, this->Link.ProcDLL);
		this->FlowEZ[localLoop] = *localChannel; 
	}
}

/**
 * \brief Class default destructor
 */
ClassicalLineUPSession::~ClassicalLineUPSession(void){
}

// General purpose functions
/**
* \brief Read Link serial number
* \return Link Serial
*/
unsigned short ClassicalLineUPSession::LinkSerial(void){
	return this->Link.Serial();
}

/**
 * \brief Read Link firmware version number
 * \return Link Version
 */
unsigned short ClassicalLineUPSession::LinkVersion(void){
	return this->Link.VersionNumber();
}

/**
 * \brief Read specific module serial number
 * \param module index
 * \return Flow EZ Serial
 */
unsigned short ClassicalLineUPSession::FlowEZSerial(unsigned char module){
	return this->FlowEZ[module].Serial();
}

/**
 * \brief Read specific module firmware version number
 * \param module index of the FlowEZ in the LineUP chai
 * \return Flow EZ Version
 */
unsigned short ClassicalLineUPSession::FlowEZVersion(unsigned char module){
	return this->FlowEZ[module].VersionNumber();
}

/**
 * \brief Set pressure unit used for all modules connected to this Link session
 * See PRESSURE_UNIT enum for details. Default value is mbar.
 * \param unit 
 */
void ClassicalLineUPSession::SetSessionPressureUnit(PRESSURE_UNIT unit){
	unsigned char localLoop = 0;
	for (localLoop = 0; localLoop < 8; localLoop++){		// Go through all modules and change unit
		this->FlowEZ[localLoop].PressureUnit = unit;
	}
}

// Link related functions
/**
* \brief Power On or Off Link module
* Set power state for the LineUP modules. This function allows power management over all modules controlled by the Link device. 
* This equals to pressing 5s on main Link button to shut down modules or one press to wake up.
* \param power state: 1 power on, 0 power off
*/
void ClassicalLineUPSession::Power(POWER_STATE state){
	this->Link.Power(state);
}

/**
* \brief Read power state (On/Off) of Link module
* \return power state: 1 power on, 0 power off
*/
POWER_STATE ClassicalLineUPSession::Power(void){
	return this->Link.Power();
}

/**
* \brief Returns total number of connected modules on Link
* \return modules number
*/
unsigned char ClassicalLineUPSession::ModulesNumber(void){
	return this->Link.ModulesNumber();
}

/**
* \brief Configure a specific TTL port (BNC port on Link) as input, output, rising or falling edge.
* \param port
* \param mode
*/
void ClassicalLineUPSession::SetTTLMode(TTL_PORT port, TTL_MODE mode){
	this->Link.SetTTLMode(port, mode);
}

/**
* \brief Read TTL ports (BNC port on Link) if set as input.
* \return port1
* \return port2
*/
void ClassicalLineUPSession::ReadTTL(bool *port1, bool *port2){
	return this->Link.ReadTTL(port1, port2);
}

/**
* \brief Trigger a specific TTL port (BNC port on Link) if set as output.
* \param port
*/
void ClassicalLineUPSession::TriggerTTL(TTL_PORT port){
	this->Link.TriggerTTL(port);
}

// Flow EZ related functions - read-only
/**
* \brief Returns status of the module
* Refer to documentation for detailed bit assignment
* \param module index
* \return Flow EZ status mask:
* MSK_RUN      0x01
* MSK_LOCAL    0x02
* MSK_REGUL    0x04
* MSK_STABLE   0x08
* MSK_UNDERP   0x10
* MSK_OVERP    0x20
* MSK_QDETECT  0x80
*/
unsigned char ClassicalLineUPSession::ModuleStatus(unsigned char module){
	return this->FlowEZ[module].Status();
}


/**
* \brief Set pressure controller mode
* FAST or SMOOTH
* \param module index of the FlowEZ in the LineUP chain
* \param pressure mode, 0 = fast = use of the TOR; 1 = smooth = no use of TO
*/
void ClassicalLineUPSession::PressureMode(unsigned char module, PRESSURE_MODE mode){
	this->FlowEZ[module].PressureMode(mode);
}

/**
 * \brief Read actual pressure controller mode
 * FAST or SMOOTH
 * \param module index of the FlowEZ in the LineUP chain
 * \return pressure mode, 0 = fast = use of the TOR; 1 = smooth = no use of TO
 */
PRESSURE_MODE ClassicalLineUPSession::PressureMode(unsigned char module){
	return this->FlowEZ[module].PressureMode();
}

/**
 * \brief Return device minimal pressure value in selected PRESSURE_UNIT
 * \param module index of the FlowEZ in the LineUP chain
 * \return pressure min range
 */
float ClassicalLineUPSession::PressureRangeMin(unsigned char module){
	return this->FlowEZ[module].PressureRangeMin();
}

/**
* \brief  Return device maximal pressure value in selected PRESSURE_UNIT
* \param module index of the FlowEZ in the LineUP chain
* \return pressure max range
*/
float ClassicalLineUPSession::PressureRangeMax(unsigned char module){
	return this->FlowEZ[module].PressureRangeMax();
}

/**
 * \brief Return device minimal sensor flowrate value in selected FLOW_UNIT
 * \param module index of the FlowEZ in the LineUP chain
 * \return flow-rate min range
 */
float ClassicalLineUPSession::FlowrateRangeMin(unsigned char module){
	return this->FlowEZ[module].FlowrateRangeMin();
}

/**
* \brief Returs device maximal sensor flowrate value in selected FLOW_UNIT
* \param module index of the FlowEZ in the LineUP chain
* \return flow-rate max range
*/
float ClassicalLineUPSession::FlowrateRangeMax(unsigned char module){
	return this->FlowEZ[module].FlowrateRangeMax();
}

/**
* \brief Send a calibration signal; module is not operational for next 5 seconds
* \param module index of the FlowEZ in the LineUP chain
*/
void ClassicalLineUPSession::CalibratePressureSensor(unsigned char module)
{
	this->FlowEZ[module].CalibratePressureSensor();
}

/**
* \brief Get type of the flow unit and available calibration table(s)
* \param module index of the FlowEZ in the LineUP chain
* \return FLOW_UNIT_TYPE
*/
FLOW_UNIT_TYPE ClassicalLineUPSession::FlowUnitType(unsigned char module)
{
	return this->FlowEZ[module].FlowUnitType();
}



// Flow EZ related functions - Set-get
/**
 * \brief Sets the current working pressure unit for all Flow EZs All pressure orders, measurements or settings for this Flow EZ are expressed in this unit
 * Change module pressure unit from mbar, kPa, or PSI
 * All orders and read values are then in selected unit. Default value is mbar.
 * \param module index of the FlowEZ in the LineUP chain
 * \param unit 
 */
void ClassicalLineUPSession::PressureUnit(unsigned char module, PRESSURE_UNIT unit){
	this->FlowEZ[module].PressureUnit = unit;
}

/**
 * \brief Retrieves the current working pressure unit of the specified Flow EZ All pressure orders, measurements or settings for this Flow EZ are expressed in this unit
 * \param module index of the FlowEZ in the LineUP chain
 * \return pressure unit
 */
PRESSURE_UNIT ClassicalLineUPSession::PressureUnit(unsigned char module){
	return this->FlowEZ[module].PressureUnit;
}

/**
 * \brief Sets the current working flowrate unit of the specified Flow EZ All flowrate orders, measurements or settings for this Flow EZ are expressed in this unit)
 * Default value is ul/min, uLperMin in enumeration list.
 * \param module index of the FlowEZ in the LineUP chain
 * \param unit 
 */
void ClassicalLineUPSession::FlowUnit(unsigned char module, FLOW_UNIT unit){
	this->FlowEZ[module].FlowUnit = unit;
}

/**
 * \brief Retrieves the current working flowrate unit of the specified Flow EZ All flowrate orders, measurements or settings for this Flow EZ are expressed in this unit
 * \param module index of the FlowEZ in the LineUP chain
 * \return flow sensor unit
 */
FLOW_UNIT ClassicalLineUPSession::FlowUnit(unsigned char module){
	return this->FlowEZ[module].FlowUnit;
}

/**
 * \brief Change flow sensor calibration table
 * Default value is H2O (water). See enumeration list for further details:
 * (0:H20; 1:IPA; 2:HFE; 3:FC40; 4:OIL) for M dual calibration type; (0:H20; 1:IPA) for XS, S, L and XL flow - units
 * \param module index of the FlowEZ in the LineUP chain
 * \param calibration table 
 */
void ClassicalLineUPSession::CalibrationTable(unsigned char module, FLOW_UNIT_CALIBRATION_TABLE table){
	this->FlowEZ[module].FlowCalibrationTable(table);
}


/**
 * \brief Read flow sensor calibration table
 * Default value is H2O (water). See enumeration list for further details:
 * (0:H20; 1:IPA; 2:HFE; 3:FC40; 4:OIL) for M dual calibration type; (0:H20; 1:IPA) for XS, S, L and XL flow - units
 * \param module index of the FlowEZ in the LineUP chain
 * \return flow-unit calibration table
 */
FLOW_UNIT_CALIBRATION_TABLE ClassicalLineUPSession::CalibrationTable(unsigned char module){
	return this->FlowEZ[module].FlowCalibrationTable();
}

/**
* \brief Send pressure order in selected PRESSURE_UNIT
* Regulate the pressure of the Flow EZ Module and enter remote control
* \param module index of the FlowEZ in the LineUP chain
* \param pressure
*/
void ClassicalLineUPSession::Pressure(unsigned char module, float pressure){
	this->FlowEZ[module].Pressure(pressure);
}

/**
* \brief Read pressure value in set PRESSURE_UNIT
* \param module index of the FlowEZ in the LineUP chain
* \return pressure
*/
float ClassicalLineUPSession::Pressure(unsigned char module){
	return this->FlowEZ[module].Pressure();
}

/**
* \brief Send pressure order in selected FLOW_UNIT
* Regulate the flow-rate of the Flow EZ Module and enter remote control
* Default unit is ul/min
* \param module index of the FlowEZ in the LineUP chain
* \param flowrate
*/
void ClassicalLineUPSession::Flowrate(unsigned char module, float flowrate){
	this->FlowEZ[module].FlowRate(flowrate);
}

/**
 * \brief Read module flow sensor value in selected FLOW_UNIT
 * \param module index of the FlowEZ in the LineUP chain
 * \return flowrate
 */
float ClassicalLineUPSession::Flowrate(unsigned char module){
	return this->FlowEZ[module].FlowRate();
}

/**
 * \brief Read flowrate regulation status when module is in flowrate control
 * \param module index of the FlowEZ in the LineUP chain
 * \return status of flowrate control: : 0 = OK, 1 = error: not able achieve requested flowrate
 */
unsigned char ClassicalLineUPSession::FlowrateControlStatus(unsigned char module){
	return this->FlowEZ[module].FlowrateControlStatus();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/// ... Unit Converter Tool: convert pressure or flow-rate values between different units ...
/////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
* \brief Convert pressure value from a selected PRESSURE_UNIT to another
* \param PRESSURE_UNIT in 
* \param PRESSURE_UNIT out 
* \param value
* \return converted value
*/
float ConvertPressure(PRESSURE_UNIT in, PRESSURE_UNIT out, float value){
	float localValueMbar = 0;
	localValueMbar = ConvertPressureToMbar(in, value);
	return ConvertPressureFromMbar(out, localValueMbar);
}

/**
* \brief Convert flowrate value from a selected FLOW_UNIT to another
* \param FLOW_UNIT in
* \param FLOW_UNIT out
* \param value
* \return converted value
*/
float ConvertFlow(FLOW_UNIT in, FLOW_UNIT out, float value){
	float localValue = 0;
	localValue = ConvertFlowToUlperMin(in, value);
	return ConvertFlowFromUlperMin(out, localValue);
}




