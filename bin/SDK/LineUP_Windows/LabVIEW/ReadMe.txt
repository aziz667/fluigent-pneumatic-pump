﻿===========================================================================
							LabVIEW LineUP SDK
===========================================================================

This file contains a summary of LineUP SDK LabVIEW middleware and basic examples.

-------------------------- SOFTWARE ---------------------------------------
The software was implemented and tested on Windows x64 platform using LabVIEW 2015 32-bits

------------------------ INSTALLATION -------------------------------------
The application is based on dynamically linked libraries: "LineUP_c_32.dll" and "LineUP_c_64.dll".
The installation in handled by VI Package Manager (VIPM)

1. Install first "fluigent_lib_fluigent_sdk_shared" package
2. Then "fluigent_lib_fluigent_sdk_lineup" package

--------------------------- FILES ----------------------------------------

5 examples are installed. You can locate them them using the LabVIEW example search tool
- Advanced parallel multiple Flow EZ modules control.vi
- Basic multiple Flow EZ modules control.vi
- Basic pressure ramp.vi
- Basic set flowrate.vi
- Basic set pressure.vi

A LabVIEW palette is also installed, and available when you right click in a VI's diagram
Help is provided in LabVIEW's contextual help.