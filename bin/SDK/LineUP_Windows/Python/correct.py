# -*- coding: utf-8 -*-
"""
Created on Tue Mar 24 14:26:47 2020

@author: ChipCare-Dell3040-2
"""

#correction script 

from __future__ import print_function
import time								# Time library, use of sleep function

import LineUP_Middleware                    # Fluigent LineUP LineUP Middleware
from LineUP_Middleware import *

from LineUP_lowLevel import PRESSURE_UNIT   # Explicit import of available enumerations
from LineUP_lowLevel import FLOW_UNIT
from LineUP_lowLevel import PRESSURE_MODE
from LineUP_lowLevel import FLOW_UNIT_CALIBRATION_TABLE
from LineUP_lowLevel import TTL_PORT
from LineUP_lowLevel import TTL_MODE
from LineUP_lowLevel import POWER_STATE
from LineUP_lowLevel import FLOW_UNIT_TYPE

# This function uses "raw_input" for Python 2.x versions and "input" for Python 3.x versions
def keyInput(prompt):
    try:
        return raw_input(prompt)
    except NameError:
        return input(prompt)

# Main function executed when this file is called
if __name__ == "__main__":
    try:  
        # Initialize session, create LineUP object on first found device
        LineUP0 = LineUPClassicalSessionFactory().Create(0)
        LineUP1 = LineUPClassicalSessionFactory().Create(0)
        
#        print('Units: ' + str(LineUP0.GetPressureUnit(0)))
#        print('Units: ' + str(LineUP1.GetPressureUnit(1)))
        
        print("Pressure set to PSI")
        LineUP0.SetPressureUnit(0,PRESSURE_UNIT.PSI)
        LineUP1.SetPressureUnit(1,PRESSURE_UNIT.PSI)
      
        msg = ''
        aspirate_amount = 0.0
        dispense_amount = 0.0
        a = ''
        d = ''
        n = ''
        
        while msg != 'n':
            msg = input("a to aspirate, d to dispense, n to quit \n")
            if msg == 'a':
                aspirate_amount = input ("Enter a value to aspirate") 
                LineUP0.SetPressure(0, aspirate_amount)
                time.sleep(5)
            elif msg == 'd':
                dispense_amount = input ("Enter a value to dispense") 
                LineUP1.SetPressure(0, dispense_amount)
                time.sleep(5)
            elif msg == 'n':
                break
            
        print ("End of script")
        
        # Set pressure to 100 (mBar in default unit: mbar) on Flow EZ #1
#        print ('Set pressure to 10 (mBar is the default unit) on Flow EZ #1')
#        LineUP.SetPressure(0, 10)
#        print ('Waiting 10 seconds...')
#        time.sleep(10)
#        print ('Set pressure to 0 (mBar is the default unit) on Flow EZ #1')
#        LineUP.SetPressure(1, 10)
#        # Wait 10 seconds
#        print ('Waiting 10 seconds...')
#        time.sleep(10)
#
#        # Read pressure
#        print ('Read pressure on Flow EZ #1: ' + str(LineUP.GetPressure(0)))
#
#        # Exit application 
#        keyInput("Press ENTER to exit application")

    except Exception as e:      # Catch exception if occured
        print(e.args)
        print (e)
        

        