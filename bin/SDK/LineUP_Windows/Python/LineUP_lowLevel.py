import ctypes							# Used for variable definition types
from ctypes import *					# Used to load dynamic linked libraries
import platform							# Library used for x64 or x86 detection
import sys
from enum import Enum                   # Import enumeration library

## ================== Enumeration of global types =====================
## POWER_STATE is working status of the Link: OFF, ON
class POWER_STATE(Enum):
    OFF = 0
    ON = 1

## TTL mode in/out and falling/rising: DETECT_RISING_EDGE, DETECT_FALLING_EDGE, OUTPUT_PULSE_LOW, OUTPUT_PULSE_HIGH
class TTL_MODE(Enum):
    DETECT_RISING_EDGE = 0
    DETECT_FALLING_EDGE = 1
    OUTPUT_PULSE_LOW = 2
    OUTPUT_PULSE_HIGH = 3						

## BNC ports on Link: TTL1, TTL2
class TTL_PORT(Enum):
    TTL1 = 0
    TTL2 = 1

## FLOW_UNIT_TYPE: Type of the flow unit and available calibration table(s)
class FLOW_UNIT_TYPE(Enum):
    NONE = 0
    XS_single = 1
    S_single = 2
    S_dual = 3
    M_single = 4 
    M_dual = 5
    L_single = 6
    L_dual = 7 
    XL_single = 8

## Calibration table of flow units: H2O, IPA, HFE, FC40, OIL
class FLOW_UNIT_CALIBRATION_TABLE(Enum):
    H2O = 0
    IPA = 1
    HFE = 2
    FC40 = 3
    OIL = 4

## Pressure regulation mode: FAST, SMOOTH
class PRESSURE_MODE(Enum):
    FAST = 0
    SMOOTH = 1

## Pressure unit: mbar, kPa, PSI
class PRESSURE_UNIT(Enum):
    mbar = 0
    kPa = 1
    PSI = 2

## Flow-rate unit: nLperMin, uLperSec, uLperMin, uLperHour, mLperMin, mLperHour
class FLOW_UNIT(Enum):     
    nLperMin = 0     
    uLperSec = 1     
    uLperMin = 2    
    uLperHour = 3
    mLperMin = 4
    mLperHour = 5

## Unit Converter Tool: convert pressure or flow-rate values between different units

## Convert pressure value from mbar to a selected PRESSURE_UNIT
# Default read unit is mbar.
# param: out PRESSURE_UNIT
# param: value
# return: converted value
def ConvertPressureFromMbar(out, value):
    if (out == PRESSURE_UNIT.mbar):
        return float(value)
    if (out == PRESSURE_UNIT.kPa):
        return float(value / 10)
    if (out == PRESSURE_UNIT.PSI):
        return float(value / float(68.9476))
    return float(value)

## Convert pressure value to mbar from a selected PRESSURE_UNIT
# Default read unit is mbar.
# param: in PRESSURE_UNIT
# param: value
# return: converted value
def ConvertPressureToMbar(In, value):
    if (In == PRESSURE_UNIT.mbar):
        return float(value)
    if (In == PRESSURE_UNIT.kPa):
        return float(value * 10)
    if (In == PRESSURE_UNIT.PSI):
        return float(value * float(68.9476))
    return float(value)

## Convert flow-rate value from ul/min to a selected FLOW_UNIT
# Default read unit is ul/min.
# param: out FLOW_UNIT
# param: value
# return: converted value
def ConvertFlowFromUlperMin(out, value):
    if (out == FLOW_UNIT.nLperMin):
        return float(value * 1000)
    if (out == FLOW_UNIT.uLperSec):
        return float(value / 60)
    if (out == FLOW_UNIT.uLperMin):
        return float(value)
    if (out == FLOW_UNIT.uLperHour):
        return float(value * 60)
    if (out == FLOW_UNIT.mLperMin):
        return float(value / 1000)
    if (out == FLOW_UNIT.mLperHour):
        return float(value / 1000 * 60)
    return float(value)

## Convert flow-rate value to ul/min from a selected FLOW_UNIT
# Default read unit is ul/min.
# param: In FLOW_UNIT
# param: value
# return: converted value
def ConvertFlowToUlperMin(In, value):
    if (In == FLOW_UNIT.nLperMin):
        return float(value / 1000)
    if (In == FLOW_UNIT.uLperSec):
        return float(value * 60)
    if (In == FLOW_UNIT.uLperMin):
        return float(value)
    if (In == FLOW_UNIT.uLperHour):
        return float(value / 60)
    if (In == FLOW_UNIT.mLperMin):
        return float(value * 1000)
    if (In == FLOW_UNIT.mLperHour):
        return float((value * 1000) /60)
    return float(value)

        
## Global defines
libLineUP = ctypes.WinDLL
if (sys.maxsize > 2**32):
    libLineUP = ctypes.WinDLL('LineUP_c_64.dll')
else:
    libLineUP = ctypes.WinDLL('LineUP_c_32.dll')

## ================ High level abstract instruments ===================

## iInstrument : simple generic instrument high level class
class iInstrument:
    Handle = c_ulonglong(0)         ## Instrument session handler
    def __init__(self):
        ## Default constructor.
        self.Serial = 0             ## Instrument Serial Number
        self.VersionNumber = 0      ## Instrument Version Number

 ## iModule : simple generic module instrument high level class
class iModule(iInstrument):
    def __init__(self):
        ## Internal index of the module
        self.Module = 0             
        ## Internal status of the module
    def Status(self):
        return Status              

## iLink : simple generic Link instrument high level class
class iLink(iInstrument):
    ## Get Link device power state
    def ReadPower(self):
        pass
    ## Set Link device power on or off
    def SetPower(self, power):
        pass
    ## Total number of detected devices connected to the Link
    def ModulesNumber(self):
        pass


## ==================  Low level instruments =====================

##  Controller: Link Controller class
class Controller(iLink):
    ## This function displays an error message in case of LineUP returned status is not Ok. This is mandatory
    # Arguments:	functionName is the string name of called function
    #		errorCode is the returned code of called function 
    def manageReturnCode(self, code, functionName):
        if (code == 0):     # No error
            pass
        if (code == 1):     # Error USB closed
            print(functionName + ' Error Link USB closed')
        if (code == 2):     # Error command
            print(functionName + ' Error Link wrong command')
        if (code == 3):     # Error no module
            print(functionName + ' Error Link no module at this index')
        if (code == 4):     # Error wrong module
            print(functionName + ' Error Link wrong module')
        if (code == 5):     # Error module sleep
            print(functionName + ' Error Module is in sleep mode')

    ### Constructor: Load dll functions and initialize communication with the device.
    def __init__(self, serial):
        # Internal variables definition
        self.TTL_MODE_1 = TTL_MODE(TTL_MODE.DETECT_RISING_EDGE)
        self.TTL_MODE_2 = TTL_MODE(TTL_MODE.DETECT_RISING_EDGE)
        self.Handle = ctypes.c_ulonglong(0)
        ## Local variables definition
        localSerial = c_ushort(0)	
        localVersion = c_ushort(0)	
        # Load dll into memory, automatically detect python bitness
        if (sys.maxsize > 2**32):
            libLineUP = ctypes.WinDLL('LineUP_c_64.dll')
        else:
            libLineUP = ctypes.WinDLL('LineUP_c_32.dll')
       
        # Initialization function prototype
        lineup_initialization = libLineUP.lineup_initialization
        lineup_initialization.restype = c_ulonglong
        lineup_initialization.argtypes = [c_ushort]

		# Opens LineUP session with selected serial number USB device. If serial equals to 0, first found device will be used. 
        self.Handle = lineup_initialization(c_ushort(serial))

        # Check if device is connected
        libLineUP.lineup_get_serial(c_ulonglong(self.Handle), byref(localVersion), byref(localSerial))
        if(localSerial.value == 0):
            self.manageReturnCode(1, 'Link')

    ## Class destructor: unload ressources
    def __del__(self):
        # if(libLineUP != 0):
        if (sys.maxsize > 2**32):
           libLineUP = ctypes.WinDLL('LineUP_c_64.dll')
        else:
           libLineUP = ctypes.WinDLL('LineUP_c_32.dll')

        if(int(self.Handle) != 0):
            libLineUP.lineup_close(c_ulonglong(self.Handle))
        ctypes.windll.kernel32.FreeLibrary(libLineUP._handle)
        del libLineUP

    ## Functions

    ## Link Serial Number
    def Serial(self):
        localSerial = c_ushort(0)	
        localVersion = c_ushort(0)	
        localError = c_char()
        localError = libLineUP.lineup_get_serial(c_ulonglong(self.Handle), byref(localVersion), byref(localSerial))
        self.manageReturnCode(localError, 'Serial get')
        return localSerial.value

    ## Link Version Number
    def VersionNumber(self):
        localSerial = c_ushort(0)	
        localVersion = c_ushort(0)	
        localError = c_char()
        localError = libLineUP.lineup_get_serial(c_ulonglong(self.Handle), byref(localVersion), byref(localSerial))
        self.manageReturnCode(localError, 'Version number get')
        return localVersion.value

    ## Get Link device power state
    def ReadPower(self):
        localModuleNumber = c_ushort(0)	
        localError = c_char()
        localStatus = c_char()
        localError = libLineUP.lineup_get_module_number(c_ulonglong(self.Handle), byref(localStatus), byref(localModuleNumber))
        self.manageReturnCode(localError, 'Power get')
        return POWER_STATE(list(localStatus.value)[0])

    ## Set Link device power on or off
    def SetPower(self, power):
        localError = c_char()
        localError = libLineUP.lineup_power(c_ulonglong(self.Handle), power.value)
        self.manageReturnCode(localError, 'Power set')

    ## Get total number of detected devices connected to the Link
    def ModulesNumber(self):
        localModuleNumber = c_ushort(0)	
        localError = c_char()
        localStatus = c_char()
        localError = libLineUP.lineup_get_module_number(c_ulonglong(self.Handle), byref(localStatus), byref(localModuleNumber))
        self.manageReturnCode(localError, 'ModulesNumber')
        return localModuleNumber.value

    ## Set operating mode of TTL connectors
    def SetTTLMode(self, port, mode):
        localError = c_char()
        if(port == TTL_PORT.TTL1.value):
            self.TTL_MODE_1 = mode
        if(port == TTL_PORT.TTL2.value):
            self.TTL_MODE_2 = mode
        localError = libLineUP.lineup_set_ttl_mode(c_ulonglong(self.Handle), self.TTL_MODE_1.value, self.TTL_MODE_2.value)
        self.manageReturnCode(localError, 'SetTTLMode')
        
    ## Get TTL state
    def ReadTTL(self, TTL1, TTL2):
        localError = c_char()
        localError = libLineUP.lineup_get_ttl_state(c_ulonglong(self.Handle), 1, byref(c_char(TTL1)), byref(c_char(TTL2)))
        self.manageReturnCode(localError, 'ReadTTL')

    ## TriggerTTL
    def TriggerTTL(self, port):
        localError = c_char()
        if (port == TTL_PORT.TTL1.value):
            localError = libLineUP.lineup_trig_ttl(c_ulonglong(self.Handle), 0)
        if (port == TTL_PORT.TTL2.value):
            localError = libLineUP.lineup_trig_ttl(c_ulonglong(self.Handle), 1)
        self.manageReturnCode(localError, 'TriggerTTL')

##  Single LineUP channel, FlowEZ in this case
class Channel(iModule):
    # Variables definition
    FlowUnit = FLOW_UNIT.uLperMin						# Flow-rate unit: nLperMin, uLperSec, uLperMin, uLperHour, mLperMin, mLperHour
    PressureUnit = PRESSURE_UNIT.mbar				# Pressure unit: mbar, kPa, PSI 

    ## This function displays an error message in case of LineUP returned status is not Ok. This is mandatory
    # Arguments:	functionName is the string name of called function
    #		errorCode is the returned code of called function 
    def manageReturnCode(self, code, functionName):
        if (code == 0):     # No error
            pass
        if (code == 1):     # Error USB closed
            print(functionName + ' at index ' + str(self.Module) + ' Error Link USB closed')
        if (code == 2):     # Error command
            print(functionName + ' at index ' + str(self.Module) + ' Error Link wrong command')
        if (code == 3):     # Error no module
            print(functionName + ' at index ' + str(self.Module) + ' Error Link no module at this index')
        if (code == 4):     # Error wrong module
            print(functionName + ' at index ' + str(self.Module) + ' Error Link wrong module')
        if (code == 5):     # Error module sleep
            print(functionName + ' at index ' + str(self.Module) + ' Error Module is in sleep mode')

    ## Constructor: initialize protected variables
    def __init__(self, handle, module):
        self.Handle = handle
        self.Module = module

    ## Functions - Device settings
    ## Instrument Serial Number
    def Serial(self):
        localSerial = c_ushort(0)
        localVersion = c_ushort(0)
        localType = c_char()
        localError = c_char()
        localError = libLineUP.lineup_get_module_info(c_ulonglong(self.Handle), self.Module, byref(localType), byref(localVersion), byref(localSerial))
        self.manageReturnCode(localError, 'Serial get')
        return localSerial.value

    ## Instrument Version Number
    def VersionNumber(self):
        localSerial = c_ushort(0)
        localVersion = c_ushort(0)
        localType = c_char()
        localError = c_char()
        localError = libLineUP.lineup_get_module_info(c_ulonglong(self.Handle), self.Module, byref(localType), byref(localVersion), byref(localSerial))
        self.manageReturnCode(localError, 'VersionNumber get')
        return localVersion.value

    ## Instrument Status
    # Flow EZ status mask:
    # MSK_RUN      0x01
    # MSK_LOCAL    0x02
    # MSK_REGUL    0x04
    # MSK_STABLE   0x08
    # MSK_UNDERP   0x10
    # MSK_OVERP    0x20
    # MSK_QDETECT  0x80
    def Status(self):
        localTimeStamp = c_char()
        localStatus = c_char()
        localPressure = c_float(0)
        localFlowRate = c_float(0)
        localError = c_char()
        localError = libLineUP.flowez_read_module(c_ulonglong(self.Handle), self.Module, byref(localStatus), byref(localPressure), byref(localFlowRate), byref(localTimeStamp))
        self.manageReturnCode(localError, 'Status get')
        return localStatus.value

    ## Get pressure maximal range in set PRESSURE_UNIT
    def PressureRangeMax(self):
        localPressureMax = c_float(0)
        localFlowRateMax = c_float(0)
        localQtable = c_char()
        localError = c_char()
        Array = (c_char * 32)
        localQArtCode = Array()
        localError = libLineUP.flowez_module_config(c_ulonglong(self.Handle), self.Module, byref(localPressureMax), byref(localFlowRateMax), byref(localQtable), localQArtCode)
        self.manageReturnCode(localError, 'PressureRangeMax get')
        return float(abs(ConvertPressureFromMbar(self.PressureUnit,localPressureMax.value)))

    ## Get pressure minimal range in set PRESSURE_UNIT
    def PressureRangeMin(self):
        return float(0)        #  not implemented for FlowEZ

    ## Get flow-rate maximal range in set FLOW_UNIT
    def FlowrateRangeMax(self):
        localPressureMax = c_float(0)
        localFlowRateMax = c_float(0)
        localQtable = c_char()
        localError = c_char()
        Array = (c_char * 32)
        localQArtCode = Array()
        localError = libLineUP.flowez_module_config(c_ulonglong(self.Handle), self.Module, byref(localPressureMax), byref(localFlowRateMax), byref(localQtable), localQArtCode)
        self.manageReturnCode(localError, 'FlowrateRangeMax get')
        return float(ConvertFlowFromUlperMin(self.FlowUnit,localFlowRateMax.value))

    ## Get flow-rate minimal range in set FLOW_UNIT
    def FlowrateRangeMin(self):
        localPressureMax = c_float(0)
        localFlowRateMax = c_float(0)
        localQtable = c_char()
        localError = c_char()
        Array = (c_char * 32)
        localQArtCode = Array()
        localError = libLineUP.flowez_module_config(c_ulonglong(self.Handle), self.Module, byref(localPressureMax), byref(localFlowRateMax), byref(localQtable), localQArtCode)
        self.manageReturnCode(localError, 'FlowrateRangeMin get')
        return -float(ConvertFlowFromUlperMin(self.FlowUnit, localFlowRateMax.value))


    ## Get type of the flow unit and available calibration table(s)
    def FlowUnitType(self):
        localPressureMax = c_float(0)
        localFlowRateMax = c_float(0)
        localQtable = c_char()
        localError = c_char()
        Array = (c_char * 32)
        localQArtCode = Array()
        localType = FLOW_UNIT_TYPE.NONE
        localError = libLineUP.flowez_module_config(c_ulonglong(self.Handle), self.Module, byref(localPressureMax), byref(localFlowRateMax), byref(localQtable), localQArtCode)
        self.manageReturnCode(localError, 'FlowUnitArticleCode get')

        # Get sensor type from sensor article code
        if 'S' in str(localQArtCode):
            if 'X' in str(localQArtCode):
                localType = FLOW_UNIT_TYPE.XS_single
            else:
                if 'i' in str(localQArtCode):
                     localType = FLOW_UNIT_TYPE.S_dual
                else:
                     localType = FLOW_UNIT_TYPE.S_single
        if 'M' in str(localQArtCode):
            if 'i' in str(localQArtCode):
                localType = FLOW_UNIT_TYPE.M_dual
            else:
                localType = FLOW_UNIT_TYPE.M_single
        if 'L' in str(localQArtCode):
            if 'X' in str(localQArtCode):
                localType = FLOW_UNIT_TYPE.XL_single
            else:
                if 'i' in str(localQArtCode):
                     localType = FLOW_UNIT_TYPE.L_dual
                else:
                     localType = FLOW_UNIT_TYPE.L_single
        return localType

    ## Functions - read/write control
    ## Read pressure value in set PRESSURE_UNIT
    def ReadPressure(self):
        localTimeStamp = c_char()
        localStatus = c_char()
        localPressure = c_float(0)
        localFlowRate = c_float(0)
        localError = c_char()
        localError = libLineUP.flowez_read_module(c_ulonglong(self.Handle), self.Module, byref(localStatus), byref(localPressure), byref(localFlowRate), byref(localTimeStamp))
        self.manageReturnCode(localError, 'Pressure get')
        return ConvertPressureFromMbar(self.PressureUnit, localPressure.value)

    ## Set pressure value in set PRESSURE_UNIT
    def SetPressure(self, command):
        localError = c_char()
        localError = libLineUP.flowez_set_pressure(c_ulonglong(self.Handle), self.Module, c_float(ConvertPressureToMbar(self.PressureUnit, command)))
        self.manageReturnCode(localError, 'Pressure set')

    ## Read flow-rate value in set FLOW_UNIT
    def ReadFlowRate(self):
        localTimeStamp = c_char()
        localStatus = c_char()
        localPressure = c_float(0)
        localFlowRate = c_float(0)
        localError = c_char()
        localError = libLineUP.flowez_read_module(c_ulonglong(self.Handle), self.Module, byref(localStatus), byref(localPressure), byref(localFlowRate), byref(localTimeStamp))
        self.manageReturnCode(localError, 'FlowRate get')
        return ConvertFlowFromUlperMin(self.FlowUnit, localFlowRate.value)

    ## Set flow-rate value in set FLOW_UNIT
    def SetFlowRate(self, command):
        localError = c_char()
        localError = libLineUP.flowez_set_flowrate(c_ulonglong(self.Handle), self.Module, c_float(ConvertFlowToUlperMin(self.FlowUnit, command)))
        self.manageReturnCode(localError, 'FlowRate set')

    ## Read status of flow-rate regulation state
    def FlowrateControlStatus(self):
        localError = c_char()
        status = c_char()
        localError = libLineUP.flowez_get_flowcontrol_status(c_ulonglong(self.Handle), self.Module, byref(status))
        self.manageReturnCode(localError, 'FlowrateControlStatus')
        return status.value

    ## Functions - Custom
    ## Read flow unit sensor calibration table (0:H20; 1:IPA; 2:HFE; 3:FC40; 4:OIL) for M dual calibration type; (0:H20; 1:IPA) for XS, S, L and XL flow - units
    def ReadFlowCalibrationTable(self):
        localPressureMax = c_float(0)
        localFlowRateMax = c_float(0)
        localQtable = c_char()
        localError = c_char()
        Array = (c_char * 32)
        localQArtCode = Array()
        localError = libLineUP.flowez_module_config(c_ulonglong(self.Handle), self.Module, byref(localPressureMax), byref(localFlowRateMax), byref(localQtable), localQArtCode)
        self.manageReturnCode(localError, 'FlowCalibrationTable get')
        return FLOW_UNIT_CALIBRATION_TABLE(list(localQtable.value)[0])

    ## Set flow unit sensor calibration table (0:H20; 1:IPA; 2:HFE; 3:FC40; 4:OIL) for M dual calibration type; (0:H20; 1:IPA) for XS, S, L and XL flow - units
    def SetFlowCalibrationTable(self, unit):
        localError = c_char()
        localError = libLineUP.flowez_set_q_calibtable(c_ulonglong(self.Handle), self.Module, unit.value)
        self.manageReturnCode(localError, 'FlowCalibrationTable set')

    ## SRead actual pressure controller mode from FAST or SMOOTH
    def ReadPressureMode(self):
        localTimeStamp = c_char()
        localStatus = c_char()
        localPressure = c_float(0)
        localFlowRate = c_float(0)
        localError = c_char()
        localError = libLineUP.flowez_read_module(c_ulonglong(self.Handle), self.Module, byref(localStatus), byref(localPressure), byref(localFlowRate), byref(localTimeStamp))
        self.manageReturnCode(localError, 'PressureMode get')
        return PRESSURE_MODE((list(localStatus.value)[0] & 0x08) == 0x08)

    ## Set pressure controller mode from FAST or SMOOTH
    def SetPressureMode(self, mode):
        localError = c_char()
        localError = libLineUP.flowez_set_mode(c_ulonglong(self.Handle), self.Module, mode.value)
        self.manageReturnCode(localError, 'PressureMode set')

    ## Functions - Specific
    ## Send a calibration signal; module is not operational for next 5 seconds
    def CalibratePressureSensor(self):
        localError = c_char()
        localError = libLineUP.flowez_pressure_reset(c_ulonglong(self.Handle), self.Module)
        self.manageReturnCode(localError, 'CalibratePressureSensor')


## Instrument: LineUP Detect
def LineUPDetect():
    Array = (c_ushort * 256)
    localTable = Array()
    # Load dll into memory, automatically detect python bitness
    if (sys.maxsize > 2**32):
        libLineUP = ctypes.WinDLL('LineUP_c_64.dll')
    else:
        libLineUP = ctypes.WinDLL('LineUP_c_32.dll')
    libLineUP.lineup_detect(localTable)
    ctypes.windll.kernel32.FreeLibrary(libLineUP._handle)
    del libLineUP
    return localTable