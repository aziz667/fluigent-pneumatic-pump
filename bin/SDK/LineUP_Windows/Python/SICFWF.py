# -*- coding: utf-8 -*-
"""
Created on Fri Mar 20 15:41:39 2020

@author: auddin
"""

#This script will contain the steps of the workflow for the SIC 
#This script will be using the Fluigent software development kit and the hardware associated with it  


from __future__ import print_function
import time								# Time library, use of sleep function
import LineUP_Middleware                    # Fluigent LineUP LineUP Middleware
from LineUP_Middleware import *

from LineUP_lowLevel import PRESSURE_UNIT   # Explicit import of available enumerations
from LineUP_lowLevel import FLOW_UNIT
from LineUP_lowLevel import PRESSURE_MODE
from LineUP_lowLevel import FLOW_UNIT_CALIBRATION_TABLE
from LineUP_lowLevel import TTL_PORT
from LineUP_lowLevel import TTL_MODE
from LineUP_lowLevel import POWER_STATE
from LineUP_lowLevel import FLOW_UNIT_TYPE



# This function uses "raw_input" for Python 2.x versions and "input" for Python 3.x versions
def keyInput(prompt):
    try:
        return raw_input(prompt)
    except NameError:
        return input(prompt)

# Main function executed when this file is called
if __name__ == "__main__":
    try:  
        # Initialize session on all connected Links, create LineUP array object
     
        LineUP0 = LineUPClassicalSessionFactory().Create(0)
        LineUP1 = LineUPClassicalSessionFactory().Create(0)
        
#        print('Units: ' + str(LineUP0.GetPressureUnit(0)))
#        print('Units: ' + str(LineUP1.GetPressureUnit(1)))
        
        print("Pressure set to PSI")
        LineUP0.SetPressureUnit(0,PRESSURE_UNIT.PSI)
        LineUP1.SetPressureUnit(1,PRESSURE_UNIT.PSI)
#        
        
        print('Pressure Units: ' + str(LineUP0.GetPressureUnit(0)))
        print('Pressure Units: ' + str(LineUP1.GetPressureUnit(1)))
        
        
        print('Calibrating Pressure controller 1')
        LineUP0.CalibratePressureSensor(0)
        time.sleep(5)
        
        print('Calibrating Pressure controller 2')
        LineUP1.CalibratePressureSensor(1)
        time.sleep(5)
        
        print('Calibration complete')
     
        #push to mastermix and then mix
         # Set pressure to 5 (mBar in default unit: mbar) on Flow EZ #1
        print ('Set pressure to 1 PSI on Flow EZ #1')
        print ('Moving to MasterMix')
        LineUP0.SetPressure(0, 1)
        print ('Waiting 10 seconds...')
        time.sleep(10)
        print ('Now in MasterMix')
        LineUP0.SetPressure(0, 0)
        
        #mixing time
        mixing_value = 0
        while mixing_value < 3: 
            print ('Set pressure to 0.5 PSI on Flow EZ #1')
            print ('Beginning mixing ')
            LineUP0.SetPressure(0, 1)
            time.sleep(2)
            LineUP1.SetPressure(1, 1)
            time.sleep(2)
            print ('Waiting 3 seconds...')
            time.sleep(3)
            print("mixing iteration:" + str(mixing_value))
            mixing_value += 1
        
        LineUP0.SetPressure(0, 0)
        # Exit application 
        #keyInput("Press ENTER to exit application")

    except Exception as e:      # Catch exception if occured
        print(e.args)
        print (e)
        
        
        
        
        
        
        
        
        