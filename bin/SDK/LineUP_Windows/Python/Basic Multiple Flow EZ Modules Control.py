
#!/usr/bin/env python
#============================================================================
#                   Fluigent LineUP Python SDK                       
#----------------------------------------------------------------------------
#         Copyright (c) Fluigent 2018.  All Rights Reserved.                 
#----------------------------------------------------------------------------
#                                                                            
# Title:   Basic Multiple Flow EZ Modules Control.py											  
# Purpose: Interact with Fluigent LineUP devices using object orientated     
#				programming and Windows LineUP Middleware. 
#				This example shows how to set concurrent pressure or flowrate 
#   			orders on the two Flow EZ modules on two different LineUP chains  				  		  
# Version: 18.0.1        
# Software: "LineUP LineUP Middleware.py" is the Fluigent instrument interface        
# Hardware setup: Two Link each connected to a Flow EZ                                      
# Library version: 1.0.2.2													  
#                                                                            
#============================================================================

from __future__ import print_function
import time								# Time library, use of sleep function
import LineUP_lowLevel
import LineUP_Middleware                    # Fluigent LineUP LineUP Middleware
from LineUP_Middleware import *

from LineUP_lowLevel import PRESSURE_UNIT   # Explicit import of available enumerations
from LineUP_lowLevel import FLOW_UNIT
from LineUP_lowLevel import PRESSURE_MODE
from LineUP_lowLevel import FLOW_UNIT_CALIBRATION_TABLE
from LineUP_lowLevel import TTL_PORT
from LineUP_lowLevel import TTL_MODE
from LineUP_lowLevel import POWER_STATE
from LineUP_lowLevel import FLOW_UNIT_TYPE

# This function uses "raw_input" for Python 2.x versions and "input" for Python 3.x versions
def keyInput(prompt):
    try:
        return raw_input(prompt)
    except NameError:
        return input(prompt)

# Main function executed when this file is called
if __name__ == "__main__":
    try:  
        # Initialize session on all connected Links, create LineUP array object
        LineUP = LineUPClassicalSessionFactory().CreateAll()
        
        # Display serial numbers
        print ('Link #1 Flow EZ #1, serial number: ' + str(LineUP[0].FlowEZSerial(0)))
        print ('Link #2 Flow EZ #1, serial number: ' + str(LineUP[1].FlowEZSerial(0)))

        # Set pressure to 100 (mBar is the default unit) on Link #1 Flow EZ #1
        print('Link #1 Flow EZ #1: Set pressure to 100 mBar ')
        LineUP[0].SetPressure(0, 100)

        # Set flowrate to 1 (ul/min is the default unit) on Link #2 Flow EZ #1 
        print('Link #2 Flow EZ #1: Set flowrate to 1 ul/min ')
        LineUP[1].SetFlowrate(0, 1)

        # Wait 10 second
        time.sleep(10)

        # Set pressure to 50 (mBar is the default unit) on Link #2 Flow EZ #1
        # Note that flow regulation stops when a pressure order is sent
        print('Link #1 Flow EZ #1: Set pressure to 100 mBar ')
        LineUP[1].SetPressure(0, 50)

        # Exit application 
        keyInput("Press ENTER to exit application")

    except Exception as e:      # Catch exception if occured
        print(e.args)
        print (e)
        