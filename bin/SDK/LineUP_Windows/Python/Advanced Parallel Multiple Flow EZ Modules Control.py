
#!/usr/bin/env python
#============================================================================
#                   Fluigent LineUP Python SDK                       
#----------------------------------------------------------------------------
#         Copyright (c) Fluigent 2018.  All Rights Reserved.                 
#----------------------------------------------------------------------------
#                                                                            
# Title:   Basic Multiple Flow EZ Modules Control.py											  
# Purpose: Interact with Fluigent LineUP devices using object orientated     
#				programming and Windows LineUP Middleware. 
#				This example shows how to set concurrent pressure or flowrate 
#   			orders on the two Flow EZ modules on two different LineUP chains  				  		  
# Version: 18.0.1        
# Software: "LineUP LineUP Middleware.py" is the Fluigent instrument interface        
# Hardware setup: Two Link each connected to a Flow EZ                                      
# Library version: 1.0.2.2													  
#                                                                            
#============================================================================

from __future__ import print_function
import time								# Time library, use of sleep function
import random
import sys
from threading import Thread

import LineUP_Middleware                    # Fluigent LineUP LineUP Middleware
from LineUP_Middleware import *

from LineUP_lowLevel import PRESSURE_UNIT   # Explicit import of available enumerations
from LineUP_lowLevel import FLOW_UNIT
from LineUP_lowLevel import PRESSURE_MODE
from LineUP_lowLevel import FLOW_UNIT_CALIBRATION_TABLE
from LineUP_lowLevel import TTL_PORT
from LineUP_lowLevel import TTL_MODE
from LineUP_lowLevel import POWER_STATE
from LineUP_lowLevel import FLOW_UNIT_TYPE

# Global varible definition used to stop thread execution
CancellationToken = 1

# This function uses "raw_input" for Python 2.x versions and "input" for Python 3.x versions
def keyInput(prompt):
    try:
        return raw_input(prompt)
    except NameError:
        return input(prompt)

# Link #1 Flow EZ #1 is driven in pressure, every 2 seconds randomly
class task1(Thread):
    def __init__(self, mot):
        Thread.__init__(self)
        self.LineUP = LineUP

    def run(self):                          # Thread running function
        while (CancellationToken):          
            PressureOrder = random.randint(0,self.LineUP[0].GetPressureRangeMax(0))
            self.LineUP[0].SetPressure(0, PressureOrder)
            print ('New pressure order: ' + str(PressureOrder) + ' mbar')
            time.sleep(2)
       
# Link #2 Flow EZ #1 is driven in flow, every 5 seconds, randomly
class task2(Thread):
    def __init__(self, mot):
        Thread.__init__(self)
        self.LineUP = LineUP

    def run(self):                          # Thread running function
        while (CancellationToken):          
            FlowrateOrder = random.randint(0,self.LineUP[1].GetFlowrateRangeMax(0))
            self.LineUP[1].SetFlowrate(0, FlowrateOrder)
            print ('New flowrate order: ' + str(FlowrateOrder) + ' ul/min ')
            time.sleep(5)

# Main function executed when this file is called
if __name__ == "__main__":
    try:  
        # Initialize session on all connected Links, create LineUP array object
        LineUP = LineUPClassicalSessionFactory().CreateAll()

        # Construct two threads and run. Does not block execution.
        thread_1 = task1(LineUP)
        thread_2 = task2(LineUP)

        # Start thread execution
        thread_1.start()
        thread_2.start()

        # Wait 10 seconds
        time.sleep(10)

        # Stop threads
        CancellationToken = 0
        thread_1.join()
        thread_2.join()

        print ('Application is closing...')

        # Exit application 
        keyInput("Press ENTER to exit application")

    except Exception as e:      # Catch exception if occured
        print(e.args)
        print (e)
        