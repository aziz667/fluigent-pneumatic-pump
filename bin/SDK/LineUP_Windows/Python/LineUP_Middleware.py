import LineUP_lowLevel
from LineUP_lowLevel import *

## LineUP Classical Session Factory class : high level factory class of ClassicalLineUPSession
# This class' methods support all available actions on Link and Flow EZ LineUP hardwares 
class LineUPClassicalSessionFactory:
    ## Create single instance of LineUP Session. Use this method if only one Link is used. If no serial is set (=0), first found device will be initialized
    def Create(self, LinkSerial):
        return ClassicalLineUPSession(LinkSerial)

    ## Create multiple instances of LineUP Session. Use this method if multiple Link are connected and only specific modules are used. LinkSerial is an array of Link Serial numbers to initialize.
    def CreateMultiple(self, LinkSerial):
        mClassicalLineUPSession = []
        for localLoop in range(0, len(LinkSerial)):
            mClassicalLineUPSession.append(ClassicalLineUPSession(LinkSerial[localLoop]))
        return mClassicalLineUPSession

    ## Create all LineUP Session from all found Link module(s).
    def CreateAll(self):
        localN = 0
        localTable = LineUPDetect()
        mClassicalLineUPSession = []
        for localCounter in range(0, len(localTable)):
            if(localTable[localCounter] != 0):
                localN = localN +1
        for localLoop in range(0, localN):
            mClassicalLineUPSession.append(ClassicalLineUPSession(localTable[localLoop])) # localTable[localLoop]
        return mClassicalLineUPSession

## Convert pressure value from a selected PRESSURE_UNIT to another
# @param: PRESSURE_UNIT In 
# @param: PRESSURE_UNIT out 
# @param: value
# @returns converted value
def ConvertPressure(In, out, value):
    localValueMbar = ConvertPressureToMbar(In, value)
    return ConvertPressureFromMbar(out, localValueMbar)


## Convert flow-rate value from a selected FLOW_UNIT to another
# @param: FLOW_UNIT In
# @param: FLOW_UNIT out
# @param: value
# @returns converted value
def ConvertFlow(In, out, value):
    localValue = ConvertFlowToUlperMin(In, value)
    return ConvertFlowFromUlperMin(out, localValue)

## Classical LineUP Session class : high level class containing a Link and channels
class ClassicalLineUPSession:            
    def __init__(self, LinkSerial):
        # Private values
        self.Link = Controller(LinkSerial)      # Create new Link Controller Object. This object will handle all connected modules
        self.FlowEZ = []
        for localLoop in range(0, 8):
            self.FlowEZ.append(Channel(self.Link.Handle, localLoop))

    ## Read Link serial number
    # @returns Link serial number
    def LinkSerial(self):
        return self.Link.Serial()

    ## Read Link firmware version number
    # @returns Link Version
    def LinkVersion(self):
        return self.Link.VersionNumber()

    ## Read specific module serial number
    # @param: module index of the FlowEZ in the LineUP chain
    # @returns FlowEZ Serial
    def FlowEZSerial(self, module):
        return (self.FlowEZ[module].Serial())

    ## Read specific module firmware version number
    # @param: module index of the FlowEZ in the LineUP chain
    # @returns FlowEZ Version
    def FlowEZVersion(self, module):
        return (self.FlowEZ[module].VersionNumber())

    ## Sets the current working pressure unit for all Flow EZs All pressure orders, measurements or settings for this Flow EZ are expressed in this unit.
    # See PRESSURE_UNIT enum for details. Default value is mbar.
    # @param: unit 
    def SetSessionPressureUnit(self, unit):
        for localLoop in range(0, 8):
            self.FlowEZ[localLoop].PressureUnit = unit

    ## Power On or Off Link module
    # @param: power
    def SetPower(self, state):
        self.Link.SetPower(POWER_STATE(state))
        
    ## Read power state (On/Off) of Link module
    # @returns power
    def GetPower(self):
        return POWER_STATE(self.Link.ReadPower())

    ## Returns total number of connected modules on Link
    # @returns modules number
    def ModulesNumber(self):
        return self.Link.ModulesNumber()

    ## Configure a specific TTL port (BNC port on Link) as input, output, rising or falling edge.
    # @param: port TTL_PORT
    # @param: mode TTL_MODE
    def SetTTLMode(self, port, mode):
        self.Link.SetTTLMode(port, mode)

    ## Read TTL ports (BNC port on Link) if set as input.
    # @returns port1 TTL_PORT
    # @returns port2 TTL_PORT
    def ReadTTL(self, port1, port2):
        self.Link.ReadTTL(port1, port2)

    ## Trigger a specific TTL port (BNC port on Link) if set as output.
    # @param: port
    def TriggerTTL(self, port):
        self.Link.TriggerTTL(port)

    ## Return device minimal pressure value in selected PRESSURE_UNIT
    # @param: module index of the FlowEZ in the LineUP chain
    # @returns pressure min range
    def GetPressureRangeMin(self, module):
        return self.FlowEZ[module].PressureRangeMin()

    ## Return device maximal pressure value in selected PRESSURE_UNIT
    # @param: module index of the FlowEZ in the LineUP chain
    # @returns pressure max range
    def GetPressureRangeMax(self, module):
        return self.FlowEZ[module].PressureRangeMax()

    ## Return device minimal sensor flowrate value in selected FLOW_UNIT
    # @param: module index of the FlowEZ in the LineUP chain
    # @returns flow-rate min range
    def GetFlowrateRangeMin(self, module):
        return self.FlowEZ[module].FlowrateRangeMin()

    ## Return device maximal sensor flowrate value in selected FLOW_UNIT
    # @param: module index of the FlowEZ in the LineUP chain
    # @returns flow-rate max range
    def GetFlowrateRangeMax(self, module):
        return self.FlowEZ[module].FlowrateRangeMax()

    ## Send a calibration signal; module is not operational for next 5 seconds
    # @param: module index of the FlowEZ in the LineUP chain
    def CalibratePressureSensor(self, module):
        self.FlowEZ[module].CalibratePressureSensor()

    ## Get type of the flow unit and available calibration table(s)
    # @param: module index of the FlowEZ in the LineUP chain
    # @returns FLOW_UNIT_TYPE
    def FlowUnitType(self, module):
        return self.FlowEZ[module].FlowUnitType()

    
    ## Read actual pressure controller mode
    # FAST or SMOOTH
    # @param: module index of the FlowEZ in the LineUP chain
    # @returns pressure mode
    def GetPressureMode(self, module):
        return self.FlowEZ[module].ReadPressureMode()

    ## Set pressure controller mode
    # FAST or SMOOTH
    # @param: module index of the FlowEZ in the LineUP chain
    # @param: mode is pressure controller mode FAST or SMOOTH
    def SetPressureMode(self, module, mode):
        self.FlowEZ[module].SetPressureMode(mode)

    ## Retrieves the current working pressure unit of the specified Flow EZ All pressure orders, measurements or settings for this Flow EZ are expressed in this unit
    # @param: module index of the FlowEZ in the LineUP chain
    # @returns pressure unit PRESSURE_UNIT
    def GetPressureUnit(self, module):
        return self.FlowEZ[module].PressureUnit

    ## Change module pressure unit from mbar, kPa, or PSI
    # All orders and read values are then in selected unit. Default value is mbar.
    # @param: module index of the FlowEZ in the LineUP chain
    # @param: unit PRESSURE_UNIT
    def SetPressureUnit(self, module, unit):
        self.FlowEZ[module].PressureUnit = unit

    ## Retrieves the current working flowrate unit of the specified Flow EZ All flowrate orders, measurements or settings for this Flow EZ are expressed in this unit
    # @param: module index of the FlowEZ in the LineUP chain
    # @returns pressure unit FLOW_UNIT
    def GetFlowUnit(self, module):
        return self.FlowEZ[module].FlowUnit

    ## Sets the current working flowrate unit of the specified Flow EZ All flowrate orders, measurements or settings for this Flow EZ are expressed in this unit)
    # Default value is ul/min, uLperMin in enumeration list.
    # @param: module index of the FlowEZ in the LineUP chain
    # @param: unit FLOW_UNIT
    def SetFlowUnit(self, module, unit):
        self.FlowEZ[module].FlowUnit = unit

    ## Change flow sensor calibration table
    # Default value is H2O (water). See enumeration list for further details.
    # @param: module index of the FlowEZ in the LineUP chain
    # @param: calibration table FLOW_UNIT_CALIBRATION_TABLE
    def SetCalibrationTable(self, module, table):
        self.FlowEZ[module].SetFlowCalibrationTable(table)

    ## Read flow sensor calibration table
    # @param: module index of the FlowEZ in the LineUP chain
    # @returns flow-unit calibration table FLOW_UNIT_CALIBRATION_TABLE
    def GetCalibrationTable(self, module):
        return self.FlowEZ[module].ReadFlowCalibrationTable()

    ## Get measured pressure in selected PRESSURE_UNIT
    # @param: module index of the FlowEZ in the LineUP chain
    # @param: pressure
    def GetPressure(self, module):
        return self.FlowEZ[module].ReadPressure()

    ## Send pressure order in selected PRESSURE_UNIT
    # @param: module index of the FlowEZ in the LineUP chain
    # @param: pressure
    def SetPressure(self, module, pressure):
        self.FlowEZ[module].SetPressure(pressure)

    ## Send pressure order in selected FLOW_UNIT
    # Default unit is ul/min
    # @param: module index of the FlowEZ in the LineUP chain
    # @param: flowrate
    def SetFlowrate(self, module, flowrate):
        self.FlowEZ[module].SetFlowRate(flowrate)

    ## Read module flow sensor value in selected FLOW_UNIT
    # @param: module index of the FlowEZ in the LineUP chain
    # @returns flowrate
    def GetFlowrate(self, module):
        return self.FlowEZ[module].ReadFlowRate()

    ## Read flowrate regulation status when module is in flowrate control
    # @param: module index of the FlowEZ in the LineUP chain
    # @returns status of flowrate control : 0 = OK, 1 = error: not able achieve requested flowrate
    def ReadFlowrateControlStatus(self, module):
        return self.FlowEZ[module].FlowrateControlStatus()

    ## Returns status of the module
    # Refer to documentation for detailed bit assignment   
    # @param: module index
    # @returns Flow EZ status mask
    #
    # MSK_RUN      0x01    
    # MSK_LOCAL    0x02    
    # MSK_REGUL    0x04    
    # MSK_STABLE   0x08    
    # MSK_UNDERP   0x10    
    # MSK_OVERP    0x20    
    # MSK_QDETECT  0x80
    def ModuleStatus(self, module):
        return self.FlowEZ[module].Status()


