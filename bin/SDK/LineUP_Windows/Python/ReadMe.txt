﻿===========================================================================
				Python LineUP SDK
===========================================================================

This file contains a summary of LineUP SDK python middleware and basic examples.

-------------------------- SOFTWARE ---------------------------------------
The software was implemented and tested on Windows x64 platform using python 2.7 and 3.4.6 versions

------------------------ INSTALLATION -------------------------------------
The application is based on a dynamically linked library "LineUP_c_32.dll" and "LineUP_c_64.dll".
LineUP_lowLevel.py file contains dll functions call.
LineUP_Middleware.py file is the entry point to the LineUP interface. Create a session using LineUPClassicalSessionFactory object then all functions are available through the interface. 
Five examples are provided and can be executed out of the box.

Code can be directly executed in a basic windows Command Prompt (run "cmd") or in a Linux Terminal.
Note: you may need to go in the corresponding folder with "cd ../examples/Python" command
Depending on python version, you may need to copy "enum" folder to <Python folder>/Lib.

Using an IDE tool such as Spyder, Eclipse, Visual Studio may be easier to use.

--------------------------- FILES ----------------------------------------
List of project files:

LineUP_lowLevel.py
    Low level interface to LineUP dll

LineUP_Middleware.py
    User interface used to access LineUP functionalities
	
Basic Set Pressure.py
	Simple example to set a pressure
	
Basic Set Flowrate.py
	Simple example to set a flowrate
	
Basic Pressure Ramp.py
	Simple example to set a pressure ramp over time
	
Basic Multiple Flow EZ Modules Control.py
	Simple example to set pressures on different multiple Link modules

Advanced Parallel Multiple Flow EZ Modules Control.py
	Advanced example using threads in order to apply pressure asynchronously

LineUP_c_64.dll
    This is the dynamic linked library used by 64bit systems in order to control the device

LineUP_c_32.dll
   This is the dynamic linked library used by 32bit systems in order to control the device
