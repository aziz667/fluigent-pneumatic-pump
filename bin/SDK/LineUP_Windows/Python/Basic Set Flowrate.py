

#!/usr/bin/env python
#============================================================================
#                   Fluigent LineUP Python SDK                       
#----------------------------------------------------------------------------
#         Copyright (c) Fluigent 2018.  All Rights Reserved.                 
#----------------------------------------------------------------------------
#                                                                            
# Title:   Basic Set Flowrate.py											  
# Purpose: Interact with Fluigent LineUP devices using object orientated     
#				programming and Windows LineUP Middleware. 
#				This example shows how to set a flowrate order on the first
#				Flow EZ module of your LineUP chain    				  		  
# Version: 18.0.1        
# Software: "LineUP LineUP Middleware.py" is the Fluigent instrument interface        
# Hardware setup: One Link connected to one Flow EZ with a flow unit                                        
# Library version: 1.0.2.2													  
#                                                                            
#============================================================================

from __future__ import print_function
import time								# Time library, use of sleep function

import LineUP_Middleware                    # Fluigent LineUP LineUP Middleware
from LineUP_Middleware import *

from LineUP_lowLevel import PRESSURE_UNIT   # Explicit import of available enumerations
from LineUP_lowLevel import FLOW_UNIT
from LineUP_lowLevel import PRESSURE_MODE
from LineUP_lowLevel import FLOW_UNIT_CALIBRATION_TABLE
from LineUP_lowLevel import TTL_PORT
from LineUP_lowLevel import TTL_MODE
from LineUP_lowLevel import POWER_STATE
from LineUP_lowLevel import FLOW_UNIT_TYPE

# This function uses "raw_input" for Python 2.x versions and "input" for Python 3.x versions
def keyInput(prompt):
    try:
        return raw_input(prompt)
    except NameError:
        return input(prompt)

# Main function executed when this file is called
if __name__ == "__main__":
    try:  
        # Initialize session, create LineUP object on first found device
        LineUP = LineUPClassicalSessionFactory().Create(0)

        # Set flowrate to 1 (ul/min in default unit) on Flow EZ #1
        print ('Set flowrate to 1 (ul/min is the default unit) on Flow EZ #1')
        LineUP.SetFlowrate(0, 1)
        
        time.sleep(1)

        # Wait 10 seconds
        print ('Waiting 10 seconds...')
        time.sleep(10)

        # Read pressure
        print ('Read flowrate on Flow EZ #1: ' + str(LineUP.GetFlowrate(0)))

        # Exit application 
        keyInput("Press ENTER to exit application")

    except Exception as e:      # Catch exception if occured
        print(e.args)
        print (e)
        