﻿using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace LowLevel
{
    public static class LineUpDll
    {
#if WIN64
        public const string DLL_FILE_NAME = "SharedLibs\\Windows\\LineUP\\LineUP_c_64.dll";
#else
        public const string DLL_FILE_NAME = "SharedLibs\\Windows\\LineUP\\LineUP_c_32.dll";
        #endif

        private struct CommonErrors
        {
            public Error Code { get; set; }
            public string Caption { get; set; }
        }

        public enum Error
        {
            NoError = 0,
            UsbClosed = 1,
            InvalidCmd = 2,
            NoModule = 3,
            WrongModule = 4,
            MskStable = 8,
            MskUnderp = 16,
            MskOverp = 32,
            MskQdetect = 128,
            LineupErrUsbClosed = 245,
            LineupErrCommand = 246,
            LineupErrNoSwitch = 247,
            LineupErrWrongSwitch = 248,
            LineupErrPresence = 249,
            LineupErrCalibration = 250,
            LineupErrorProcessing = 251,
            LineupErrorMove = 252,
            LineupRead = 253,
            LineupCommonError = 254,
        }

        private static List<CommonErrors> ListCommonError => new List<CommonErrors>
        {
            new CommonErrors {Code = Error.UsbClosed, Caption = "K_ERR_CLOSED"},
            new CommonErrors {Code = Error.InvalidCmd, Caption = "K_ERR_CMD"},
            new CommonErrors {Code = Error.NoModule, Caption = "K_ERR_NO_MODULE"},
            new CommonErrors {Code = Error.WrongModule, Caption = "K_ERR_WRONG_MODULE"},
            new CommonErrors {Code = Error.MskStable, Caption = "E_MSK_STABLE"},
            new CommonErrors {Code = Error.MskUnderp, Caption = "E_MSK_UNDERP"},
            new CommonErrors {Code = Error.MskOverp, Caption = "E_MSK_OVERP"},
            new CommonErrors {Code = Error.MskQdetect, Caption = "E_MSK_QDETECT"},
            new CommonErrors {Code = Error.LineupErrUsbClosed, Caption = "lineup_ERR_USB_CLOSED"},
            new CommonErrors {Code = Error.LineupErrCommand, Caption = "lineup_ERR_COMMAND"},
            new CommonErrors {Code = Error.LineupErrNoSwitch, Caption = "lineup_ERR_NO_SWITCH"},
            new CommonErrors {Code = Error.LineupErrWrongSwitch, Caption = "lineup_ERR_WRONG_SWITCH"},
            new CommonErrors {Code = Error.LineupErrPresence, Caption = "lineup_ERR_PRESENCE"},
            new CommonErrors {Code = Error.LineupErrCalibration, Caption = "lineup_ERR_CALIBRATION"},
            new CommonErrors {Code = Error.LineupErrorProcessing, Caption = "lineup_ERROR_PROCESSING"},
            new CommonErrors {Code = Error.LineupErrorMove, Caption = "lineup_ERROR_MOVE"},
            new CommonErrors {Code = Error.LineupRead, Caption = "lineup_READ"},
            new CommonErrors {Code = Error.LineupCommonError, Caption = "lineup_COMMON_ERROR"},
            //new common_errors{ codeErr = 0, Caption = "lineup_OK" }
        };

        /// <summary>
        /// Error check function. Transform SDK call to DllFunctionCallResult
        /// </summary>
        /// <param name="toCheck"></param>
        /// <returns></returns>
        public static DllFunctionCallResult ErrorCheck(int toCheck)
        {
            return toCheck == (byte) Error.NoError
                ? new DllFunctionCallResult {Success = true} :

                throw new Exception(ListCommonError.Find(o => (byte)o.Code == toCheck).Caption);
        }

        public class DllFunctionCallResult
        {
            public bool Success { get; set; }
            public string Error { get; set; }
        }

        #region LineUp Imports

        [DllImport(DLL_FILE_NAME)]
        public static extern ulong lineup_initialization(ushort serial_number);
        [DllImport(DLL_FILE_NAME)]
        public static extern byte lineup_close(ulong handle);
        [DllImport(DLL_FILE_NAME)]
        public static extern byte lineup_power(ulong handle, byte mode);
        [DllImport(DLL_FILE_NAME)]
        public static extern byte lineup_get_serial(ulong handle, ref ushort version, ref ushort serial);
        [DllImport(DLL_FILE_NAME)]
        public static extern byte lineup_get_module_number(ulong handle, ref byte status, ref byte moduleNumber);
        [DllImport(DLL_FILE_NAME)]
        public static extern byte lineup_get_module_info(ulong handle, byte module, ref byte type, ref ushort version, ref ushort serial);
        [DllImport(DLL_FILE_NAME)]
        public static extern byte flowez_read_module(ulong handle, byte module, ref byte status, ref float pressure, ref float flowrate, ref byte TimeStamp);
        [DllImport(DLL_FILE_NAME)]
        public static extern byte flowez_module_data(ulong handle, byte module, ref byte P_unit, ref ushort P_max, ref ushort P_fullScale, ref ushort P_zero, ref byte Q_calibTable, ref byte Q_resolution, ref byte Q_scaleFactor, ref byte Q_unit, ref byte Q_timeBase, byte[] Q_artCode);
        [DllImport(DLL_FILE_NAME)]
        public static extern byte flowez_module_config(ulong handle, byte module, ref float P_range, ref float Q_range, ref byte Q_calibTable, byte[] Q_artCode);
        [DllImport(DLL_FILE_NAME)]
        public static extern byte lineup_get_ttl_state(ulong handle, byte TTL_ack, ref byte TTL_state1, ref byte TTL_state2);
        [DllImport(DLL_FILE_NAME)]
        public static extern byte lineup_set_ttl_mode(ulong handle, byte TTL_mode1, byte TTL_mode2);
        [DllImport(DLL_FILE_NAME)]
        public static extern byte lineup_trig_ttl(ulong handle, byte TTL_index);
        [DllImport(DLL_FILE_NAME)]
        public static extern byte flowez_set_pressure(ulong handle, byte module, float P_cmd);
        [DllImport(DLL_FILE_NAME)]
        public static extern byte flowez_set_flowrate(ulong handle, byte module, float Q_cmd);
        [DllImport(DLL_FILE_NAME)]
        public static extern byte flowez_get_flowcontrol_status(ulong handle, byte module, ref byte status);
        [DllImport(DLL_FILE_NAME)]
        public static extern byte flowez_set_mode(ulong handle, byte module, byte mode);
        [DllImport(DLL_FILE_NAME)]
        public static extern byte flowez_pressure_reset(ulong handle, byte module);
        [DllImport(DLL_FILE_NAME)]
        public static extern byte flowez_set_q_calibtable(ulong handle, byte module, byte Q_calibTable);
        [DllImport(DLL_FILE_NAME)]
        public static extern byte lineup_detect(ushort[] table);

        #endregion
    }
}
