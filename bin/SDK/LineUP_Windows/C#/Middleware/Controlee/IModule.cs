﻿namespace Middleware.Controlee
{
    public interface IModule: IInstrument
    {
        /// <summary>
        /// Module's index within the LineUP chain
        /// </summary>
        int Module { get; }

        /// <summary>
        /// Module's current status
        /// </summary>
        byte Status { get; }
    }
}