﻿using System;
using LowLevel;
using static LowLevel.LineUpDll;

namespace Middleware.Controlee
{
    public class FlowEZ: IModule
    {
        private readonly float _pressureRangeMin = 0;
        private readonly float _pressureRangeMax;
        private readonly float _flowrateMin;
        private readonly float _flowrateMax;
        private FLOW_UNIT_CALIBRATION_TABLE _flowCalibrationTable = FLOW_UNIT_CALIBRATION_TABLE.H20;
        private byte _status;

        public FlowEZ(ulong handle, int serialNumber, int module)
        {
            Handle = handle;
            SerialNumber = serialNumber;
            Module = module;

            byte type = 0;
            ushort firmware = 0;
            ushort serial = 0;
            var test = ErrorCheck(lineup_get_module_info(Handle, (byte) Module, ref type, ref firmware, ref serial));
            if(test.Success) VersionNumber = firmware;

            float pRange = 0;
            float qRange = 0;
            byte qCalibTable = 0;
            byte[] qArtCode = new byte[16];
            test = ErrorCheck(flowez_module_config(handle, (byte) Module, ref pRange, ref qRange, ref qCalibTable, qArtCode));

            if (test.Success)
            {
                artCode = System.Text.Encoding.Default.GetString(qArtCode);
                _pressureRangeMax = pRange;
                _flowrateMax = qRange;
                _flowrateMin = -qRange;
                FlowCalibrationTable = (FLOW_UNIT_CALIBRATION_TABLE) qCalibTable;
            }
        }

        /// <inheritdoc />
        public ulong Handle { get; }

        /// <inheritdoc />
        public int VersionNumber { get; }

        /// <inheritdoc />
        public int SerialNumber { get; }

        /// <inheritdoc />
        public int Module { get; }

        /// <inheritdoc />
        public byte Status
        {
            get
            {
                float pressure = 0;
                float flow = 0;
                byte timestamp = 0;
                ErrorCheck(LineUpDll.flowez_read_module(Handle, (byte) Module, ref _status, ref pressure, ref flow, ref timestamp));
                return _status;
            }
        }

        /// <summary>
        /// Connected Flow unit article code
        /// </summary>
        public string artCode { get;}

        /// <summary>
        /// The current flowrate unit this instrument works with
        /// </summary>
        public UnitConverter.FLOW_UNIT FlowUnit { get; set; } = UnitConverter.FLOW_UNIT.ulperMin;

        /// <summary>
        /// The current pressure unit this instrument works with
        /// </summary>
        public UnitConverter.PRESSURE_UNIT PressureUnit { get; set; } = UnitConverter.PRESSURE_UNIT.mbar;

        /// <summary>
        /// The minimum pressure this instrument can deliver
        /// </summary>
        public float PressureRangeMin => UnitConverter.ConvertPressure(UnitConverter.PRESSURE_UNIT.mbar, PressureUnit, _pressureRangeMin);

        /// <summary>
        /// The maximum pressure this instrument can deliver
        /// </summary>
        public float PressureRangeMax => UnitConverter.ConvertPressure(UnitConverter.PRESSURE_UNIT.mbar, PressureUnit, _pressureRangeMax);

        /// <summary>
        /// The minimim flowrate this instrument can deliver
        /// </summary>
        public float FlowrateMin => UnitConverter.ConvertFlow(UnitConverter.FLOW_UNIT.ulperMin, FlowUnit, _flowrateMin);

        /// <summary>
        /// The maximum flowrate this instrument can deliver
        /// </summary>
        public float FlowrateMax => UnitConverter.ConvertFlow(UnitConverter.FLOW_UNIT.ulperMin, FlowUnit, _flowrateMax);

        /// <summary>
        /// The flowrate calibration table the instrument uses
        /// </summary>
        public FLOW_UNIT_CALIBRATION_TABLE FlowCalibrationTable
        {
            get => _flowCalibrationTable;
            set
            {
                _flowCalibrationTable = value;
                ErrorCheck(LineUpDll.flowez_set_q_calibtable(Handle, (byte) Module, (byte)_flowCalibrationTable));
            }
        }

        /// <summary>
        /// The connected Flow unit extended type
        /// </summary>
        public FlowUnitTypeEx ConnectedFlowUnitType
        {
            get
            {
                var isDual = IsDual(artCode);
                var type = GetTypeFromArtCode(artCode);

                switch (type)
                {
                    case FlowUnitType.Unknown:
                        return FlowUnitTypeEx.None;
                    case FlowUnitType.XS:
                        return FlowUnitTypeEx.XSsingle;
                    case FlowUnitType.S:
                        return isDual ? FlowUnitTypeEx.Sdouble : FlowUnitTypeEx.Ssingle;
                    case FlowUnitType.M:
                        return isDual ? FlowUnitTypeEx.Mdouble : FlowUnitTypeEx.Msingle;
                    case FlowUnitType.L:
                        return isDual ? FlowUnitTypeEx.Ldouble : FlowUnitTypeEx.Lsingle;
                    case FlowUnitType.XL:
                        return FlowUnitTypeEx.XLsingle;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        /// <summary>
        /// Sets the pressure settling mode
        /// </summary>
        /// <param name="mode">New mode to set</param>
        /// <returns></returns>
        public DllFunctionCallResult SetPressureMode(PRESSURE_MODE mode)
        {
            return ErrorCheck(flowez_set_mode(Handle, (byte) Module, (byte) mode));
        }

        /// <summary>
        /// Sets the instrument Pcmd
        /// </summary>
        /// <param name="order">New Pcmd</param>
        /// <returns></returns>
        public DllFunctionCallResult SetPressure(float order)
        {
            return ErrorCheck(flowez_set_pressure(Handle, (byte) Module, UnitConverter.ConvertPressure(PressureUnit, UnitConverter.PRESSURE_UNIT.mbar, order)));
        }

        /// <summary>
        /// Reads the currently applied pressure
        /// </summary>
        /// <param name="value">Applied pressure</param>
        /// <returns></returns>
        public DllFunctionCallResult ReadPressure(ref float value)
        {
            byte status = 0;
            float flowrate = 0;
            byte timeStamp = 0;
            float pressure = 0;
            var result = ErrorCheck(flowez_read_module(Handle, (byte) Module, ref status, ref pressure, ref flowrate, ref timeStamp));
            value = UnitConverter.ConvertPressure(UnitConverter.PRESSURE_UNIT.mbar, PressureUnit, pressure);
            return result;
        }

        /// <summary>
        /// Sets the instrument Qcmd
        /// </summary>
        /// <param name="order">New Qcmd</param>
        /// <returns></returns>
        public DllFunctionCallResult SetFlowrate(float order)
        {
            return ErrorCheck(flowez_set_flowrate(Handle, (byte) Module, UnitConverter.ConvertFlow(FlowUnit, UnitConverter.FLOW_UNIT.ulperMin, order)));
        }

        /// <summary>
        /// Reads the currently applied flowrate
        /// </summary>
        /// <param name="value">Applied flowrate</param>
        /// <returns></returns>
        public DllFunctionCallResult ReadFlowrate(ref float value)
        {
            byte status = 0;
            float pressure = 0;
            byte timeStamp = 0;
            float flowrate = 0;
            var result = ErrorCheck(flowez_read_module(Handle, (byte)Module, ref status, ref pressure, ref flowrate, ref timeStamp));
            value = UnitConverter.ConvertFlow(UnitConverter.FLOW_UNIT.ulperMin, FlowUnit, flowrate);
            return result;
        }

        /// <summary>
        /// Opens the output valve, and saves current atmospherical pressure as zero sensor value for the specified Flow EZ module. 
        /// This operation takes 5 seconds; no order should be sent to the Flow EZ module in the meanwhile.
        /// </summary>
        /// <returns></returns>
        public DllFunctionCallResult CalibratePressure()
        {
            return ErrorCheck(flowez_pressure_reset(Handle, (byte) Module));
        }

        /// <summary>
        /// Reads the flowrate control status flag
        /// </summary>
        /// <param name="status">1 = control is OK, 0 = control is KO</param>
        /// <returns></returns>
        public DllFunctionCallResult ReadFlowrateControlStatus(ref byte status)
        {
            byte statusTemp = 0;
            var result = ErrorCheck(flowez_get_flowcontrol_status(Handle, (byte) Module, ref statusTemp));
            status = statusTemp;
            return result;
        }

        /// <summary>
        /// Pressure settling modes
        /// </summary>
        public enum PRESSURE_MODE
        {
            FAST,
            SMOOTH
        }

        /// <summary>
        /// Flow unit calibration tables
        /// </summary>
        public enum FLOW_UNIT_CALIBRATION_TABLE
        {
            H20,
            IPA,
            HFE,
            FC40,
            OIL
        }

        /// <summary>
        /// Checks wether the connected Flow unit support dual calibration table
        /// </summary>
        /// <param name="qArtCode">Flow unit's article code</param>
        /// <returns>Bool</returns>
        internal bool IsDual(string qArtCode)
        {
            return qArtCode.Contains("_IPA_") && GetTypeFromArtCode(qArtCode) != FlowUnitType.Unknown;
        }

        /// <summary>
        /// Retrieves the connected Flow unit type
        /// </summary>
        /// <param name="qArtCode">Flow unit's article code</param>
        /// <returns>Flow unit type</returns>
        internal FlowUnitType GetTypeFromArtCode(string qArtCode)
        {
            if (qArtCode.Contains("_XS_")) return FlowUnitType.XS;
            if (qArtCode.Contains("_S_")) return FlowUnitType.S;
            if (qArtCode.Contains("_M_")) return FlowUnitType.M;
            if (qArtCode.Contains("_L_")) return FlowUnitType.L;
            if (qArtCode.Contains("_XL_")) return FlowUnitType.XL;
            return FlowUnitType.Unknown;
        }

        /// <summary>
        /// Fluigent's Flow unit types
        /// </summary>
        internal enum FlowUnitType
        {
            Unknown = 0,
            XS = 1,
            S = 2,
            M = 4,
            L = 8,
            XL = 16
        }

        /// <summary>
        /// Extended Flow unit type (contains dual calibration information)
        /// </summary>
        public enum FlowUnitTypeEx
        {
            None,
            XSsingle,
            Ssingle,
            Sdouble,
            Msingle,
            Mdouble,
            Lsingle,
            Ldouble,
            XLsingle
    }
    }
}