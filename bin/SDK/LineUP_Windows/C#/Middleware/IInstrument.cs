﻿namespace Middleware
{
    public interface IInstrument
    {
        /// <summary>
        /// Instrument's handle
        /// </summary>
        ulong Handle { get; }

        /// <summary>
        /// Instrument's firmaware version number
        /// </summary>
        int VersionNumber { get; }

        /// <summary>
        /// Instrument's serial number
        /// </summary>
        int SerialNumber { get; }
    }
}