﻿using System;
using static LowLevel.LineUpDll;

namespace Middleware
{
    public class Link: ILink
    {
        private TTL_MODE _ttl1Mode = TTL_MODE.INPUT_RISING_EDGE;
        private TTL_MODE _ttl2Mode = TTL_MODE.INPUT_RISING_EDGE;

        public Link(int serialNumber)
        {
            Handle = lineup_initialization((ushort) SerialNumber);

            ushort firmware = 0;
            ushort serial = 0;
            var test = ErrorCheck(lineup_get_serial(Handle, ref firmware, ref serial));
            if (test.Success)
            {
                VersionNumber = firmware;
                SerialNumber = serial;
            } 

            byte status = 0;
            byte modulesNumber = 0;
            test = ErrorCheck(lineup_get_module_number(Handle, ref status, ref modulesNumber));
            if(test.Success) ModulesNumber = modulesNumber;
        }

        /// <inheritdoc />
        public ulong Handle { get; }

        /// <inheritdoc />
        public int VersionNumber { get; }

        /// <inheritdoc />
        public int SerialNumber { get; }

        /// <inheritdoc />
        public int ModulesNumber { get; }

        /// <inheritdoc />
        public DllFunctionCallResult Power(POWER_STATE state)
        {
            return ErrorCheck(lineup_power(Handle, (byte)state));
        }

        /// <summary>
        /// Sets the type of TTL mode (I/O and edge type) of the specified TTL port
        /// </summary>
        /// <param name="port">TTL port to set mode on</param>
        /// <param name="mode">New TTL mode</param>
        public DllFunctionCallResult SetTTLMode(TTL_PORT port, TTL_MODE mode)
        {
            switch (port)
            {
                case TTL_PORT.TTL1:
                    _ttl1Mode = mode;
                    break;
                case TTL_PORT.TTL2:
                    _ttl2Mode = mode;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(port), port, null);
            }
            return ErrorCheck(lineup_set_ttl_mode(Handle, (byte) _ttl1Mode, (byte) _ttl2Mode));
        }

        /// <summary>
        /// Retrieves TTL event flags
        /// </summary>
        /// <param name="Ttl1"></param>
        /// <param name="Ttl2"></param>
        public DllFunctionCallResult ReadTTL(ref bool Ttl1, ref bool Ttl2)
        {
            byte ttl1Temp = 0, ttl2Temp = 0;
            var result = ErrorCheck(lineup_get_ttl_state(Handle, 1, ref ttl1Temp, ref ttl2Temp));
            Ttl1 = ttl1Temp != 0 ? true : false;
            Ttl2 = ttl2Temp != 0 ? true : false;
            return result;
        }

        /// <summary>
        /// Generates a TTL event on the specified TTL port
        /// </summary>
        /// <param name="port">Port to generate the event on</param>
        public DllFunctionCallResult TriggerTTL(TTL_PORT port)
        {
            return ErrorCheck(lineup_trig_ttl(Handle, (byte) port));
        }

        /// <summary>
        /// TLL edges the instrument can detect
        /// </summary>
        public enum TTL_MODE
        {
            INPUT_RISING_EDGE,
            INPUT_FALLING_EDGE,
            OUTPUT_PULSE_LOW,
            OUTPUT_PULSE_HIGH
        }

        /// <summary>
        /// Instrument's TTL ports
        /// </summary>
        public enum TTL_PORT
        {
            TTL1,
            TTL2
        }
    }


}