﻿using LowLevel;

namespace Middleware
{
    public interface ILink: IInstrument
    {
        /// <summary>
        /// Number of connected modules
        /// </summary>
        int ModulesNumber { get; }

        /// <summary>
        /// Powers the entire LineUP chain
        /// </summary>
        /// <param name="state">Power state</param>
        LineUpDll.DllFunctionCallResult Power(POWER_STATE state);
    }

    public enum POWER_STATE
    {
        OFF,
        ON,
    }
}