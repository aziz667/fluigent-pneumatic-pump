﻿using System.Collections.Generic;
using System.Linq;
using static LowLevel.LineUpDll;

namespace Middleware
{
    /// <summary>
    /// This class' methods allow initialization of ClassicalLineUPSession objects.
    /// </summary>
    public static class LineUpClassicalSessionFactory
    {
        /// <summary>
        /// Opens a communication session with the specified Link controler
        /// </summary>
        /// <param name="linkSerial">The Link serial number to open communication with</param>
        /// <returns>An initialized session</returns>
        public static ClassicalLineUpSession Create(int linkSerial)
        {
            return new ClassicalLineUpSession(linkSerial);
        }

        /// <summary>
        /// Opens communication sessions with several Link controlers
        /// </summary>
        /// <param name="linkSerialNumbers">List of Links' serial numbers</param>
        /// <returns>List of initialized sessions</returns>
        public static List<ClassicalLineUpSession> CreateMultiple(IEnumerable<int> linkSerialNumbers)
        {
            return linkSerialNumbers.Select(linkSerialNumber => new ClassicalLineUpSession(linkSerialNumber)).ToList();
        }

        /// <summary>
        /// Opens communication sessions with all connected Link controlers
        /// </summary>
        /// <returns>List of initialized sessions</returns>
        public static List<ClassicalLineUpSession> CreateAll()
        {
            var table = new ushort[256];
            lineup_detect(table);
            var linkSerialNumbers = table.ToList().Where(e => e!= 0).Select(e => (int)e).OrderBy(e => e).ToList();

            return CreateMultiple(linkSerialNumbers);
        }
    }
}