﻿using System.Collections.Generic;
using LowLevel;
using Middleware.Controlee;
using static LowLevel.LineUpDll;

namespace Middleware
{
    /// <summary>
    /// This class' methods support all available actions on Link and Flow EZ LineUP hardwares
    /// </summary>
    public class ClassicalLineUpSession
    {
        private Link _link;
        private List<FlowEZ> _modules = new List<FlowEZ>();

        public ClassicalLineUpSession(int linkSerial)
        {
            _link = new Link(linkSerial);

            for (var index = 0; index < _link.ModulesNumber; index++)
            {
                byte type = 0;
                ushort softVers = 0;
                ushort flowEzSerial = 0;

                var test = ErrorCheck(lineup_get_module_info(_link.Handle, (byte) index, ref type, ref softVers, ref flowEzSerial));
                _modules.Add(new FlowEZ(_link.Handle, flowEzSerial, index));
            }
        }

        /// <summary>
        /// Retrieves the Link's serial number
        /// </summary>
        /// <returns>Serial number</returns>
        public int GetLinkSerial()
        {
            return _link.SerialNumber;
        }

        /// <summary>
        /// Retrieve's the Link's firmware version number
        /// </summary>
        /// <returns></returns>
        public int GetLinkVersion()
        {
            return _link.VersionNumber;
        }

        /// <summary>
        /// Retrieve's the specified Flow EZ's firmware version number
        /// </summary>
        /// <returns></returns>
        public int GetFlowEZVersion(int module)
        {
            return _modules[module].VersionNumber;
        }

        /// <summary>
        /// Retrieve's the specified Flow EZ's status word
        /// * MSK_RUN      0x01
        /// * MSK_LOCAL    0x02
        /// * MSK_REGUL    0x04
        /// * MSK_STABLE   0x08
        /// * MSK_UNDERP   0x10
        /// * MSK_OVERP    0x20
        /// * MSK_QDETECT  0x80
        /// </summary>
        /// <param name="module"></param>
        /// <returns></returns>
        public byte GetModuleStatus(int module)
        {
            return _modules[module].Status;
        }

        /// <summary>
        /// Retrieves the serial of the specified Flow EZ
        /// </summary>
        /// <param name="module">Index of the FlowEZ in the LineUP chain</param>
        /// <returns>Serial number</returns>
        public int GetFlowEzSerialNumber(int module)
        {
            return _modules[module].SerialNumber;
        }

        /// <summary>
        /// Retrieves the number of Flow EZs
        /// </summary>
        /// <returns>Number of Flow EZs</returns>
        public int GetModulesNumber()
        {
            return _link.ModulesNumber;
        }

        /// <summary>
        /// Powers or shuts down the hardware chain (Link controler + all Flow EZ modules)
        /// </summary>
        /// <param name="state">New power on/off state</param>
        /// <returns>Information about call success</returns>
        public void Power(POWER_STATE state)
        {
            _link.Power(state);
        }

        /// <summary>
        /// Sets the type of TTL mode (I/O and edge type) of the specified Link's port
        /// </summary>
        /// <param name="port">Port to set mode on</param>
        /// <param name="mode">New mode</param>
        /// <returns></returns>
        public void SetTTLMode(Link.TTL_PORT port, Link.TTL_MODE mode)
        {
            _link.SetTTLMode(port, mode);
        }

        /// <summary>
        /// Retrieves TTL event flags for both TTL ports
        /// </summary>
        /// <param name="TTL1Flag">The event flag for TTL1 port: True if a TTL signal matching the TTL port's configuration has occurred, False otherwise</param>
        /// <param name="TTL2Flag">The event flag for TTL2 port: True if a TTL signal matching the TTL port's configuration has occurred, False otherwise</param>
        /// <returns></returns>
        public void ReadTTL(ref bool TTL1Flag, ref bool TTL2Flag)
        {
            var ttl1Temp = false;
            var ttl2Temp = false;
            _link.ReadTTL(ref ttl1Temp, ref ttl2Temp);
            TTL1Flag = ttl1Temp;
            TTL2Flag = ttl2Temp;
        }

        /// <summary>
        /// Opens the output valve, and saves current atmospheric pressure as zero sensor value for the specified Flow EZ module. 
        /// This operation takes 5 seconds; no order should be sent to the Flow EZ module in the meanwhile.
        /// </summary>
        /// <param name="module"></param>
        public void CalibratePressure(int module)
        {
            _modules[module].CalibratePressure();
        }

        /// <summary>
        /// Generates a TTL event on the specified TTL port of the specified Link
        /// </summary>
        /// <param name="port">TTL port to generate event on</param>
        public void TriggerTTL(Link.TTL_PORT port)
        {
            _link.TriggerTTL(port);
        }

        /// <summary>
        /// Sets the current working pressure unit of the specified Flow EZ
        /// All pressure orders, measurements or settings for this Flow EZ are expressed in this unit
        /// </summary>
        /// <param name="module">Index of the FlowEZ in the LineUP chain</param>
        /// <param name="unit">New pressure unit</param>
        public void SetPressureUnit(int module, UnitConverter.PRESSURE_UNIT unit)
        {
            _modules[module].PressureUnit = unit;
        }

        /// <summary>
        /// Retrieves the current working pressure unit of the specified Flow EZ
        /// All pressure orders, measurements or settings for this Flow EZ are expressed in this unit
        /// </summary>
        /// <param name="module">Index of the FlowEZ in the LineUP chain</param>
        /// <returns>The Flow EZ's current pressure unit</returns>
        public UnitConverter.PRESSURE_UNIT GetPressureUnit(int module)
        {
            return _modules[module].PressureUnit;
        }

        /// <summary>
        /// Sets the current working pressure unit for all Flow EZs
        /// All pressure orders, measurements or settings for this Flow EZ are expressed in this unit
        /// </summary>
        /// <param name="unit"></param>
        public void SetSessionPressureUnit(UnitConverter.PRESSURE_UNIT unit)
        {
            foreach (var module in _modules)
            {
                module.PressureUnit = unit;
            }
        }

        /// <summary>
        /// Sets the current working flowrate unit of the specified Flow EZ
        /// All flowrate orders, measurements or settings for this Flow EZ are expressed in this unit)
        /// </summary>
        /// <param name="module">Index of the FlowEZ in the LineUP chain</param>
        /// <param name="unit">New flow unit</param>
        public void SetFlowUnit(int module, UnitConverter.FLOW_UNIT unit)
        {
            _modules[module].FlowUnit = unit;
        }

        /// <summary>
        /// Retrieves the current working flowrate unit of the specified Flow EZ
        /// All flowrate orders, measurements or settings for this Flow EZ are expressed in this unit
        /// </summary>
        /// <param name="module">Index of the FlowEZ in the LineUP chain</param>
        /// <returns>The Flow EZ's current flow unit</returns>
        public UnitConverter.FLOW_UNIT GetFlowUnit(int module)
        {
            return _modules[module].FlowUnit;
        }

        /// <summary>
        /// Retrieves the minimum pressure of the specified Flow EZ
        /// </summary>
        /// <param name="module">Index of the FlowEZ in the LineUP chain</param>
        /// <returns>The Flow EZ minimum pressure</returns>
        public float GetPressureRangeMin(int module)
        {
            return _modules[module].PressureRangeMin;
        }

        /// <summary>
        /// Retrieves the maximum pressure of the specified Flow EZ
        /// </summary>
        /// <param name="module">Index of the FlowEZ in the LineUP chain</param>
        /// <returns>The Flow EZ maximum pressure</returns>
        public float GetPressureRangeMax(int module)
        {
            return _modules[module].PressureRangeMax;
        }

        /// <summary>
        /// Retrieves the minimum flowrate of the specified Flow EZ
        /// </summary>
        /// <param name="module">Index of the FlowEZ in the LineUP chain</param>
        /// <returns>The Flow EZ minimum flowrate</returns>
        public float GetFlowrateRangeMin(int module)
        {
            return _modules[module].FlowrateMin;
        }

        /// <summary>
        /// Retrieves the maximum flowrate of the specified Flow EZ
        /// </summary>
        /// <param name="module">Index of the FlowEZ in the LineUP chain</param>
        /// <returns>The Flow EZ maximum flowrate</returns>
        public float GetFlowrateRangeMax(int module)
        {
            return _modules[module].FlowrateMax;
        }

        /// <summary>
        /// Sets the flowrate calibration table for the specified Flow EZ
        /// </summary>
        /// <param name="module">Index of the FlowEZ in the LineUP chain</param>
        /// <param name="table">The new calibration table</param>
        public void SetCalibrationTable(int module, FlowEZ.FLOW_UNIT_CALIBRATION_TABLE table)
        {
            _modules[module].FlowCalibrationTable = table;
        }

        /// <summary>
        /// Retrieves the flowrate calibration table of the specified Flow EZ
        /// </summary>
        /// <param name="module">Index of the FlowEZ in the LineUP chain</param>
        /// <returns>The Flow EZ flowrate calibration table</returns>
        public FlowEZ.FLOW_UNIT_CALIBRATION_TABLE GetCalibrationTable(int module)
        {
            return _modules[module].FlowCalibrationTable;
        }

        /// <summary>
        /// Sets the pressure settling mode for the specified Flow EZ
        /// </summary>
        /// <param name="module">Index of the FlowEZ in the LineUP chain</param>
        /// <param name="mode">New pressure settling mode</param>
        public void SetPressureMode(int module, FlowEZ.PRESSURE_MODE mode)
        {
            _modules[module].SetPressureMode(mode);
        }

        /// <summary>
        /// Sets the pressure order for the specified Flow EZ
        /// </summary>
        /// <param name="module">Index of the FlowEZ in the LineUP chain</param>
        /// <param name="order">New pressure order</param>
        public void SetPressure(int module, float order)
        {
            _modules[module].SetPressure(order);
        }

        /// <summary>
        /// Retrieves the current pressure measurement of the specified Flow EZ
        /// </summary>
        /// <param name="module">Index of the FlowEZ in the LineUP chain</param>
        /// <returns>The currently applied pressure</returns>
        public float ReadPressure(int module)
        {
            float value = 0;
            _modules[module].ReadPressure(ref value);
            return value;
        }

        /// <summary>
        /// Retrieves the specified Flow EZ's connected Flow unit type
        /// </summary>
        /// <param name="module"></param>
        public FlowEZ.FlowUnitTypeEx GetFlowUnitType(int module)
        {
            return _modules[module].ConnectedFlowUnitType;
        }

        /// <summary>
        /// Sets the flowrate order for the specified Flow EZ
        /// </summary>
        /// <param name="module">Index of the FlowEZ in the LineUP chain</param>
        /// <param name="order">New flowrate order</param>
        public void SetFlowrate(int module, float order)
        {
            _modules[module].SetFlowrate(order);
        }

        /// <summary>
        /// Retrieves the current flowrate measurement of the specified Flow EZ
        /// </summary>
        /// <param name="module">Index of the FlowEZ in the LineUP chain</param>
        /// <returns>The currently applied flowrate</returns>
        public float ReadFlowrate(int module)
        {
            var value = 0f;
            _modules[module].ReadFlowrate(ref value);
            return value;
        }

        /// <summary>
        /// Retrieves the current flowrate control status flag for the specified Flow EZ
        /// </summary>
        /// <param name="module">Index of the FlowEZ in the LineUP chain</param>
        /// <returns>0 if the Flow EZ does not achieve to control the flowrate, 1 otherwise.</returns>
        public byte ReadFlowrateControlStatus(int module)
        {
            byte value = 0;
            _modules[module].ReadFlowrateControlStatus(ref value);
            return value;
        }

        /// <summary>
        /// Closes a previously initialized LineUP session
        /// </summary>
        public void Close()
        {
            var handle = _link.Handle;
            _link = null;
            _modules = new List<FlowEZ>(0);
            ErrorCheck(lineup_close(handle));
        }
    }
}