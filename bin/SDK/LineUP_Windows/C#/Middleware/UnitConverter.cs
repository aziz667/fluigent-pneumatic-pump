﻿using System;
using PhysicalMeasure;
using static PhysicalMeasure.SI;

namespace Middleware
{
    public static class UnitConverter
    {
        private static readonly ConvertibleUnit Bar = new ConvertibleUnit("bar", "bar", Pa, new ScaledValueConversion(1.0/100000));
        private static readonly Unit kPa = Pa * Prefix.k;
        private static readonly Unit mBar = Bar * Prefix.m;
        private static readonly ConvertibleUnit PSI = new ConvertibleUnit("PSI", "PSI", Bar, new ScaledValueConversion(1.0/0.069));

        public static float ConvertPressure(PRESSURE_UNIT initial, PRESSURE_UNIT final, float value)
        {
            Quantity initialQuantity;
            Quantity finalQuantity;
            switch (initial)
            {
                case PRESSURE_UNIT.mbar:
                    initialQuantity = value * mBar;
                    break;
                case PRESSURE_UNIT.kPa:
                    initialQuantity = value * kPa;
                    break;
                case PRESSURE_UNIT.PSI:
                    initialQuantity = value * PSI;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(initial), initial, null);
            }

            switch (final)
            {
                case PRESSURE_UNIT.kPa:
                    finalQuantity = initialQuantity.ConvertTo(kPa);
                    break;
                case PRESSURE_UNIT.PSI:
                    finalQuantity = initialQuantity.ConvertTo(PSI);
                    break;
                case PRESSURE_UNIT.mbar:
                    finalQuantity = initialQuantity.ConvertTo(mBar);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(final), final, null);
            }

            return (float) finalQuantity.Value;
        }

        /// <summary>
        /// Pressure units the instrument can work with
        /// </summary>
        public enum PRESSURE_UNIT
        {
            mbar,
            kPa,
            PSI,
        }

        private static readonly ConvertibleUnit min = new ConvertibleUnit("min", "min", s, new ScaledValueConversion(1.0/60));
        private static readonly Unit nlperMin = Prefix.n * l / min;
        private static readonly Unit ulperSec = Prefix.my * l / s;
        private static readonly Unit ulPerMin = Prefix.my * l / min;
        private static readonly Unit ulPerHour = Prefix.my * l / h;
        private static readonly Unit mlPerMin = Prefix.m * l / min;
        private static readonly Unit mlPerHour = Prefix.m * l / h;

        public static float ConvertFlow(FLOW_UNIT initial, FLOW_UNIT final, float value)
        {
            Quantity initialQuantity;
            Quantity finalQuantity;
            switch (initial)
            {
                case FLOW_UNIT.nlperMin:
                    initialQuantity = value * nlperMin;
                    break;
                case FLOW_UNIT.ulperSec:
                    initialQuantity = value * ulperSec;
                    break;
                case FLOW_UNIT.ulperMin:
                    initialQuantity = value * ulPerMin;
                    break;
                case FLOW_UNIT.ulperHour:
                    initialQuantity = value * ulPerHour;
                    break;
                case FLOW_UNIT.mlperMin:
                    initialQuantity = value * mlPerMin;
                    break;
                case FLOW_UNIT.mlperHour:
                    initialQuantity = value * mlPerHour;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(final), final, null);
            }

            switch (final)
            {
                case FLOW_UNIT.nlperMin:
                    finalQuantity = initialQuantity.ConvertTo(nlperMin);
                    break;
                case FLOW_UNIT.ulperSec:
                    finalQuantity = initialQuantity.ConvertTo(ulperSec);
                    break;
                case FLOW_UNIT.ulperMin:
                    finalQuantity = initialQuantity.ConvertTo(ulPerMin);
                    break;
                case FLOW_UNIT.ulperHour:
                    finalQuantity = initialQuantity.ConvertTo(ulPerHour);
                    break;
                case FLOW_UNIT.mlperMin:
                    finalQuantity = initialQuantity.ConvertTo(mlPerMin);
                    break;
                case FLOW_UNIT.mlperHour:
                    finalQuantity = initialQuantity.ConvertTo(mlPerHour);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(final), final, null);
            }

            return (float)finalQuantity.Value;
        }

        /// <summary>
        /// Flow units the instrument can work with
        /// </summary>
        public enum FLOW_UNIT
        {
            nlperMin,
            ulperSec,
            ulperMin,
            ulperHour,
            mlperMin,
            mlperHour
        }
    }
}