﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Middleware;
using static Middleware.LineUpClassicalSessionFactory;

/*
 * Required Hardware:
 * - 1 x Link controler
 * - 1 x Flow EZ module
 * This example shows how to set a pressure order on the first Flow EZ module of your LineUP chain
 */
namespace Basic_Set_Pressure
{
    class Program
    {
        static void Main(string[] args)
        {
            ClassicalLineUpSession session = null;
            try
            {
                // Initialize session on Link (by S/N)
                Console.Write("Enter your Link serial number: ");
                var serial = Convert.ToInt32(Console.ReadLine());
                session = LineUpClassicalSessionFactory.Create(serial);

                // Set pressure to 100 (mbar is the default unit) on Flow EZ #1
                Console.Write("Set pressure to 100 (mbar is the default unit) on Flow EZ #1");
                session.SetPressure(0, 100);

                // Wait 10 seconds
                Console.WriteLine("Waiting 10 seconds");
                Thread.Sleep(new TimeSpan(0, 0, 0, 10));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                var waitForUser = Console.ReadLine();
            }
            finally
            {
                // Close session
                session?.Close();
            }
            
        }
    }
}
