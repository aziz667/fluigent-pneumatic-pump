﻿using System;
using System.Threading;
using Middleware;

/* Required Hardware:
 * - 1 x Link controler
 * - 1 x Flow EZ module
 * This example shows how to set a flowrate order on the first Flow EZ module of your LineUP chain
 */
namespace Basic_Set_Flowrate
{
    class Program
    {
        static void Main(string[] args)
        {
            ClassicalLineUpSession session = null;
            try
            {
                // Initialize session on Link (by S/N)
                Console.Write("Enter your Link serial number: ");
                var serial = Convert.ToInt32(Console.ReadLine());
                session = LineUpClassicalSessionFactory.Create(serial);

                // Set flowrate to 1 (ul/min is the default unit) on Flow EZ #1
                session.SetFlowrate(0, 1);

                // Wait 10 seconds
                Console.WriteLine("Waiting 10 seconds");
                Thread.Sleep(new TimeSpan(0, 0, 0, 10));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                var waitForUser = Console.ReadLine();
            }
            finally
            {
                // Close session
                session?.Close();
            }
        }
    }
}
