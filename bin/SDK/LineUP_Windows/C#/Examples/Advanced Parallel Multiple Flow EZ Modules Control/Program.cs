﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Middleware;

/*
 * Required Hardware:
 * - 2 x Link controlers
 * - 1 x Flow EZ module on each Link controler
 * This example shows how to control in parallel pressure/flowrate orders on the two Flow EZ modules on two different LineUP chains 
 */

namespace Advanced_Parallel_Multiple_Flow_EZ_Modules_Control
{
    class Program
    {
        static void Main(string[] args)
        {
            var ts = new CancellationTokenSource();

            var sessions = new List<ClassicalLineUpSession>();
            try
            {
                // Initialize sessions on all connected Links
                sessions = LineUpClassicalSessionFactory.CreateAll();

                if(sessions.Count < 2) throw new Exception("This example requires 2 x LineUP chains");

                // Link #1 Flow EZ #1 is driven in pressure, every 2 seconds randomly
                var session1 = sessions[0];
                var task1 = Task.Factory.StartNew(() =>
                {
                    var rand = new Random(DateTime.Now.Millisecond);
                    var pressureMax = (int) session1.GetPressureRangeMax(0);
                    while (!ts.IsCancellationRequested)
                    {
                        var pressure = rand.Next(pressureMax);
                        
                        session1.SetPressure(0, pressure);
                        Console.WriteLine($"{DateTime.Now}: New pressure order: {pressure} mbar");
                        Thread.Sleep(new TimeSpan(0, 0, 0, 2));
                    }
                });

                // Link #2 Flow EZ #1 is driven in flow, every 5 seconds, randomly
                var session2 = sessions[1];
                var task2 = Task.Factory.StartNew(() =>
                {
                    var rand = new Random(DateTime.Now.Millisecond);
                    var flowMax = (int)session2.GetFlowrateRangeMax(0);
                    while (!ts.IsCancellationRequested)
                    {
                        var flowrate = rand.Next(flowMax);
                        session2.SetFlowrate(0, flowrate);
                        Console.WriteLine($"{DateTime.Now}: New flowrate order: {flowrate} ul/min");
                        Thread.Sleep(new TimeSpan(0, 0, 0, 5));
                    }
                });

                // Keep running for 15 seconds
                Thread.Sleep(new TimeSpan(0, 0, 0, 15));

                ts.Cancel();
                Console.Write("Application is closing...");

                Task.WaitAll(task1, task2);
                ts.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                var waitForUser = Console.ReadLine();
            }
            finally
            {
                // Close sessions
                foreach (var session in sessions) session.Close();
                sessions = new List<ClassicalLineUpSession>(0);
            }
        }
    }
}