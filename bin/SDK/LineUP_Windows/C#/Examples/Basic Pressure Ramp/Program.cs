﻿using System;
using System.Threading;
using Middleware;

/* Required Hardware:
 * - 1 x Link controler
 * - 1 x Flow EZ module
 * This example shows how to generate a pressure ramp(over the whole pressure range) on the first Flow EZ module of your LineUP chain
 */
namespace Basic_Pressure_Ramp
{
    class Program
    {
        static void Main(string[] args)
        {
            ClassicalLineUpSession session = null;
            try
            {
                // Initialize session on Link (by S/N)
                Console.Write("Enter your Link serial number: ");
                var serial = Convert.ToInt32(Console.ReadLine());
                session = LineUpClassicalSessionFactory.Create(serial);

                // Retrieve Flow EZ's maximum pressure
                var maxPressure = session.GetPressureRangeMax(0);

                // Set pressure to 100 (mbar is the default unit) on Flow EZ #1
                for (var pressure = 0f; pressure <= maxPressure; pressure += maxPressure / 10)
                {
                    session.SetPressure(0, pressure);
                    Console.WriteLine($"{DateTime.Now}: New pressure order: {pressure} mbar");
                    Thread.Sleep(new TimeSpan(0, 0, 0, 1));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                var waitForUser = Console.ReadLine();
            }
            finally
            {
                // Close session
                session?.Close();
            }
        }
    }
}
