﻿using System;
using System.Collections.Generic;
using System.Threading;
using Middleware;

/*
 * Required Hardware:
 * - 2 x Link controlers
 * - 1 x Flow EZ module on each Link controler
 * This example shows how to set concurrent pressure or flowrate orders on the two Flow EZ modules on two different LineUP chains
 */
namespace Basic_Multiple_Flow_EZ_Modules_Control
{
    class Program
    {
        static void Main(string[] args)
        {
            var sessions = new List<ClassicalLineUpSession>();
            try
            {
                // Initialize sessions on all connected Links
                sessions = LineUpClassicalSessionFactory.CreateAll();

                if (sessions.Count < 2) throw new Exception("This example requires 2 x LineUP chains");

                // Retrieve and display both Flow EZs' serial numbers
                var serial1 = sessions[0].GetFlowEzSerialNumber(0);
                Console.WriteLine($"Link #1 Flow EZ #1, serial number: {serial1}");
                var serial2 = sessions[1].GetFlowEzSerialNumber(0);
                Console.WriteLine($"Link #2 Flow EZ #1, serial number: {serial2}");

                // Set pressure to 100 (mbar is the default unit) on Link #1 Flow EZ #1
                Console.WriteLine("Link #1 Flow EZ #1: Set pressure to 100 mbar");
                sessions[0].SetPressure(0, 100);

                // Set flowrate to 1 (ul/min is the default unit) on Link #2 Flow EZ #1
                Console.WriteLine("Link #2 Flow EZ #1: Set flowrate to 1 ul/min");
                sessions[1].SetFlowrate(0, 1);

                // Wait 10 seconds
                Thread.Sleep(new TimeSpan(0, 0, 0, 10));

                // Set pressure to 50 (mbar is the default unit) on Link #12 Flow EZ #1
                Console.WriteLine("Link #2 Flow EZ #1: Set pressure to 50 mbar");
                sessions[1].SetPressure(0, 50); Thread.Sleep(new TimeSpan(0, 0, 0, 1));

                // Wait 10 seconds
                Thread.Sleep(new TimeSpan(0, 0, 0, 10));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                var waitForUser = Console.ReadLine();
            }
            finally
            {
                // Close session
                foreach (var session in sessions) session.Close();
            }
        }
    }
}
