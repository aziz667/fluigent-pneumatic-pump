﻿===========================================================================
							C# .NET LineUP SDK
===========================================================================

This file contains a summary of LineUP SDK C# .NET middleware and basic examples.

-------------------------- SOFTWARE ---------------------------------------
The software was implemented and tested on Windows x64 platform using Visual Studio 2017 15.5.2 and .NET Framework 4.6.1

------------------------ INSTALLATION -------------------------------------
The application is based on a dynamically linked libraries "LineUP_c_32.dll" and "LineUP_c_64.dll".

Copy/paste all files in "C#" directory to your development computer
Open "Examples/LineUP Csharp Example" solution file
Right click the solution in "Solution explorer" and select "Rebuild solution" in order to update packages

--------------------------- FILES ----------------------------------------
List of project files:

LineUP Csharp Example.sln
	Project solution containing all required dependencies and examples

LowLevel folder
	Low level interface to LineUP dll

Middleware folder
	User interface used to access LineUP functionalities
	
Basic Set Pressure.csproj
	Simple example to set a pressure
	
Basic Set Flowrate.csproj
	Simple example to set a flowrate
	
Basic Pressure Ramp.csproj
	Simple example to set a pressure ramp over time
	
Basic Multiple Flow EZ Modules Control.csproj
	Simple example to set pressures on different multiple Link modules

Advanced Parallel Multiple Flow EZ Modules Control.csproj
	Advanced example using threads in order to apply pressure asynchronously

LineUP_c_64.dll
	This is the dynamic linked library used by 64bit systems in order to control the device

LineUP_c_32.dll
	This is the dynamic linked library used by 32bit systems in order to control the device
