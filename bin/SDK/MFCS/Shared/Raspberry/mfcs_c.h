//MFCS_C.H
//---------------------------------------------------------------------------
#ifndef mfcs_cH
#define mfcs_cH

#ifdef __cplusplus
extern "C"
{
#endif

/* Name: 		mfcs_initialization
 *
 * Description:	initialize USB connection and launches a continuous check for a MFCS with the specified serial number
 * Arguments:	serial is the serial number of the MFCS for which  you want to open the session.  
 * Returns:		handle is the identifier of the opened session
 */
unsigned long long  mfcs_initialization(unsigned short serial);

/* Name: 		mfcsez_initialisation
 *
 * Description:	initialize USB connection and launches a continuous check for a MFCS-EZ with the specified serial number
 * Arguments:	serial is the serial number of the MFCS-EZ for which  you want to open the session.  
 * Returns:		handle is the identifier of the opened session
 */
unsigned long long  mfcsez_initialization(unsigned short serial);

/* Name: 		mfcs_close
 *
 * Description:	terminates threads and deallocates memory used by the MFCS or MFCS-EZ session
 * Arguments:	handle is the identifier of the MFCS or MFCS-EZ session 
 * Returns:		C error is the error code
 */	
unsigned char  mfcs_close(unsigned long long handle);

/* Name: 		mfcs_activate
 *
 * Description:	Remotely arm MFCS device (same as pressing green button). This feature is only available on latest revision.
 * Arguments:	handle is the identifier of the MFCS or MFCS-EZ session 
 * Returns:		C error is the error code
 */	
unsigned char  mfcs_activate(unsigned long long handle);

/* Name: 		mfcs_reset
 *
 * Description:	Remotely disarm MFCS device (same as pressing red button). This feature is only available on latest revision.
 * Arguments:	handle is the identifier of the MFCS or MFCS-EZ session 
 * Returns:		C error is the error code
 */	
unsigned char  mfcs_reset(unsigned long long handle);

/* Name: 		mfcs_read
 *
 * Description:	Read raw data from the MFCS device
 * Arguments:	handle is the identifier of the MFCS or MFCS-EZ session 
                S is an array of bytes that will receive the data sent by the device
 * Returns:		C error is the error code
 */	
unsigned char  mfcs_read(unsigned long long handle,unsigned char * S);

/* Name: 		mfcs_read
 *
 * Description:	Send a data frame to the MFCS device. The frame must be formatted according to the documentation
 * Arguments:	handle is the identifier of the MFCS or MFCS-EZ session 
                S is an array of bytes containing the data to be sent to the device
 * Returns:		C error is the error code
 */	
unsigned char  mfcs_write(unsigned long long handle,unsigned char * S);

/* Name: 		mfcs_set_purge_on
 *
 * Description:	opens the purge on channel 1, if available on the instrument
 * Arguments:	handle is the identifier of the MFCS session 
 * Returns:		C error is the error code
 */
unsigned char  mfcs_set_purge_on(unsigned long long handle);

/* Name: 		mfcs_set_purge_off
 *
 * Description:	closes the purge on channel 1, if available on the instrument
 * Arguments:	handle is the identifier of the MFCS session 
 * Returns:		C error is the error code
 */	
unsigned char  mfcs_set_purge_off(unsigned long long handle);

/* Name: 		mfcs_get_purge
 *
 * Description:	gets the purge status - ONLY FOR MFCS
 * Arguments:	handle is the identifier of the MFCS session 
 * Returns:		C error is the error code
 *      		c it the status of the purge
 */	
unsigned char  mfcs_get_purge(unsigned long long handle,unsigned char * c);

/* Name: 		mfcs_get_status
 *
 * Description:	gets the MFCS or MFCS-EZ device status
 * Arguments:	handle is the identifier of the MFCS or MFCS-EZ session 
 * Returns:		C error is the error code
 *      		c it the status of the MFCS or MFCS-EZ
 */
unsigned char  mfcs_get_status(unsigned long long handle,unsigned char * c);

/* Name: 		mfcs_read_pressure
 *
 * Description:	reads the pressure value of the specified channel
 * Arguments:	handle is the identifier of the MFCS or MFCS-EZ session 
 *              channel is the channel number
 * Returns:		C error is the error code
 *      		pressure is the measured pressure in mbar
 *      		chrono is the counter value of the 25 ms timer
 */
unsigned char  mfcs_read_pressure(unsigned long long handle,unsigned char channel,
	float * pressure,unsigned short * chrono);
	
/* Name: 		mfcs_data_chan
 *
 * Description:	reads the pressure sensor specification
 * Arguments:	handle is the identifier of the MFCS or MFCS-EZ session 
 *              channel is the channel number
 * Returns:		C error is the error code
 *      		unite is pressure sensor unit
 *      		max is the maximum pressure range
 *      		zero is the sensor zero value
 *      		mesure is the measured pressure
 *      		chrono is the counter value of the 25 ms timer
 */
unsigned char  mfcs_data_chan(unsigned long long handle,unsigned char channel,
	unsigned char * unite,unsigned short * max,unsigned short * zero,
	unsigned short * mesure,unsigned short * chrono);
	
/* Name: 		mfcs_get_serial
 *
 * Description:	gets serial number of the MFCS or MFCS-EZ
 * Arguments:	handle is the identifier of the MFCS or MFCS-EZ session
 * Returns:		C error is the error code
 *              us is the MFCS or MFCS-EZ serial number associated to the MFCS or MFCS-EZ session specified by the connection handle
 */	
unsigned char  mfcs_get_serial(unsigned long long handle,unsigned short * serial);

/* Name: 		mfcs_set_pressure
 *
 * Description:	regulates the pressure of the specified channel on the MFCS or MFCS-EZ
 * Arguments:	handle is the identifier of the MFCS or MFCS-EZ session
 *              channel is the channel number
 *              pcons is the desired pressure value in mbar
 * Returns:		C error is the error code
 */
unsigned char  mfcs_set_pressure(unsigned long long handle,unsigned char channel,float pcons);

/* Name: 		mfcs_set_alpha
 *
 * Description:	sets the alpha value for the specified channel on the MFCS or MFCS-EZ
 * Arguments:	handle is the identifier of the MFCS or MFCS-EZ session
 *              channel is the channel number
 *              alpha is the value of the alpha coefficient
 * Returns:		C error is the error code
 */
unsigned char  mfcs_set_alpha(unsigned long long handle,unsigned char channel,unsigned char alpha);

/* Name: 		mfcs_set_manual
 *
 * Description:	sets the  solenoid valve voltage in % for the specified channel on the MFCS or MFCS-EZ
 * Arguments:	handle is the identifier of the MFCS or MFCS-EZ session
 *              channel is the channel number
 *              pcons is the value of the solenoid valve voltage in % of the full scale
 * Returns:		C error is the error code
 */
unsigned char  mfcs_set_manual(unsigned long long handle,unsigned char channel,float pcons);

/* Name: 		mfcs_set_zero
 *
 * Description:	saves the zero sensor value on the MFCS or MFCS-EZ
 * Arguments:	handle is the identifier of the MFCS or MFCS-EZ session
 *              channel is the channel number
 *              zero is the value of the sensor zero value
 * Returns:		C error is the error code
 */
unsigned char  mfcs_set_zero(unsigned long long handle,unsigned char channel,unsigned short zero);

/* Name: 		mfcs_detect
 *
 * Description:	detects the connected MFCS and return their serial numbers in the table
 * Arguments:	
 * Returns:		C error is the error code
 *              table is the array of all connected MFCS
 */	
unsigned char  mfcs_detect(unsigned short serialTable[256]);

/* Name: 		mfcsez_detect
 *
 * Description:	detects the connected MFCS-EZ and return their serial numbers in the table
 * Arguments:	
 * Returns:		C error is the error code
 *              table is the array of all connected MFCS-EZ
 */
unsigned char  mfcsez_detect(unsigned short serialTable[256]);
#ifdef __cplusplus
}
#endif

#endif






