/*============================================================================*/
/*                    Fluigent / Cpp MFCS library Example                     */
/*----------------------------------------------------------------------------*/
/*         Copyright (c) Fluigent 2018.  All Rights Reserved.                 */
/*----------------------------------------------------------------------------*/
/*                                                                            */
/* Title:   Cpp_MFCS_Series.cpp                                               */
/* Purpose: Interact with Fluigent MFCS device(s) using Windows API	     	  */
/*			This script will apply a pressure ramp on the device			  */	
/* Version: 18.0.0                                                            */
/* Library version: 1.4.1.0												      */	
/* Hardware setup: One MFCS device with at least one pressure channel    	  */
/*                                                                            */
/*============================================================================*/

#include <cstdlib>
#include <iostream>
#include <windows.h>
#include "mfcs_c.h"

/* Define functions prototype according to mfcs_c.h file */
typedef unsigned long long(__stdcall *Hmfcs_initialization)(unsigned short sn);
typedef unsigned long long(__stdcall *Hmfcsez_initialization)(unsigned short sn);
typedef unsigned char(__stdcall *Hmfcs_close)(unsigned long long handle);
typedef unsigned char(__stdcall *Hmfcs_activate)(unsigned long long handle);
typedef unsigned char(__stdcall *Hmfcs_reset)(unsigned long long handle);
typedef unsigned char(__stdcall *Hmfcs_set_purge_on)(unsigned long long handle);
typedef unsigned char(__stdcall *Hmfcs_set_purge_off)(unsigned long long handle);
typedef unsigned char(__stdcall *Hmfcs_get_purge)(unsigned long long handle, unsigned char * c);
typedef unsigned char(__stdcall *Hmfcs_get_status)(unsigned long long handle, unsigned char * c);
typedef unsigned char(__stdcall *Hmfcs_read_pressure)(unsigned long long handle, unsigned char chan, float * pressure, unsigned short * chrono);
typedef unsigned char(__stdcall *Hmfcs_data_chan)(unsigned long long  handle, unsigned char canal, unsigned char * unite, unsigned short * max, unsigned short * zero, unsigned short * mesure, unsigned short * chrono);
typedef unsigned char(__stdcall *Hmfcs_get_serial)(unsigned long long handle, unsigned short *serial);
typedef unsigned char(__stdcall *Hmfcs_set_pressure)(unsigned long long handle, unsigned char canal, float pcons);
typedef unsigned char(__stdcall *Hmfcs_set_alpha)(unsigned long long handle, unsigned char canal, unsigned char alpha);
typedef unsigned char(__stdcall *Hmfcs_set_manual)(unsigned long long handle, unsigned char canal, float pcons);
typedef unsigned char(__stdcall *Hmfcs_set_zero)(unsigned long long handle, unsigned char canal, unsigned short zero);
typedef unsigned char(__stdcall *Hmfcs_detect)(unsigned short table[256]);
typedef unsigned char(__stdcall *Hmfcsez_detect)(unsigned short table[256]);

//===============================================================
//		Main function
//===============================================================
int main(int argc, char *argv[])
{
	/* System settings variable definition */
	float start_pressure = 70;					// Pressure set (mbar) at the beginning
	float target_pressure = 80;					// Maximal pressure setpoint (mbar)
	unsigned char pressureChannel = 0;			// Selected channel to control, index starts at 0 for first MFCS channel
	HINSTANCE MFCSProcIDDLL = NULL;				// Define dll handler

	/* Define variables used for MFCS device */
	unsigned long long mfcsHandle = 0;			// Local instrument handle
	unsigned short Serial = 0;					// Instrument Serial Number
	float read_pressure = 0;					
	unsigned short chrono = 0;
	unsigned int loop_index = 0;

	/* Load DLL into memory */
#ifdef _WIN64
	MFCSProcIDDLL = LoadLibrary(TEXT("mfcs_c_64.dll"));
#else
	MFCSProcIDDLL = LoadLibrary(TEXT("mfcs_c_32.dll"));
#endif

	/* Declare pointers on dll functions */
	Hmfcs_initialization pmfcs_initialization;
	Hmfcsez_initialization pmfcsez_initialization;
	Hmfcs_close pmfcs_close;
	Hmfcs_activate pmfcs_activate;
	Hmfcs_reset pmfcs_reset;
	Hmfcs_set_purge_on pmfcs_set_purge_on;
	Hmfcs_set_purge_off pmfcs_set_purge_off;
	Hmfcs_get_purge pmfcs_get_purge;
	Hmfcs_get_status pmfcs_get_status;
	Hmfcs_read_pressure pmfcs_read_pressure;
	Hmfcs_data_chan pmfcs_data_chan;
	Hmfcs_get_serial pmfcs_get_serial;
	Hmfcs_set_pressure pmfcs_set_pressure;
	Hmfcs_set_alpha pmfcs_set_alpha;
	Hmfcs_set_manual pmfcs_set_manual;
	Hmfcs_set_zero pmfcs_set_zero;
	Hmfcs_detect pmfcs_detect;
	Hmfcsez_detect pmfcsez_detect;

	/* Link dll pointers with functions prototype */
	pmfcs_initialization = (Hmfcs_initialization)GetProcAddress(MFCSProcIDDLL, "mfcs_initialization");
	pmfcsez_initialization = (Hmfcsez_initialization)GetProcAddress(MFCSProcIDDLL, "mfcsez_initialization");
	pmfcs_close = (Hmfcs_close)GetProcAddress(MFCSProcIDDLL, "mfcs_close");
	pmfcs_activate = (Hmfcs_close)GetProcAddress(MFCSProcIDDLL, "mfcs_activate");
	pmfcs_reset = (Hmfcs_close)GetProcAddress(MFCSProcIDDLL, "mfcs_reset");
	pmfcs_set_purge_on = (Hmfcs_set_purge_on)GetProcAddress(MFCSProcIDDLL, "mfcs_set_purge_on");
	pmfcs_set_purge_off = (Hmfcs_set_purge_off)GetProcAddress(MFCSProcIDDLL, "mfcs_set_purge_off");
	pmfcs_get_purge = (Hmfcs_get_purge)GetProcAddress(MFCSProcIDDLL, "mfcs_get_purge");
	pmfcs_get_status = (Hmfcs_get_status)GetProcAddress(MFCSProcIDDLL, "mfcs_get_status");
	pmfcs_read_pressure = (Hmfcs_read_pressure)GetProcAddress(MFCSProcIDDLL, "mfcs_read_pressure");
	pmfcs_data_chan = (Hmfcs_data_chan)GetProcAddress(MFCSProcIDDLL, "mfcs_data_chan");
	pmfcs_get_serial = (Hmfcs_get_serial)GetProcAddress(MFCSProcIDDLL, "mfcs_get_serial");
	pmfcs_set_pressure = (Hmfcs_set_pressure)GetProcAddress(MFCSProcIDDLL, "mfcs_set_pressure");
	pmfcs_set_alpha = (Hmfcs_set_alpha)GetProcAddress(MFCSProcIDDLL, "mfcs_set_alpha");
	pmfcs_set_manual = (Hmfcs_set_manual)GetProcAddress(MFCSProcIDDLL, "mfcs_set_manual");
	pmfcs_set_zero = (Hmfcs_set_zero)GetProcAddress(MFCSProcIDDLL, "mfcs_set_zero");
	pmfcs_detect = (Hmfcs_detect)GetProcAddress(MFCSProcIDDLL, "mfcs_detect");
	pmfcsez_detect = (Hmfcsez_detect)GetProcAddress(MFCSProcIDDLL, "mfcsez_detect");


	if (MFCSProcIDDLL != NULL) {		// If dll loaded
		std::cout << "MFCS dll is loaded" << std::endl;
	
		/* Initialize device */
		/* Initialize the first MFCS in Windows enumeration list */
		mfcsHandle = pmfcs_initialization(0);
		std::cout << "MFCS initialized" << std::endl;

		/* Read device serial number */
		/*Get the serial number of the MFCS*/
		pmfcs_get_serial(mfcsHandle, &Serial);
		std::cout << "MFCS SN: " << Serial << std::endl;

		/* Set pressure regulation servitude coefficient */
		pmfcs_set_alpha(mfcsHandle, pressureChannel, 5);		// Default value (just after power on) is 0 
															    // Alpha value has to be grather than 0 in order to regulate pressure

		/* Change and read pressure every second until reaching 'target_pressure' value */
		for (loop_index = int(start_pressure); loop_index<target_pressure; loop_index++){
			pmfcs_set_pressure(mfcsHandle, pressureChannel, float(loop_index));		 // Set required output pressure value         
			Sleep(1000);   															 // Wait 1 s
			pmfcs_read_pressure(mfcsHandle, pressureChannel, &read_pressure, &chrono);   // Get the pressure value on the specified channel
			std::cout << "Set pressure at: " << loop_index << "mbar" << "; Read pressure: " << read_pressure
				<< "mbar" << std::endl;												 // Display pressure setpoint and channel pressure value
		}

		/* Close MFCS session */
		pmfcs_close(mfcsHandle);
		std::cout << "MFCS closed" << std::endl;
	}

	/* Release the DLL */
	FreeLibrary(MFCSProcIDDLL);
	std::cout << "MFCS dll unloaded" << std::endl;

	/* Exit application */
	system("PAUSE");

	return EXIT_SUCCESS;
}


