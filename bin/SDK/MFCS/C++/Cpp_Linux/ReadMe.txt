﻿===========================================================================
    CONSOLE APPLICATION : Cpp_MFCS_Series_Application
===========================================================================

This file contains a summary of the MFCS-EZ example application.

-------------------------- HARDWARE ---------------------------------------
One MFCS with at least one channel and capable of 50mbar pressure output

-------------------------- SOFTWARE ---------------------------------------
The software was implemented and tested on different Linux distributions including Debian, OpenSuse, Ubuntu and Scientific Linux

------------------------ INSTALLATION -------------------------------------
Super user rights are required in order to grant full access to “/usr/lib” before installation or use “cp libName.so /usr/lib” command.

The application is based on a dynamically linked shared object libraries contained in .tar archieve. 
In order to use the library functions, all files have to be copied in the default Linux library directory: “/usr/lib”.

USB device permissions have to be changed (in order to grant full access to Fluigent devices).
Copy or merge "99-hid.rules" file to /etc/udev/rules.d/ folder:
SUBSYSTEM=="hidraw", ATTRS{idVendor}=="04d8", ATTRS{idProduct}=="0000", MODE="0666"
SUBSYSTEM=="hidraw", ATTRS{idVendor}=="04d8", ATTRS{idProduct}=="0001", MODE="0666"
SUBSYSTEM=="hidraw", ATTRS{idVendor}=="04d8", ATTRS{idProduct}=="0002", MODE="0666"
SUBSYSTEM=="hidraw", ATTRS{idVendor}=="04d8", ATTRS{idProduct}=="0003", MODE="0666"

The "mfcs_c.h" file is the header for the library. MFCS and MFCS-EZ functions can be directly called using this file.
However the "libmfcs_lib.so" shared library has to be included in project makefile (“LIBS=-lmfcs_lib”) while building application.

One way to create an application is by using directly the gcc/g++ compiler:
1. "Cpp_MFCS_Series.cpp" file can be edit using a text editor.
2. Open console in folder location and type "g++ -o Cpp_MFCS_Series_Application Cpp_MFCS_Series.cpp -lmfcs_lib" 
3. Execute the console application "./Cpp_MFCS_Series_Application"

-------------------- APPLICATION BEHAVIOUR -------------------------------
The executable application performs the following steps:
1. Open a MFCS session for the first found device
2. Display device serial number
3. Display purge status
4. Set pressure to increase from start_pressure (20mbar) to target_pressure (50mbar) at 1mbar/s rate
5. Set pressure to 0mbar.
6. Close communication with the device

