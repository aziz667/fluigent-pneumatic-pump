/*============================================================================*/
/*                    Fluigent / Cpp MFCS library Example                     */
/*----------------------------------------------------------------------------*/
/*         Copyright (c) Fluigent 2018.  All Rights Reserved.                 */
/*----------------------------------------------------------------------------*/
/*                                                                            */
/* Title:   Cpp_MFCS_Series.cpp                                               */
/* Purpose: Interact with Fluigent MFCS device(s) using Linux API	          */
/* Version: 2.0.0                                                             */
/* Hardware setup: One MFCS-EZ device with at least one pressure channel      */
/*                                                                            */
/*============================================================================*/



#include "mfcs_c.h"
#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>         // Use of sleep() function
#include <signal.h>         // Used to capture keyboard interrupts

#define True 1
#define False 0

#define SUCCESS 1
#define FAIL 0

typedef unsigned long long ULL;
typedef unsigned short US;
typedef char S[];
typedef unsigned char C;
typedef float F;
typedef char B;


/*============================================================================*/
/* Function name:   exit_application()                                        */
/* Purpose:         releases resources (MFCS session) and exits application  */
/* Arguments:       BOOLEAN SUCCESS/FAIL: end state of the application        */
/* Returns:         N/A                                                       */
/*============================================================================*/
int exit_application(bool state, ULL Handle)
{
    B B_OK;
	std::cout << "Releasing USB resource." << std::endl;
    B_OK=mfcs_close(Handle);	    //Close MFCS session
    if (B_OK==0){
	printf("\nPress 'ENTER' to continue.");      
        getchar();   	   	    // Wait for user to press Enter
        return EXIT_SUCCESS;        //Returns global state of the application as success
    }                               //Note: if used by other application(s), global result status can also be used for control
    else{
        return EXIT_FAILURE;        //Returns global state of the application as failed
    }
}


/*============================================================================*/
/* 				Main function 				      */
/*============================================================================*/
int main(int argc, char *argv[])
{
     /* System settings variable definition */
     F start_pressure = 20;         //Pressure set (mbar) at the beginning
     F target_pressure = 50;      //Maximal pressure setpoint (mbar)
     C pressureChannel = 0;        //Selected channel (0 for 1st pressure channel)

     /* General variables definition */
     ULL mfcsHandle = 0;
     US sysSerial = 0;
     C C_error;
     B B_OK;
     F read_pressure;
     US chrono;
     C purge_status;
     int loop_index;
     unsigned short MFCS_detectTable[256] = { 0 };
     unsigned short MFCSEZ_detectTable[256] = { 0 };

     std::cout << "FLUIGENT: C++ MFCS Series example." << std::endl;		// Display to Terminal

    /* Optional: Detect connected MFCS and MFCS-EZ devices */
    // C_error = mfcs_detect(MFCS_detectTable);
    // C_error = mfcsez_detect(MFCSEZ_detectTable);

    /* Open the MFCS-EZ session for the first enumerated MFCS device */
    mfcsHandle = mfcs_initialization(0);						// Use "mfcsez_initialization" function for MFCS-EZ devices!
    std::cout << "MFCS session handle: " << mfcsHandle << std::endl;		// Display session handle

    /* Get serial number of the MFCS associated to the session*/
    C_error = mfcs_get_serial(mfcsHandle, &sysSerial);
    if (C_error==0){		 // Manage returned status of called library function
      std::cout << "Device serial number: " << sysSerial << std::endl;
    } else {
      std::cout << "Error getting device serial number (1: USB closed; 2: Wrong channel): " << int(C_error) << std::endl;
      return EXIT_FAILURE;        //Returns global state of the application as failed -> No MFCS detected!	
    }

    /* Get purge status */
    C_error=mfcs_get_purge(mfcsHandle,&purge_status); // Get the status of the purge
    if (C_error==0){		 
      std::cout << "Device purge status (0: OFF; 1: ON): " << int(purge_status) << std::endl;
    } else {
      std::cout << "Error getting purge status (1: USB closed; 2: Wrong channel): " << int(C_error) << std::endl;
    }
                               
    /* Change then read pressure every 1 second */
    for (loop_index=start_pressure; loop_index<target_pressure; loop_index++){        
           C_error = mfcs_set_pressure(mfcsHandle,pressureChannel,loop_index);              // Set required output pressure value
           sleep(1);   								      // Wait 1 s
           C_error = mfcs_read_pressure(mfcsHandle,pressureChannel,&read_pressure,&chrono); // Get the pressure value on the specified channel
           std::cout << "Set pressure at: " << loop_index << "mbar" << "; Read pressure: " << read_pressure 
		<< "mbar" << std::endl;						      // Display pressure setpoint and channel pressure value
    }
	
    /* Set pressure to 0mbar before quit */
    C_error = mfcs_set_pressure(mfcsHandle,pressureChannel,0);     

    /* Exit application */
    return exit_application(SUCCESS,mfcsHandle);
}
