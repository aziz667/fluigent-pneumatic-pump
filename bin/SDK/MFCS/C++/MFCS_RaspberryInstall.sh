#!/bin/sh
if [ "$(whoami)" != "root" ]; then
	su -c "$0" root
	exit
fi
echo "Extracting MFCS library..."
tar -xvzf ./Shared/Raspberry/pi_libmfcs_lib.so.tar.gz -C /usr/lib
ldconfig -l /usr/lib/libmfcs_lib.so.2.0.0
echo "Copy of 99-hid.rules..."
cp ./Shared/Raspberry/99-hid.rules /etc/udev/rules.d/
echo "Compilation of example application..."
g++ -I. -o Cpp_MFCS_Series_Application ./Cpp_Linux/Cpp_MFCS_Series.cpp -lmfcs_lib

