#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This example shows how to interact with an individual channel of an MFCS, 
MFCS-EZ or PX device using the properties of the MFCS.Channel class.
The MFCS class acts as a virtual representation of the instrument. It also 
manages the lifetime of the connection with the instrument, ensuring it gets
closed when the object is destroyed.
"""

# Print function for Python 2 compatibility
from __future__ import print_function 
# MFCS class
from Fluigent.MFCS import MFCS
import time

# Get the serial numbers of the available instruments
instrument_serial_numbers = MFCS.detect()

if not instrument_serial_numbers:
    raise Exception("No MFCS Series device detected")
    
print("Available devices: {}". format(instrument_serial_numbers))

# Initialize the first instrument in the list
mfcs = MFCS(instrument_serial_numbers[0])
# Get a reference to the first channel of the device
channel1 = mfcs[1]

# Display the channel data
print(channel1.data)

# Display the pressure on the channel
print("Channel 1 pressure: {}".format(channel1.pressure))

# Set the pressure on the channel to half of the maximum pressure
channel1.pressure = channel1.max_pressure
time.sleep(1)
print("Channel 1 pressure : {}".format(channel1.pressure))
