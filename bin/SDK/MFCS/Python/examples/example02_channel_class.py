#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This example shows how to interact with each individual channel of an MFCS, 
MFCS-EZ or PX device using the Channel class.
The MFCS.Channel class allows you to access the channel directly without
having to specify the channel number each time
"""

# Print function for Python 2 compatibility
from __future__ import print_function 
# MFCS class
from Fluigent.MFCS import MFCS
import time

# Get the serial numbers of the available instruments
instrument_serial_numbers = MFCS.detect()

if not instrument_serial_numbers:
    raise Exception("No MFCS Series device detected")
    
print("Available devices: {}". format(instrument_serial_numbers))

# Initialize the first instrument in the list
mfcs = MFCS(instrument_serial_numbers[0])

# Get a reference to the first channel of the device
channel1 = mfcs[1]

# "print" the channel to view its basic information
print(channel1)

# Set the pressure on the channel to half of the maximum pressure
channel1.set_pressure(channel1.max_pressure/2)
time.sleep(1)
print("Channel 1 pressure : {}".format(channel1.get_pressure()))
