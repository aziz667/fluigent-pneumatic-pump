#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This example shows how to initialize a PX device using the PX function
The PX function simply initializes an MFCS object and returns its first 
pressure channel, since the PX device only has one channel. Using this
channel reference, you can communicate with the PX device directly without
having to specify the channel, as you would do with an MFCS device
"""

# Print function for Python 2 compatibility
from __future__ import print_function 
# MFCS class
from Fluigent.MFCS import MFCS, PX
import time

# Get the serial numbers of the available instruments
instrument_serial_numbers = MFCS.detect()

if not instrument_serial_numbers:
    raise Exception("No MFCS Series device detected")
    
print("Available devices: {}". format(instrument_serial_numbers))

# Initialize the first instrument in the list as a PX
# If the device is not a PX, this line will raise an exception
px = PX(instrument_serial_numbers[0])

# "print" the initialized device to view its basic information
# Note that it is displayed as an MFCS channel
print(px)

# Display the pressure on the first channel
print("Pressure: {}".format(px.get_pressure()))

# Set the pressure on the channel to half of the maximum pressure
px.set_pressure(px.max_pressure/2)
time.sleep(1)
print("Pressure : {}".format(px.get_pressure()))
